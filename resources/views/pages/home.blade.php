@extends('layout')
@section('content')

<div class="flexslider" data-interval="3000" data-pause="true">
    <ul class="slides">
        @php
        $i = 0;
        @endphp
        @foreach($slider as $key => $slide)
        <li class="slide">
            @php
            $i++;
            @endphp
            <img alt="{{$slide->slider_desc}}" src="{{asset('public/uploads/slider/'.$slide->slider_image)}}"
                height="200" width="100%" class="img img-responsive img-slider">
        </li>
        @endforeach
    </ul>
</div>
<div id="ctservicecmsblock">
    <div class="container">
        <div class="row">
            <div class="serviceblock hb-animate-element right-to-left">
                <div class="service-1 service col-sm-4">
                    <span class="service1-icon icon"></span>
                    <div class="service-content">
                        <div class="content-title">Free Shipping</div>
                        <div class="content-desc">Curabitur blandit tempus ardua
                            ridiculus doloresed
                            magna.</div>
                    </div>
                </div>
                <div class="service2 service col-sm-4">
                    <span class="service2-icon icon"></span>
                    <div class="service-content">
                        <div class="content-title">online support</div>
                        <div class="content-desc">Curabitur blandit tempus ardua
                            ridiculus doloresed
                            magna.</div>
                    </div>
                </div>
                <div class="service-3 service col-sm-4">
                    <span class="service3-icon icon"></span>
                    <div class="service-content">
                        <div class="content-title">special offer</div>
                        <div class="content-desc">Curabitur blandit tempus ardua
                            ridiculus doloresed
                            magna.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <section class="ct-hometabcontent">
            <h1 class="h1 products-section-title"><span>Special Products</span></h1>
            <div class="tabs">
                <ul style="border-bottom: none" id="home-page-tabs"
                    class="nav nav-tabs clearfix hb-animate-element top-to-bottom hb-in-viewport">


                    <li class="nav-item">
                        <a style="background: none" data-toggle="tab" href="#newProduct" class="newProduct  nav-link "
                            aria-expanded="true">
                            <span>Sale products</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a style="background: none" data-toggle="tab" href="#bestseller" class=" bestseller nav-link "
                            aria-expanded="false">
                            <span> Special Products</span>
                        </a>
                    </li>

                </ul>


                <div class="tab-content hb-animate-element bottom-to-top hb-in-viewport">

                    <div id="newProduct" class="ct_productinner tab-pane active" aria-expanded="true">
                        <section class="newproducts clearfix">
                            <h2 class="h1 products-section-title text-uppercase">
                                Sale products
                            </h2>

                            <div class="products">
                                <div class="product_new">
                                    <!-- Define Number of product for SLIDER -->
                                    <ul id="newproduct-grid-sale"
                                        class="newproduct_grid product_list grid row gridcount ">
                                    </ul>


                                    <input type="hidden" id="numpage-sale" name="numpage-sale" value="0">
                                    <button id="load_more_button-sale" type="button"
                                        class="all-product-link pull-xs-left pull-md-right h4">Load more</button>


                                </div>
                            </div>
                        </section>
                    </div>
                    <div id="bestseller" class="ct_productinner tab-pane" aria-expanded="false">
                        <section class="bestseller-products">
                            <h2 class="h1 products-section-title">
                                Special Products
                            </h2>
                            <div class="products">
                                <div class="product_bestseller">
                                    <!-- Define Number of product for SLIDER -->
                                    <ul id="newproduct-grid-special"
                                        class="bestseller_grid product_list grid row gridcount">

                                    </ul>



                                    <input type="hidden" id="numpage-special" name="numpage-special" value="0">
                                    <button id="load_more_button-special" type="button"
                                        class="all-product-link pull-xs-left pull-md-right h4">Load more</button>
                                </div>
                                <!-- Modal -->



                            </div>

                        </section>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h1 style="text-align: center" id="name_modal" class="modal-title">Modal Header</h1>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 col-sm-6 hidden-xs-down" id="gallery_home">
                        <div class="images-container">
                            <div class="product-cover">
                                <img id="img_modal" class="js-qv-product-cover" src="" alt="" title=""
                                    style="width:100%;" itemprop="image">

                                <div class="layer hidden-sm-down" data-toggle="modal" data-target="#product-modal1">
                                    <i class="material-icons zoom-in"></i>
                                </div>
                            </div>
                            <!-- Define Number of product for SLIDER -->

                        </div>
                        <div class="arrows js-arrows" style="display: none;">
                            <i class="material-icons arrow-up js-arrow-up"></i>
                            <i class="material-icons arrow-down js-arrow-down"></i>
                        </div>
                    </div>



                    <div class="col-md-6 col-sm-6">

                        <div class="product-prices" style="text-align: left">
                            <div class="product-price h5 " itemprop="offers" itemscope="" itemtype="">
                                <link itemprop="availability">

                                <meta itemprop="priceCurrency" content="USD">
                                <div class="current-price" style="font-size: 18px">
                                    <div id="price"></div>
                                </div>
                                <div id="product_quickview_desc_1"></div>
                            </div>
                            <div class="tax-shipping-delivery-label">
                            </div>
                        </div>

                        <div class="product-actions">
                            <form>
                                @csrf
                                <div id="product_quickview_value"></div>
                                <input type="hidden" name="token" value="37a35808bd3e31a6c88db859b1dc2e5a">
                                <input type="hidden" name="id_product" value="1" id="product_page_product_id">
                                <input type="hidden" name="id_customization" value="0" id="product_customization_id">

                                <div class="product-add-to-cart">
                                    <span class="control-label">Quantity</span>
                                    <div class="product-quantity">
                                        <div class="qty">
                                            <div class="input-group bootstrap-touchspin">
                                                <span class="input-group-addon bootstrap-touchspin-prefix"
                                                    style="display: none;"></span><input type="text" name="qty"
                                                    id="quantity_wanted" value="1" class="input-group form-control"
                                                    min="1" style="display: block;"><span
                                                    class="input-group-addon bootstrap-touchspin-postfix"
                                                    style="display: none;"></span><span
                                                    class="input-group-btn-vertical"><button
                                                        class="btn btn-touchspin js-touchspin bootstrap-touchspin-up"
                                                        type="button"><i
                                                            class="material-icons touchspin-up"></i></button><button
                                                        class="btn btn-touchspin js-touchspin bootstrap-touchspin-down"
                                                        type="button"><i
                                                            class="material-icons touchspin-down"></i></button></span>
                                            </div>
                                        </div>

                                        <div class="add">

                                            <input id="data-id" type="button" value="Buy now"
                                                class="btn btn-primary add-to-cart-quickview" data-id_product
                                                name="add-to-cart-quickview">

                                            <span id="product-availability">
                                                <i class="material-icons product-available"></i>
                                                In stock
                                            </span>
                                        </div>
                                        <div id="beforesend_quickview_1"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <p class="product-minimal-quantity">
                                    </p>
                                </div>
                                <input class="product-refresh" data-url-update="false" name="refresh" type="submit"
                                    value="Refresh" hidden="">
                            </form>
                        </div>


                    </div>

                </div>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-primary redirect-cart" data-dismiss="modal">Go to
                    cart</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>


    </div>
</div>





<script type="text/javascript">
    $(document).ready(function(){
        var _token = $('input[name="_token"]').val();
        load_data(_token,'sale','');
        load_data(_token,'special','');
        function load_data(_token,branch,category){
            if(category != ''){
                var _num = parseInt($('input[id="numpage-'+category+'"]').val()) + 4;
            }else{
                var _num = parseInt($('input[id="numpage-'+branch+'"]').val()) + 4;
            }
            $.ajax({
                url : '{{url('/loadmore')}}',
                method: 'POST',
                data:{ _token:_token,"numpage":_num,"category": category,"branch": branch,"field_sort" : 'none', "type_sort": 'none'},
                success:function(data){
                    if(category != ''){
                        $('#newproduct-grid-'+category).html(data.out);
                        $('#numpage-'+category).val(data.num);
                    }else{
                        $('#newproduct-grid-'+branch).html(data.out);
                        $('#numpage-'+branch).val(data.num);
                    }
                    if(data.num >= data.count){
                        document.getElementById("load_more_button-"+branch).style.display = "none";
                    }

                    $('.quick-view').click(function(){
                        var id = $(this).data('id_product');
                        var cart_product_id = $('.cart_product_id_' + id).val();
                        var cart_product_name = $('.cart_product_name_' + id).val();
                        var cart_product_image = $('.cart_product_image_' + id).val();
                        var cart_product_quantity = $('.cart_product_quantity_' + id).val();
                        var cart_product_price = $('.cart_product_price_' + id).val();
                        var cart_product_sale_price = $('.cart_product_sale_price_' + id).val();
                        var cart_product_qty = $('.cart_product_qty_' + id).val();
                        var _token = $('input[name="_token"]').val();
                        if(cart_product_price!=(cart_product_price - cart_product_sale_price)){
                            $('#price').html(
                                '<span style="color:#777">'+ 'Our price: $'+'<span style="text-decoration: line-through">'+new Intl.NumberFormat().format(cart_product_price)+'</span>'+'</span>'+'<br><br>' + 'Savings: -$'+cart_product_sale_price
                                +'<br><br>'+'Sale Price: $'+new Intl.NumberFormat().format((cart_product_price - cart_product_sale_price))
                            );
                        }else{
                                $('#price').html('Our price: $'+ new Intl.NumberFormat().format(cart_product_price));
                        }
                        $('#price_modal').html(new Intl.NumberFormat().format(cart_product_price));
                        $('#discount_modal').html(new Intl.NumberFormat().format(cart_product_sale_price));
                        $('#regular_modal').html(new Intl.NumberFormat().format(cart_product_price - cart_product_sale_price));
                        $('#name_modal').html(cart_product_name);
                        $('#img_modal').attr("src", '{{URL::to('/public/uploads/product/')}}'+'/'+cart_product_image);
                        $('#data-id').attr("data-id_product",id) ;



                        $('#imageGallery1').lightSlider({
                            gallery:true,
                            item:1,
                            loop:true,
                            thumbItem:3,
                            slideMargin:0,
                            enableDrag: false,
                            currentPagerPosition:'left',
                            onSliderLoad: function(el) {
                                el.lightGallery({
                                    selector: '#imageGallery1 .lslide'
                                });
                            }
                        });

                        $("#myModal").modal();


                    });


                    $('.add-to-cart').click(function(){
                        var id = $(this).data('id_product');
                        var cart_product_id = $('.cart_product_id_' + id).val();
                        var cart_product_name = $('.cart_product_name_' + id).val();
                        var cart_product_image = $('.cart_product_image_' + id).val();
                        var cart_product_quantity = $('.cart_product_quantity_' + id).val();
                        var cart_product_price = $('.cart_product_price_' + id).val();
                        var cart_product_sale_price = $('.cart_product_sale_price_' + id).val();
                        var cart_product_qty = $('.cart_product_qty_' + id).val();
                        var _token = $('input[name="_token"]').val();
                        if(parseInt(cart_product_qty)>parseInt(cart_product_quantity)){
                        alert('Smaller please ' + cart_product_quantity);
                        }else{
                        $.ajax({
                            url: '{{url('/add-cart-ajax')}}',
                            method: 'POST',
                            data:{cart_product_id:cart_product_id,cart_product_name:cart_product_name,cart_product_image:cart_product_image,cart_product_price:cart_product_price,cart_product_sale_price:cart_product_sale_price,cart_product_qty:cart_product_qty,_token:_token,cart_product_quantity:cart_product_quantity},
                            success:function(data){
                                $('.cart-products-count').html(data);
                                swal({
                                        title: "Product added to cart",
                                        text: "",
                                        showCancelButton: true,
                                        cancelButtonText: "See more",
                                        confirmButtonClass: "btn-success",
                                        confirmButtonText: "Go to cart",
                                        closeOnConfirm: false

                                    },
                                    function() {
                                        window.location.href = "{{url('/cart')}}";
                                    });

                        }
                    });
              }
          });
                }
            });
        }
        $(document).on('click','#load_more_button-sale',function(){
            load_data(_token,'sale','');
        });
        $(document).on('click','#load_more_button-special',function(){
            load_data(_token,'special','');
        });
    });

</script>

{{-- load more button --}}
<script type="text/javascript">
    $(document).ready(function(){
            var _token = $('input[name="_token"]').val();
            // loadmorebutton(_token,'special','');

            function loadmorebutton(_token,branch){
                    var _num = parseInt($('input[id="numpage-special"]').val()) + 4;
                $.ajax({
                    url : '{{url('/loadmorebutton')}}',
                    method: 'POST',
                    data:{ _token:_token,"numpage":_num},
                    success:function(data){
                            $('#newproduct-grid-special').html(data.out);
                            $('#numpage-special').val(data.num);
                        if(data.num >= data.count){
                            document.getElementById("load_more_button-"+branch).style.display = "none";
                        }
                    }
                });

            }

            $(document).on('click','#load_more_button-special',function(){
                loadmorebutton(_token,'special','');
            });
    });

</script>
@endsection
