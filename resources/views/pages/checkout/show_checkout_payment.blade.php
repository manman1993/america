<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkout with PayPal</title>
    <base href="{{('/public/frontend/')}}">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">


    <style>
        .img-responsive {
            margin: 0 auto;
        }

        .bg-color {
            color: white;
            background: -moz-linear-gradient(0deg, #004094 0%, #0096D9 50%, #004094 100%); /* ff3.6+ */
            background: -webkit-gradient(linear, left top, right top, color-stop(0%, #004094), color-stop(50%, #0096D9), color-stop(100%, #004094)); /* safari4+,chrome */
            background: -webkit-linear-gradient(0deg, #004094 0%, #0096D9 50%, #004094 100%); /* safari5.1+,chrome10+ */
            background: -o-linear-gradient(0deg, #004094 0%, #0096D9 50%, #004094 100%); /* opera 11.10+ */
            background: -ms-linear-gradient(0deg, #004094 0%, #0096D9 50%, #004094 100%); /* ie10+ */
            background: linear-gradient(90deg, #004094 0%, #0096D9 50%, #004094 100%); /* w3c */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#004094', endColorstr='#004094',GradientType=1 ); /* ie6-9 */
        }

        form p {
            margin-bottom: 0;
        }

    </style>

</head>
<body>

    <div class="container-fluid">
        <div class="well bg-color">
            <h2 class="text-center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                Checkout with PayPal</font></font></h2>

        </div>
    </div>

    <div class="col-sm-offset-4 col-md-4">


        <form id="orderView" class="form-horizontal" style="display: block;">
            <h3 style="text-align: center;><font style="vertical-align: inherit;"><font style="text-align: center;vertical-align: inherit;">Your payment is complete</font></font></h3>
            <h4 style="text-align: center;">
                <span id="viewFirstName"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$paypal->full_name}} </font></font></span>
                <span id="viewLastName"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"></font></font></span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> , Thank you for your Order
            </font></font></h4>
            <hr>
            <div class="form-group">
                <div class="form-group">
                    <label class="col-sm-5 control-label"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Shipping Details</font></font></label>
                    <div class="col-sm-7">
                        <p id="viewRecipientName"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$paypal->full_name}}</font></font></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Transaction Details</font></font></label>
                    <div class="col-sm-7">


                        <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Payment Total Amount: </font></font><span id="viewFinalAmount"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{'$'.number_format($paypal->price_paypal,2)}}</font></font></span> </p>
                        <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Currency Code: </font></font><span id="viewCurrency"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">USD</font></font></span></p>
                        <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Payment Status: </font></font><span id="viewPaymentState"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$paypal->status}}</font></font></span></p>
                        <p id="transactionType" style="display: none;">Payment Type: <span id="viewTransactionType"></span> </p>
                    </div>
                </div>
            </div>
            <hr>
        <h3><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Click </font></font><a href="{{'/'}}"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">here</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> to return to Home Page</font></font></h3>
        </form>
    </div>
</body>
</html>


