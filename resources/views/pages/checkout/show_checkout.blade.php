@extends('layout')
@section('content')
<section class="special-products">
    <div class="container">
        <section id="cart_items" >
            <div class="container">
                <div class="row">
                    <div id="columns_inner">
                        <div id="content-wrapper" class="left-column col-xs-12 col-sm-12 col-md-12">
                            <section id="main">

                                     <div class="cart-grid row">
                                    <!-- Left Block: cart product informations & shpping -->
                                    <div class="cart-grid-body col-xs-12 col-lg-8">
                                        <!-- cart products detailed -->
                                        <div class="card cart-container">
                                            <div class="card-block">
                                                <h1 class="h1">SHIPPING METHOD</h1>
                                                    <div class="form-one">
                                                    <form  id="idform" class="js-customer-form" method="post"  style="text-align: left">
                                                        @csrf
                                                        <section>
                                                            <input type="hidden" name="id_customer" value="45">

                                                            <div class="form-group row ">

                                                                <div class="col-md-6">
                                                                    <p >First name*
                                                                        <input   class="form-control first_name" id="first_name"  name="first_name" type="text" value="" required="" >

                                                                        {{-- @error('firstname')
					                                                        <span class="text-danger">{{ $message }}</span>
					                                                    @enderror --}}
                                                                    </p>
                                                                    <p >Last name*
                                                                        <input style="border:1px solid #ccc" class="form-control last_name"   name="last_name" type="text" value="" required="">

                                                                    </p>
                                                                    <p> Phone*
                                                                        <input style="border:1px solid #ccc" class="form-control js-child-focus js-visible-password shipping_phone"  name="shipping_phone" type="number" value=""  required="">

                                                                    </p>
                                                                    <p >
                                                                        State*
                                                                        <select style="height: 36px;border:1px solid #ccc" name="city" id="city" class="form-control input-sm m-bot15 choose city " >
                                                                                <option  value="">-- please choose --</option>
                                                                            @foreach($city as $key => $ci)
                                                                                <option  value="{{$ci->matp}}">{{$ci->name_city}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </p>
                                                                </div>
                                                                <div class="col-md-6 ">
                                                                        <p >Address*
                                                                            <input style="border:1px solid #ccc" class="form-control shipping_address"  name="shipping_address" type="text" value="" required="">

                                                                        </p>
                                                                        <p>
                                                                            Apt, suite, etc (optional)*<input style="border:1px solid #ccc" class="form-control js-child-focus js-visible-password shipping_notes"  name="shipping_notes" type="text" value=""  >

                                                                        </p>
                                                                        <p> City*<input style="border:1px solid #ccc" class="form-control js-child-focus js-visible-password shipping_city"  name="shipping_city" type="text" value=""  required=""></p>
                                                                        <p> ZIP code*<input style="border:1px solid #ccc" class="form-control js-child-focus js-visible-password" class="zip_code" name="zip_code" type="number" value=""  required=""></p>
                                                                </div>
                                                            </div>




                                                        <div class="form-group row ">
                                                            <label class="col-md-3 form-control-label required">

                                                            </label>
                                                            <div class="col-md-6" >
                                                                {{-- <input type="radio"  id="paypal" value="0" name="payment_radio" >&nbsp;&nbsp;  --}}
                                                                <img width="50" src="images/paypal.png" alt=""> Paypal payment<br>
                                                                {{-- <input type="radio" id="visa" value="1" name="payment_radio" > --}}
                                                            </div>
                                                            <div class="col-md-3 form-control-comment">
                                                            </div>
                                                        </div>
                                                            <div class="form-group row ">
                                                                <label class="col-md-3 form-control-label required">

                                                                </label>
                                                                <div class="col-md-6" style="display: flex;justify-content: center">
                                                                    <input type="submit" value="PAYMENT" name="send_order" class="btn btn-primary btn-sm check-err send_order">
                                                                </div>
                                                                <div class="col-md-3 form-control-comment">
                                                                </div>
                                                            </div>
                                                        </section>


                                                    </form>

                                                </div>
                                            </div>
                                            <hr>

                                        </div>

                                        <a class="label" href="{{'/'}}">
                                        <i class="material-icons">chevron_left</i>Continue shopping
                                        </a>
                                        <!-- shipping informations -->
                                    </div>


                                    <!-- Right Block: cart subtotal & cart total -->
                                    <div class="cart-grid-right col-xs-12 col-lg-4">
                                        <div class="card cart-summary">
                                            <div class="cart-detailed-totals">

                                                <form action="{{url('/update-cart')}}" method="POST">
                                                    @csrf
                                                    @php
                                                            $total = 0;
                                                    @endphp
                                                    @if(Session::get('cart')==true)


                                                        @foreach(Session::get('cart') as $key => $cart)
                                                            @php
                                                                $subtotal = ($cart['product_price']-$cart['product_sale_price'])*$cart['product_qty'];
                                                                $total+=$subtotal;
                                                            @endphp


                                                    <div class="cart-item">
                                                        <div class="product-line-grid">
                                                            <div class="product-line-grid-left col-md-3 col-xs-4">
                                                                <span class="product-image media-middle">
                                                                    <img src="{{asset('public/uploads/product/'.$cart['product_image'])}}" width="90" alt="{{$cart['product_name']}}" />
                                                                </span>
                                                            </div>
                                                          <div class="product-line-grid-body col-md-4 col-xs-8">
                                                                <div class="product-line-info">
                                                                        {{$cart['product_name']}}
                                                                </div>

                                                            </div>

                                                            <div class="product-line-grid-right product-line-actions col-md-5 col-xs-12">
                                                                <div class="row">
                                                                    <div class="col-xs-4 hidden-md-up"></div>
                                                                    <div class="col-md-10 col-xs-6">
                                                                        <div class="row">
                                                                            <div class="col-md-6 col-xs-6 qty">
                                                                                <span class="product-price">
                                                                                    <strong>
                                                                                        <p>x{{$cart['product_qty']}}</p>

                                                                                    </strong>
                                                                                </span>
                                                                            </div>
                                                                            <div class="col-md-6 col-xs-2 price">
                                                                                <span class="product-price">
                                                                                <strong>
                                                                                    <p>{{'$'.number_format($subtotal)}}</p>

                                                                                </strong>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>

                                                @endforeach

                                                    @else
                                                    <tr>
                                                        <td colspan="5"><center>
                                                            <td colspan="5">
                                                                <p style="color: red;margin-top:10px">Please add product to cart</p>
                                                            </td>
                                                    </tr>
                                                    @endif
                                            </form>

                                                <div class="card-block">

                                                    <div class="cart-summary-line" id="cart-subtotal-shipping">
                                                        <span class="label">
                                                        Payment:
                                                        </span>
                                                        <input id="total_price" value="{{$total*0.0825 + $total}}" type="hidden">
                                                        <strong><span class="value">$<span id="total_113">{{number_format($total)}}</span></span> </strong>
                                                        <div><small class="value"></small></div>
                                                    </div>
                                                    <div class="cart-summary-line" id="cart-subtotal-shipping">
                                                        <span class="label">
                                                            Shipping:
                                                        </span>
                                                        <span class="value">$<span id="fee_ajax" >0</span></span>

                                                        <div><small class="value"></small></div>
                                                    </div>
                                                    <div class="cart-summary-line" id="cart-subtotal-shipping">
                                                        <span class="label">
                                                            Sales Tax:
                                                        </span>
                                                        <span class="value">$<span id="fee_vat" >{{number_format($total*0.0825)}}</span></span>

                                                        <div><small class="value"></small></div>
                                                    </div>

                                                </div>
                                                <hr>
                                                <div class="card-block">
                                                    <div class="cart-summary-line cart-total">
                                                        <span class="label">Total: </span>
                                                        <strong><span class="value">$<span id="total_112" >{{number_format($total + $total*0.0825)}}</span> </span> </strong>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>

                                        </div>
                                        <div id="block-reassurance" style="text-align:left !important">
                                            <ul>
                                                <li>
                                                    <div class="block-reassurance-item">
                                                        <img src="images/icon-1.png" alt="Security policy (edit with Customer reassurance module)">
                                                        <span class="h6">Security policy (edit with Customer reassurance module)</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="block-reassurance-item">
                                                        <img src="images/icon-2.png" alt="Delivery policy (edit with Customer reassurance module)">
                                                        <span class="h6">Delivery policy (edit with Customer reassurance module)</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="block-reassurance-item">
                                                        <img src="images/icon3.png" alt="Return policy (edit with Customer reassurance module)">
                                                        <span class="h6">Return policy (edit with Customer reassurance module)</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</section>

@endsection





