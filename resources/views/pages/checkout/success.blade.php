

 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>Email</title>
 </head>

 <body>
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td align="center" valign="top">
                    <table style="border:1px solid rgb(237,237,237)" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                        <tbody>
                            <tr>
                                <td bgcolor="#ffffff">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td>

                                                    <div class="a6S" dir="ltr" style="opacity: 0.01; left: 811.6px; top: 100.8px;">
                                                        <div id=":ei" class="T-I J-J5-Ji aQv T-I-ax7 L3 a5q" role="button" tabindex="0" aria-label=" " data-tooltip-class="a1V" data-tooltip="">
                                                            <div class="aSK J-J5-Ji aYr"></div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td valign="top" width="45"></td>
                                                <td style="font-family:Helvetica,'Arial',sans-serif;color:rgb(0,0,0);font-size:11px" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td height="15">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-family:Helvetica,'Arial',sans-serif;color:rgb(0,175,65);font-size:20px;font-weight:bold;line-height:26px;text-align:left" align="left">

                                                                    HI <span style="text-transform: uppercase">{{$customer_name}}</span>,
                                                                    THANK YOU FOR SHOPPING WITH HANDMADE OF AMERICA!
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="15">

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="font-size:12px;line-height:21px;font-weight:bold" align="left" valign="top" width="44%">Total<br>
                                                                                    <span style="font-size:24px;line-height:32px;font-weight:bold;color:rgb(0,175,65)">{{'$'.number_format($price)}}</span>
                                                                                </td>
                                                                                <td style="font-size:12px;line-height:21px;font-weight:bold" align="left" valign="top" width="56%">
                                                                                    Date &nbsp;&nbsp;|&nbsp; Time
                                                                                    <br>
                                                                                    <span style="font-size:18px;font-weight:bold;color:#00af41">{{$time}}</span>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td height="12">

                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td valign="top" width="45"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td height="5">
                                                   </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table align="center" bgcolor="#f4f4f4" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td valign="top" width="45"></td>
                                                <td align="center" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="left" height="20"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td style="font-size:14px;font-weight:bold;color:rgb(0,175,65)" align="left" width="55%">
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                            <tr>

                                                                <td valign="top" width="270">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style="font-size:14px;font-weight:bold;color:rgb(0,175,65)" align="left" valign="top">ORDER DETAILS</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center" height="10" valign="middle">
                                                                                   </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <table style="border:1px solid rgb(221,221,221)" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="padding:0cm" align="left" valign="top">
                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                        <tbody>
                                                                                                            <tr>
                                                                                                                <td valign="top">
                                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                        <tbody>
                                                                                                                            <tr>
                                                                                                                                <td align="left" valign="top">
                                                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td align="left" height="10"></td>
                                                                                                                                                <td colspan="2" align="left" height="10"></td>
                                                                                                                                                <td align="left" height="10"></td>
                                                                                                                                            </tr>

                                                                                                                                            <tr>
                                                                                                                                                <td align="left" height="5"></td>
                                                                                                                                                <td colspan="2" align="left" height="5"></td>
                                                                                                                                                <td align="left" height="5"></td>
                                                                                                                                            </tr>

                                                                                                                                            <tr>
                                                                                                                                                <td align="left" width="15"></td>
                                                                                                                                                <td style="font-family:'',Helvetica" align="left" width="171">
                                                                                                                                                    <span style="font-size:11px;color:rgb(158,158,158);line-height:21px">Detail</span>
                                                                                                                                                </td>
                                                                                                                                                <td style="font-family:'',Helvetica" align="left" width="80">
                                                                                                                                                    <span style="font-size:11px;color:rgb(158,158,158);line-height:28px">&nbsp;&nbsp;Price</span>
                                                                                                                                                </td>
                                                                                                                                                <td align="left" width="15"></td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td align="left" height="3"></td>
                                                                                                                                                <td colspan="2" align="left" height="3"></td>
                                                                                                                                                <td align="left" height="3"></td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td align="left" height="5"></td>
                                                                                                                                                <td colspan="2" style="border-top:1px dashed rgb(158,158,158)" align="left" height="5"></td>
                                                                                                                                                <td align="left" height="5"></td>
                                                                                                                                            </tr>




                                                                                                                                                {!!$output!!}

                                                                                                                                            <tr>
                                                                                                                                                <td align="right" width="15"></td>
                                                                                                                                                <td align="right">&nbsp;</td>
                                                                                                                                                <td align="right">&nbsp;</td>
                                                                                                                                                <td align="right" width="15"></td>
                                                                                                                                            </tr>


                                                                                                                                            <tr>
                                                                                                                                                <td align="left" width="15"></td>
                                                                                                                                                <td align="left" style="font-family:Helvetica,'Arial',sans-serif;font-weight:normal;color:#000000"><span style="font-size:11px;color:#000000;line-height:18px">Fee</span></td>
                                                                                                                                                <td align="left" style="font-family:Helvetica,'Arial',sans-serif;font-weight:normal;color:#000000">
                                                                                                                                                    <span style="font-size:11px;color:#000000;line-height:18px">&nbsp;&nbsp;${{$fee}}</span>
                                                                                                                                                </td>
                                                                                                                                                <td align="left" width="15"></td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td align="left" width="15"></td>
                                                                                                                                                <td align="left" style="font-family:Helvetica,'Arial',sans-serif;font-weight:normal;color:#000000"><span style="font-size:11px;color:#000000;line-height:18px"> Sales Tax</span></td>
                                                                                                                                                <td align="left" style="font-family:Helvetica,'Arial',sans-serif;font-weight:normal;color:#000000">

                                                                                                                                                    <span style="font-size:11px;color:#000000;line-height:18px">&nbsp;&nbsp;{{'$'.number_format($vat*0.0825)}}</span>
                                                                                                                                                </td>
                                                                                                                                                <td align="left" width="15"></td>
                                                                                                                                            </tr>

                                                                                                                                            <tr>
                                                                                                                                                <td align="left" height="10"></td>
                                                                                                                                                <td colspan="2" align="left" height="10"></td>
                                                                                                                                                <td align="left" height="10"></td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td align="left" height="10"></td>
                                                                                                                                                <td colspan="2" style="border-top:1px dashed rgb(158,158,158)" align="left" height="10"></td>
                                                                                                                                                <td align="left" height="10"></td>
                                                                                                                                            </tr>
                                                                                                                                            <tr>
                                                                                                                                                <td align="left" width="15"></td>
                                                                                                                                                <td align="right">
                                                                                                                                                    <span style="font-size:12px;font-weight:bolder;color:rgb(0,0,0);line-height:28px">Total&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                                                                                                                </td>
                                                                                                                                                <td align="left">
                                                                                                                                                    <span style="font-size:12px;font-weight:bolder;color:rgb(0,0,0);line-height:28px">&nbsp;&nbsp;{{'$'.number_format($price)}}</span>
                                                                                                                                                </td>
                                                                                                                                                <td align="left" width="15"></td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                            <tr>
                                                                                                                                <td align="left" valign="top">
                                                                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                                                                                        <tbody>
                                                                                                                                            <tr>
                                                                                                                                                <td align="left">

                                                                                                                                                </td>
                                                                                                                                            </tr>
                                                                                                                                        </tbody>
                                                                                                                                    </table>
                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </tbody>
                                                                                                                    </table>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center" valign="middle" height="10">

                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td height="20"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td valign="top" width="45">
                                                   </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
 </html>
