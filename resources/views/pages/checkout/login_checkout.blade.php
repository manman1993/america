
@extends('layout')
@section('content')

<section class="special-products">
    <div class="container">
        <section id="wrapper">
            <div class="container">
                <div class="row">
                    <div id="columns_inner">
                        <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                            <div class="block-categories block">
                                <h4 class="block_title hidden-md-down">
                                    <a href="{{'/'}}">Home</a>
                                </h4>
                                <h4 class="block_title hidden-lg-up" data-target="#block_categories_toggle"
                                    data-toggle="collapse">
                                    <a href="{{'/'}}">Home</a>
                                    <span class="pull-xs-right">
                                        <span class="navbar-toggler collapse-icons">
                                            <i class="material-icons add"></i>
                                            <i class="material-icons remove"></i>
                                        </span>
                                    </span>
                                </h4>
                                <div id="block_categories_toggle" class="block_content collapse">
                                    <ul class="category-top-menu">
                                        @foreach($category as $key => $cate)
                                        <li class="category" id="category-3">
                                            <a class="dropdown-item"
                                                href="{{URL::to('/category/'.$cate->slug_category_product)}}"
                                                data-depth="0">
                                                {{$cate->category_name}}
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                            <div class="sidebar-specials block">
                                <h4 class="block_title hidden-md-down">Sale Products</h4>
                                <h4 class="block_title hidden-lg-up" data-target="#block_specials_toggle"
                                    data-toggle="collapse">Special Products
                                    <span class="pull-xs-right">
                                        <span class="navbar-toggler collapse-icons">
                                            <i class="material-icons add"></i>
                                            <i class="material-icons remove"></i>
                                        </span>
                                    </span>
                                </h4>

                                <div class="block_content collapse" id="block_specials_toggle">
                                    <div class="products clearfix">
                                        @foreach($all_sale as $key => $cart)
                                        <div class="product-item">
                                            <div class="left-part">

                                                <a href="{{URL::to('/product/'.$cart->product_slug)}}">
                                                    <span class="product-image media-middle">
                                                        <img src="{{URL::to('public/uploads/product/'.$cart->product_image)}}"
                                                            width="90" alt="" />
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="right-part">
                                                <div class="product-description">

                                                    <h1 class="h3 product-title" itemprop="name"><a href=""></a></h1>
                                                    <div class="product-price-and-shipping">
                                                        <p>{{$cart->product_name}}</p>
                                                        <span style="text-decoration-line: line-through"
                                                            itemprop="price "
                                                            class="regular-price">{{'$'.(number_format($cart->product_price))}}</span>
                                                        <span
                                                            class="discount-percentage">{{'-$'.(number_format($cart->product_sale_price))}}</span>
                                                        <span
                                                            class="price">{{'$'.(number_format($cart->product_price - $cart->product_sale_price))}}</span>
                                                    </div>
                                                </div>
                                                <div class="product-actions">
                                                    <form>
                                                        @csrf
                                                        <input type="hidden" value="{{$cart->product_id}}"
                                                            class="cart_product_id_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_name}}"
                                                            class="cart_product_name_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_quantity}}"
                                                            class="cart_product_quantity_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_image}}"
                                                            class="cart_product_image_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_price}}"
                                                            class="cart_product_price_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_sale_price}}"
                                                            class="cart_product_sale_price_{{$cart->product_id}}">
                                                        <input type="hidden" value="1"
                                                            class="cart_product_qty_{{$cart->product_id}}">

                                                        <input type="button" value="Add to cart"
                                                            class="btn btn-default add-to-cart"
                                                            data-id_product="{{$cart->product_id}}" name="add-to-cart">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="clearfix">

                                        <a href="{{URL::to('/product-sale/sale')}}" class="allproducts">All sale
                                            products</a>


                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">

                            <header class="page-header" style="border-bottom:none">
                                <h1>
                                    LOG IN TO YOUR ACCOUNT
                                </h1>
                            </header>
                            <section id="content" class="page-content card card-block">
                                <section class="login-form">
                                    @if(session('error'))
                                    <div style="color: red">{{session('error')}}</div>
                                    @endif
                                    <form id="login-form" action="{{URL::to('/login-customer')}}" method="POST">
                                        {{csrf_field()}}
                                        <section>
                                            <input type="hidden" name="back" value="my-account">
                                            <div class="form-group row ">
                                                <label class="col-md-3 form-control-label required">
                                                Email
                                                </label>
                                                <div class="col-md-6">
                                                    <input class="form-control" name="email_account" type="email" value="" required="">
                                                </div>
                                                <div class="col-md-3 form-control-comment">
                                                </div>
                                            </div>
                                            <div class="form-group row ">
                                                <label class="col-md-3 form-control-label required">
                                                Password
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group js-parent-focus">
                                                        <input class="form-control js-child-focus js-visible-password" name="password_account" type="password" value="" pattern=".{5,}" required="">
                                                        <span class="input-group-btn">
                                                        <button class="btn" type="button" data-action="show-password" data-text-show="Show" data-text-hide="Hide">
                                                        Show
                                                        </button>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 form-control-comment">
                                                </div>
                                            </div>



                                            <div class="forgot_password">
                                            <a href="{{'/forgot_password'}}" rel="nofollow">
                                                Forgot your password?
                                                </a>
                                            </div>

                                        </section><br>
                                        <footer class="form-footer text-xs-center clearfix">
                                            <input type="hidden" name="submitLogin" value="1">
                                            <button class="btn btn-primary" data-link-action="sign-in" type="submit">
                                            Sign in
                                            </button>
                                        </footer>
                                    </form>
                                </section>
                                <hr>
                                <div class="no-account">
                                <a href="{{('/create-account')}}" data-link-action="display-register-form">
                                    No account? Create one here
                                    </a>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>


@endsection




