<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkout with PayPal</title>
    <base href="{{('/public/frontend/')}}">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.min.css" rel="stylesheet">


    <style>
        .img-responsive {
            margin: 0 auto;
        }

        .bg-color {
            color: white;
            background: -moz-linear-gradient(0deg, #004094 0%, #0096D9 50%, #004094 100%); /* ff3.6+ */
            background: -webkit-gradient(linear, left top, right top, color-stop(0%, #004094), color-stop(50%, #0096D9), color-stop(100%, #004094)); /* safari4+,chrome */
            background: -webkit-linear-gradient(0deg, #004094 0%, #0096D9 50%, #004094 100%); /* safari5.1+,chrome10+ */
            background: -o-linear-gradient(0deg, #004094 0%, #0096D9 50%, #004094 100%); /* opera 11.10+ */
            background: -ms-linear-gradient(0deg, #004094 0%, #0096D9 50%, #004094 100%); /* ie10+ */
            background: linear-gradient(90deg, #004094 0%, #0096D9 50%, #004094 100%); /* w3c */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#004094', endColorstr='#004094',GradientType=1 ); /* ie6-9 */
        }

        form p {
            margin-bottom: 0;
        }

    </style>

</head>
<body>
    <div class="container-fluid">
        <div class="well bg-color">
            <h2 class="text-center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                Checkout with PayPal</font></font></h2>

        </div>
    </div>

    <div class="col-sm-offset-3 col-md-6">
        <form id="orderView" class="form-horizontal" style="display: block;">
            <hr>
            <div class="form-group">
                <div class="form-group">
                    <h3 style="text-align:center;color:red " class="col-sm-12 control-label"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Payment failed</font></font></h3>
                </div>
            </div>
            <hr>
        <h3 style="text-align:center "><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Click </font></font><a href="{{'/'}}"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">here</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> to return to Home Page</font></font></h3>
        </form>
    </div>

</body>
</html>


