@extends('layout')
@section('content')
<section class="special-products">
    <div class="container">
        <div class="row">
            <h2 class="h1 products-section-title">
                <span>Contact Us</span>
            </h2>
            <div class="products hb-animate-element right-to-left">
                <div class="product_special">

                    <table width="100%" border="0" cellspacing="5" cellpadding="5" align="center">
                        <tbody>
                            <tr>
                                <td>
                                    <div style="text-align: left">
                                        <div class="article-wrap">
                                            <div class="vcb-snippet">
                                                <p>
                                                    <strong style="font-family: Helvetica;"><span style="font-family: Arial;">
                                                            <inline style="color: rgb(0, 0, 0);">Accepted Payment Types</inline>
                                                        </span></strong>
                                                </p>
                                                <p>
                                                    <span style="font-family: Arial;"><strong style="font-family: Helvetica;">
                                                            <inline style="font-family: Arial;"></inline>
                                                        </strong></span><span style="font-family: Arial; color: rgb(127, 127, 127);">We
                                                        accept Paypal and credit card payments. O</span><span
                                                        style="font-family: Helvetica;">
                                                        <inline style="color: rgb(127, 127, 127); font-family: Arial;">ur site is safe and
                                                            secured with SSL certificate.</inline>
                                                    </span>
                                                </p>
                                                <p style="background-color: rgb(255, 255, 255);">
                                                    <span style="font-family: Helvetica;"><strong>
                                                            <inline style="font-family: Arial;"><br></inline>
                                                        </strong></span>
                                                </p>
                                                <p style="background-color: rgb(255, 255, 255);">
                                                    <span style="font-family: Helvetica;"><strong>
                                                            <inline style="font-family: Arial; color: rgb(0, 0, 0);">Returns and Exchanges
                                                            </inline>
                                                        </strong></span>
                                                </p>
                                                <p style="background-color: rgb(255, 255, 255);">
                                                    <span style="font-family: Helvetica;"><strong>
                                                            <inline style="font-family: Arial;"></inline>
                                                        </strong></span><span style="font-family: Arial;">
                                                        <inline style="color: rgb(99, 99, 99); line-height: 18px; font-family: Helvetica;">
                                                            We want you to be 100% satisfied with your purchase. If you are not satisfied,
                                                            please let us know so that we may issue an exchange or full refund. In order to
                                                            receive an exchange or full refund, <b>all </b><strong>necklaces and
                                                                bracelets</strong> must be returned in unworn condition <strong>within 14
                                                                days</strong> of purchase</inline>
                                                    </span><span style="color: rgb(99, 99, 99); font-family: Arial;">. All returned packages
                                                        must include <strong>a printed invoice </strong>and<strong>
                                                            tracking</strong>.</span>
                                                </p>
                                                <p style="background-color: rgb(255, 255, 255);">
                                                    <span style="color: rgb(99, 99, 99); font-family: Arial;">To print your invoice, simply
                                                        login to your account, go to "My Account", click "Print invoices" under "My Orders"
                                                        menu and choose the correct invoice to print. O</span><span
                                                        style="font-family: Arial; color: inherit;">nce you have the printed invoice, please
                                                        include it in your returned package and ship them with tracking to the address
                                                        below:</span>
                                                </p>
                                                <inline style="font-family: Arial;"><b>

                                                   <span style="font-size: 12px;">All associated cost for shipping and tracking
                                                        is to be paid by you. </span><span
                                                        style="font-size: 12px; color: rgb(99, 99, 99);">We will notify you via e-mail of
                                                        your refund once we have received and processed the returned item.</span><br>
                                                </inline><br><br>
                                                <p
                                                    style="margin-bottom: 20px; outline: none; line-height: 1.5; color: rgb(99, 99, 99); font-family: Merriweather, Georgia, 'Times New Roman', Times, serif; background-color: rgb(255, 255, 255);">
                                                    <span style="font-family: Helvetica;">
                                                        <inline style="font-family: Helvetica;">
                                                            <inline style="font-family: Helvetica;">
                                                                <inline style="font-family: Helvetica;">
                                                                    <inline style="font-family: Helvetica;">
                                                                        <inline style="font-family: Helvetica; color: rgb(0, 0, 0);">
                                                                            <strong>
                                                                                <inline style="font-family: Arial; color: rgb(0, 0, 0);">
                                                                                    Shipping Processing and Carrier</inline>
                                                                            </strong></inline>
                                                                    </inline>
                                                                </inline>
                                                            </inline>
                                                        </inline>
                                                    </span>
                                                </p>
                                                <p
                                                    style="margin-bottom: 20px; outline: none; line-height: 1.5; color: rgb(99, 99, 99); font-family: Merriweather, Georgia, 'Times New Roman', Times, serif; background-color: rgb(255, 255, 255);">
                                                    <span style="font-family: Helvetica;">
                                                        <inline style="font-family: Helvetica;">
                                                            <inline style="font-family: Helvetica;">
                                                                <inline style="font-family: Helvetica;">
                                                                    <inline style="font-family: Helvetica;">
                                                                        <inline style="font-family: Helvetica; color: rgb(0, 0, 0);">
                                                                            <strong>
                                                                                <inline style="font-family: Arial;"></inline>
                                                                            </strong></inline>
                                                                    </inline>
                                                                </inline>
                                                            </inline>
                                                        </inline>
                                                    </span><span style="font-family: Arial;">All orders are processed within 1-3 business
                                                        days and shipped via USPS FIRST CLASS from Barrington, NJ.</span>
                                                </p>
                                                <p
                                                    style="margin-bottom: 20px; outline: none; line-height: 1.5; color: rgb(99, 99, 99); font-family: Merriweather, Georgia, 'Times New Roman', Times, serif; background-color: rgb(255, 255, 255);">
                                                    <strong style="color: rgb(0, 0, 0); font-family: Helvetica;"><span
                                                            style="font-family: Arial;"><br></span></strong>
                                                </p>
                                                <p
                                                    style="margin-bottom: 20px; outline: none; line-height: 1.5; color: rgb(99, 99, 99); font-family: Merriweather, Georgia, 'Times New Roman', Times, serif; background-color: rgb(255, 255, 255);">
                                                    <span style="font-family: Arial;"></span><strong
                                                        style="color: rgb(0, 0, 0); font-family: Helvetica;"><span
                                                            style="font-family: Arial;">
                                                            <inline style="color: rgb(0, 0, 0);">Shipping Rates</inline>
                                                        </span></strong>
                                                </p>
                                                <p
                                                    style="margin-bottom: 20px; outline: none; line-height: 1.5; color: rgb(99, 99, 99); font-family: Merriweather, Georgia, 'Times New Roman', Times, serif; background-color: rgb(255, 255, 255);">
                                                    <span style="font-family: Helvetica;">
                                                        <inline style="font-family: Helvetica;">
                                                            <inline style="font-family: Helvetica;">
                                                                <inline style="font-family: Helvetica;">
                                                                    <inline style="margin: 0px; padding: 0px; outline: none;">
                                                                        <inline style="color: rgb(0, 0, 0); font-family: Helvetica;">
                                                                            <strong>
                                                                                <inline style="font-family: Arial;"></inline>
                                                                            </strong></inline>
                                                                    </inline>
                                                                </inline>
                                                            </inline>
                                                        </inline>
                                                    </span><span style="font-family: Arial;">Shipping cost is calculated based on the actual
                                                        weight of your entire order.</span>
                                                </p>
                                                <strong style="color: rgb(99, 99, 99); font-family: Arial;"><br></strong><br>

                                                <p
                                                    style="margin-bottom: 20px; color: rgb(99, 99, 99); line-height: 1.5; font-family: Merriweather, Georgia, 'Times New Roman', Times, serif; background-color: rgb(255, 255, 255); outline: none;">
                                                    <span style="font-family: Helvetica;">
                                                        <inline>
                                                            <inline>
                                                                <inline>
                                                                    <inline>
                                                                        <inline style="color: rgb(0, 0, 0);"><strong>
                                                                                <inline style="font-family: Arial; color: rgb(0, 0, 0);">
                                                                                    Contact Us</inline>
                                                                            </strong></inline>
                                                                    </inline>
                                                                </inline>
                                                            </inline>
                                                        </inline>
                                                    </span>
                                                </p>
                                                <p
                                                    style="margin-bottom: 20px; color: rgb(99, 99, 99); line-height: 1.5; font-family: Merriweather, Georgia, 'Times New Roman', Times, serif; background-color: rgb(255, 255, 255); outline: none;">
                                                    <span style="font-family: Helvetica;">
                                                        <inline style="color: rgb(89, 89, 89);">Any questions or concerns, please email us
                                                            at <strong>
                                                                <inline style="color: rgb(0, 0, 0);">handmadeofamerica@gmail.com</inline>
                                                            </strong></inline>
                                                    </span></p>
                                            </div>
                                        </div>
                                    </div><!-- .vcb-article -->
                                    <textarea style="display:none;" id="articleBody_57" readonly="true"></textarea>

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection


