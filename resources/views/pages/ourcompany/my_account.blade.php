@extends('layout')
@section('content')

<section class="special-products">
    <div class="container">
        <section id="wrapper">
            <div class="container">
                <div class="row">
                    <div id="columns_inner">
                        <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                            <div class="block-categories block">
                                <h4 class="block_title hidden-md-down">
                                    <a href="{{'/'}}">Home</a>
                                </h4>
                                <h4 class="block_title hidden-lg-up" data-target="#block_categories_toggle"
                                    data-toggle="collapse">
                                    <a href="{{'/'}}">Home</a>
                                    <span class="pull-xs-right">
                                        <span class="navbar-toggler collapse-icons">
                                            <i class="material-icons add"></i>
                                            <i class="material-icons remove"></i>
                                        </span>
                                    </span>
                                </h4>
                                <div id="block_categories_toggle" class="block_content collapse">
                                    <ul class="category-top-menu">
                                        @foreach($category as $key => $cate)
                                        <li class="category" id="category-3">
                                            <a class="dropdown-item"
                                                href="{{URL::to('/category/'.$cate->slug_category_product)}}"
                                                data-depth="0">
                                                {{$cate->category_name}}
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                            <div class="sidebar-specials block">
                                <h4 class="block_title hidden-md-down">Sale Products</h4>
                                <h4 class="block_title hidden-lg-up" data-target="#block_specials_toggle"
                                    data-toggle="collapse">Special Products
                                    <span class="pull-xs-right">
                                        <span class="navbar-toggler collapse-icons">
                                            <i class="material-icons add"></i>
                                            <i class="material-icons remove"></i>
                                        </span>
                                    </span>
                                </h4>

                                <div class="block_content collapse" id="block_specials_toggle">
                                    <div class="products clearfix">
                                        @foreach($all_sale as $key => $cart)
                                        <div class="product-item">
                                            <div class="left-part">

                                                <a href="{{URL::to('/product/'.$cart->product_slug)}}">
                                                    <span class="product-image media-middle">
                                                        <img src="{{URL::to('public/uploads/product/'.$cart->product_image)}}"
                                                            width="90" alt="" />
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="right-part">
                                                <div class="product-description">

                                                    <h1 class="h3 product-title" itemprop="name"><a href=""></a></h1>
                                                    <div class="product-price-and-shipping">
                                                        <p>{{$cart->product_name}}</p>
                                                        <span style="text-decoration-line: line-through"
                                                            itemprop="price "
                                                            class="regular-price">{{'$'.(number_format($cart->product_price))}}</span>
                                                        <span
                                                            class="discount-percentage">{{'-$'.(number_format($cart->product_sale_price))}}</span>
                                                        <span
                                                            class="price">{{'$'.(number_format($cart->product_price - $cart->product_sale_price))}}</span>
                                                    </div>
                                                </div>
                                                <div class="product-actions">
                                                    <form>
                                                        @csrf
                                                        <input type="hidden" value="{{$cart->product_id}}"
                                                            class="cart_product_id_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_name}}"
                                                            class="cart_product_name_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_quantity}}"
                                                            class="cart_product_quantity_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_image}}"
                                                            class="cart_product_image_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_price}}"
                                                            class="cart_product_price_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_sale_price}}"
                                                            class="cart_product_sale_price_{{$cart->product_id}}">
                                                        <input type="hidden" value="1"
                                                            class="cart_product_qty_{{$cart->product_id}}">

                                                        <input type="button" value="Add to cart"
                                                            class="btn btn-default add-to-cart"
                                                            data-id_product="{{$cart->product_id}}" name="add-to-cart">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="clearfix">

                                        <a href="{{URL::to('/product-sale/sale')}}" class="allproducts">All sale
                                            products</a>


                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
                            <section id="main">
                                <header class="page-header">
                                    <h1>
                                        Your account
                                    </h1>
                                </header>
                                <section id="content" class="page-content">
                                    <aside id="notifications">
                                        <div class="container">
                                        </div>
                                    </aside>
                                    <div class="row">
                                        <div class="links">
                                            @if(Session::get('customer_id')==true)
                                            <a class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="identity-link"
                                                href="{{('/information')}}">
                                                <span class="link-item border-account" >
                                                    <i class="material-icons"></i>
                                                    Information
                                                </span>
                                            </a>
                                            @else
                                            <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="identity-link"
                                            href="{{('/sign-in')}}">
                                            <span class="link-item border-account" >
                                                <i class="material-icons"></i>
                                                Information
                                            </span>
                                            </a>
                                            @endif


                                            {{-- <a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="addresses-link"
                                                href="javacript:">
                                                <span class="link-item" style="    display: block;height: 100%;padding: 40px;border: 1px solid #ececec;">
                                                    <i class="material-icons"></i>
                                                    Addresses
                                                </span>
                                            </a> --}}
                                            <style>
                                                .border-account{display: block;height: 100%;padding:40px;border: 1px solid #ececec}
                                                .border-account:hover{border: 1px solid #004167}
                                            </style>
                                            <a class="col-lg-6 col-md-6 col-sm-6 col-xs-12" id="history-link"
                                        href="{{'/order-history'}}">
                                                <span class="link-item border-account" >
                                                    <i class="material-icons"></i>
                                                    Order history
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </section>
                                <footer class="page-footer">


                                    <div class="text-xs-center">
                                        <?php
                                        $customer_id = Session::get('customer_id');
                                        if($customer_id!=NULL){
                                    ?>
                                   <a href="{{URL::to('/logout-checkout')}}"><i class="fa fa-unlock"></i> sign
                                            out</a>

                                    <?php
                                        }else{
                                    ?>
                                   <a href="{{URL::to('/sign-in')}}"><i class="fa fa-lock"></i> sign in</a>
                                    <?php
                                        }
                                    ?>

                                    </div>


                                </footer>


                            </section>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>


@endsection





