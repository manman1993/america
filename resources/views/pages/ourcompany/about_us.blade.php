@extends('layout')
@section('content')
<section class="special-products">
    <div class="container">
        <div class="row">
            <h2 class="h1 products-section-title">
                <span>about us</span>
            </h2>
            <div class="products hb-animate-element right-to-left">
                <div class="product_special">

                    <table width="100%" border="0" cellspacing="5" cellpadding="5" align="center">
                        <tbody>
                            <tr>
                                <td>
                                    <div style="text-align: left">
                                        <h3>
                                            <p class="MsoNormal" style="margin-bottom: 0.0001pt;"><b>
                                                    <inline style="font-family: Arial; font-size: 12px;">HANDMADE</inline>
                                                </b></p>
                                        </h3>
                                        <p class="p2">
                                            <inline style="font-family: Arial; font-size: 12px;">Live it. Own it.
                                                 At <i>HANDMADE</i> we feel just like you do – in the
                                                moment and ready for fun.</inline>

                                        </p>
                                        <p class="p2">
                                            <inline style="font-family: Arial; font-size: 12px;">Our handcrafted jewelry
                                                is professionally designed, affordable and beautiful.</inline>
                                        </p>
                                        <p class="p2">
                                            <inline style="font-family: Arial; font-size: 12px;"> Our family-owned company is based on a simple idea – that every
                                                woman can look her best and sparkle guilt free.</inline>
                                        </p>

                                        <p
                                            style="margin-bottom: 20px; color: rgb(99, 99, 99); line-height: 1.5; font-family: Merriweather, Georgia, 'Times New Roman', Times, serif; background-color: rgb(255, 255, 255); outline: none;">
                                            <span style="font-family: Helvetica;">
                                                <inline style="font-family: Arial; font-size: 12px;"><strong></strong>
                                                </inline>
                                            </span></p>
                                        <p class="p2">
                                            <inline style="font-family: Arial; font-size: 12px;">Because for us, life
                                                isn’t about having a million dollars. It’s about feeling like we do.
                                            </inline>
                                        </p>
                                        <inline style="font-family: Arial; font-size: 12px;"><br></inline>
                                        <p
                                            style="margin-bottom: 20px; color: rgb(99, 99, 99); line-height: 1.5; font-family: Merriweather, Georgia, 'Times New Roman', Times, serif; background-color: rgb(255, 255, 255); outline: none;">
                                            <inline style="font-family: Arial; font-size: 12px;"><span
                                                    style="font-family: Helvetica;">Any questions or concerns, please
                                                    email us at <strong>handmadeofamerica@gmail.com
                                                    </strong></span><span style="font-family: Helvetica;">or mail to
                                                </span><span
                                                    style="font-family: Helvetica;"><strong><br></strong></span>
                                            </inline>
                                        </p>
                                        <inline style="font-family: Arial; font-size: 12px;"><b>HANDMADE</b><br>4311 tristan Ridge ln Katy ,tx 77449
                                            <br>handmadeofamerica@gmail.com</inline>
                                        <h3></h3>
                                    </div><!-- .vcb-article -->
                                    <textarea style="display:none;" id="articleBody_1" readonly="true"></textarea>

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection


