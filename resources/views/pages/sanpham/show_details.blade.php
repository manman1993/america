@extends('layout')
@section('content')
<section id="wrapper" style="padding: 15px">
    <div class="container">
        <div class="row">
            <div id="columns_inner">
                <div id="content-wrapper">
                    <section id="main" itemscope="">
                        <meta itemprop="url">
                        <div class="row">
                            <div class="col-md-6 product_left">

                                <ul id="imageGallery">
                                    @foreach($gallery as $key => $gal)
                                    <li class="product-cover"
                                        data-thumb="{{URL::to('/public/uploads/gallery/'.$gal->gallery_image)}}"
                                        data-src="{{URL::to('/public/uploads/gallery/'.$gal->gallery_image)}}">
                                        <img width="100%" alt="{{$gal->gallery_image}}"
                                            src="{{URL::to('/public/uploads/gallery/'.$gal->gallery_image)}}" />
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            @foreach($product_details as $key => $value)
                            <div class="col-md-6 product_right">
                                <h1 class="h1 productpage_title" itemprop="name">{{$value->product_name}}</h1>

                                <div class="product-prices">
                                    <?php
                                    if($value->product_sale_price !=0){
                                    ?>
                                    <div class="product-discount">
                                        Our Price:
                                        <span class="regular-price">{{'$'.(number_format($value->product_price))}}
                                        </span>
                                    </div><br>
                                    <div class="product-price h5 has-discount" itemprop="offers" itemscope="">
                                        <link itemprop="availability">
                                        <div class="current-price">
                                            <span itemprop="price">Savings:
                                                {{'-$'.(number_format($value->product_sale_price))}}</span>

                                        </div>
                                    </div><br>
                                    <div class="product-price h5 has-discount" itemprop="offers" itemscope="">
                                        <link itemprop="availability">
                                        <div class="current-price">
                                            <span itemprop="price">Sale Price:
                                                {{'$'.(number_format($value->product_price - $value->product_sale_price))}}</span>

                                        </div>
                                    </div>


                                    <?php
                                    }else{
                                    ?>
                                    <div class="product-price h5 has-discount" itemprop="offers" itemscope=""
                                        style="margin:25px 0">
                                        <link itemprop="availability">
                                        <div class="current-price">
                                            <span itemprop="price">Our Price:
                                                {{'$'.(number_format($value->product_price - $value->product_sale_price))}}</span>

                                        </div>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                    <div class="product-information">

                                        <div class="product-quantities" style="margin-top: 10px;margin-bottom: 1.5rem;">
                                            {{$value->product_quantity}} Items
                                            <label class="label">In stock</label>
                                        </div>
                                        <div class="product-actions">

                                            <form action="{{URL::to('/save-cart')}}" method="POST">
                                                @csrf
                                                <input type="hidden" value="{{$value->product_id}}"
                                                    class="cart_product_id_{{$value->product_id}}">
                                                <input type="hidden" value="{{$value->product_name}}"
                                                    class="cart_product_name_{{$value->product_id}}">
                                                <input type="hidden" value="{{$value->product_image}}"
                                                    class="cart_product_image_{{$value->product_id}}">
                                                <input type="hidden" value="{{$value->product_quantity}}"
                                                    class="cart_product_quantity_{{$value->product_id}}">
                                                <input type="hidden" value="{{$value->product_price}}"
                                                    class="cart_product_price_{{$value->product_id}}">
                                                <input type="hidden" value="{{$value->product_sale_price}}"
                                                    class="cart_product_sale_price_{{$value->product_id}}">
                                                <div class="product-variants">

                                                </div>

                                                <div class="product-add-to-cart">
                                                    <span class="control-label">Quantity</span>
                                                    <div class="product-quantity">
                                                        <div class="qty">

                                                            <div class="input-group">
                                                                <span class="input-group-addon" style="display: none;">

                                                                </span>

                                                                <input type="text" name="qty" id="quantity_wanted"
                                                                    value="1"
                                                                    class="input-group form-control cart_product_qty_{{$value->product_id}}"
                                                                    min="1" style="display: block;">
                                                                <input name="productid_hidden" type="hidden"
                                                                    value="{{$value->product_id}}" />
                                                                <span
                                                                    class="input-group-addon bootstrap-touchspin-postfix"
                                                                    style="display: none;"></span>
                                                                <span class="input-group-btn-vertical">
                                                                    <button
                                                                        class="btn btn-touchspin js-touchspin bootstrap-touchspin-up"
                                                                        type="button"><i
                                                                            class="material-icons touchspin-up"></i>
                                                                    </button>
                                                                    <button
                                                                        class="btn btn-touchspin js-touchspin bootstrap-touchspin-down"
                                                                        type="button"><i
                                                                            class="material-icons touchspin-down"></i>
                                                                    </button>
                                                                </span>

                                                            </div>
                                                        </div>
                                                        <div class="add">
                                                            <input type="button" value="+ADD TO CART"
                                                                class="btn btn-primary btn-sm add-to-cart"
                                                                data-id_product="{{$value->product_id}}"
                                                                name="add-to-cart">
                                                            <span id="product-availability">
                                                                <i class="material-icons product-available"></i>
                                                                In stock
                                                            </span>
                                                        </div>

                                                    </div>


                                                </div>
                                                <br>

                                                <span style="padding-right: 15px"><a target="_blank"
                                                        href="https://www.facebook.com/Handmadeofamerica-102382851720896"><i
                                                            style="border: 1px solid #e6e6e6;padding:5px 10px ;"
                                                            class="fa fa-facebook"></i></a></span>
                                                <span><a target="_blank"
                                                        href="https://www.instagram.com/handmadeofamerica/"><i
                                                            style="border: 1px solid #e6e6e6;padding:5px 7px;"
                                                            class="fa fa-instagram"></i></a></span>




                                            </form>

                                        </div>
                                        <div id="block-reassurance">
                                            <ul>
                                                <li>
                                                    <div class="block-reassurance-item">
                                                        <img src="images/icon-1.png"
                                                            alt="Security policy (edit with Customer reassurance module)">
                                                        <span class="h6">Security policy (edit with Customer
                                                            reassurance module)</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="block-reassurance-item">
                                                        <img src="images/icon-2.png"
                                                            alt="Delivery policy (edit with Customer reassurance module)">
                                                        <span class="h6">Delivery policy (edit with Customer
                                                            reassurance module)</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="block-reassurance-item">
                                                        <img src="images/icon3.png"
                                                            alt="Return policy (edit with Customer reassurance module)">
                                                        <span class="h6">Return policy (edit with Customer
                                                            reassurance module)</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            @endforeach

                            <section class="product-tabcontent">
                                <div class="tabs">
                                    <ul class="nav nav-tabs" style="border-bottom:none;">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#description11"
                                                style="background: none">
                                                Description
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#product-details"
                                                style="background: none">
                                                Product Details
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="tab-content">
                                        <div class="tab-pane fade in active" id="description11">
                                            <div class="product-description">
                                                {!!$meta_desc!!}
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="product-details"
                                            data-product="{&quot;id_shop_default&quot;:&quot;1&quot;,&quot;id_manufacturer&quot;:&quot;1&quot;,&quot;id_supplier&quot;:&quot;1&quot;,&quot;reference&quot;:&quot;demo_5&quot;,&quot;supplier_reference&quot;:&quot;&quot;,&quot;location&quot;:&quot;&quot;,&quot;width&quot;:&quot;0.000000&quot;,&quot;height&quot;:&quot;0.000000&quot;,&quot;depth&quot;:&quot;0.000000&quot;,&quot;weight&quot;:&quot;0.000000&quot;,&quot;quantity_discount&quot;:&quot;0&quot;,&quot;ean13&quot;:&quot;&quot;,&quot;isbn&quot;:&quot;&quot;,&quot;upc&quot;:&quot;&quot;,&quot;cache_is_pack&quot;:&quot;0&quot;,&quot;cache_has_attachments&quot;:&quot;0&quot;,&quot;is_virtual&quot;:&quot;0&quot;,&quot;state&quot;:&quot;1&quot;,&quot;id_category_default&quot;:&quot;11&quot;,&quot;id_tax_rules_group&quot;:&quot;1&quot;,&quot;on_sale&quot;:&quot;0&quot;,&quot;online_only&quot;:&quot;0&quot;,&quot;ecotax&quot;:{&quot;value&quot;:&quot;$0.00&quot;,&quot;amount&quot;:0,&quot;rate&quot;:0},&quot;minimal_quantity&quot;:&quot;1&quot;,&quot;price&quot;:&quot;$28.98&quot;,&quot;wholesale_price&quot;:&quot;9.150000&quot;,&quot;unity&quot;:&quot;&quot;,&quot;unit_price_ratio&quot;:&quot;0.000000&quot;,&quot;additional_shipping_cost&quot;:&quot;0.00&quot;,&quot;customizable&quot;:&quot;0&quot;,&quot;text_fields&quot;:&quot;0&quot;,&quot;uploadable_files&quot;:&quot;0&quot;,&quot;redirect_type&quot;:&quot;404&quot;,&quot;id_type_redirected&quot;:&quot;0&quot;,&quot;available_for_order&quot;:&quot;1&quot;,&quot;available_date&quot;:null,&quot;show_condition&quot;:&quot;0&quot;,&quot;condition&quot;:false,&quot;show_price&quot;:true,&quot;indexed&quot;:&quot;1&quot;,&quot;visibility&quot;:&quot;both&quot;,&quot;cache_default_attribute&quot;:&quot;58&quot;,&quot;advanced_stock_management&quot;:&quot;0&quot;,&quot;date_add&quot;:&quot;2018-02-17 00:31:01&quot;,&quot;date_upd&quot;:&quot;2018-02-17 02:06:25&quot;,&quot;pack_stock_type&quot;:&quot;3&quot;,&quot;meta_description&quot;:&quot;&quot;,&quot;meta_keywords&quot;:&quot;&quot;,&quot;meta_title&quot;:&quot;&quot;,&quot;link_rewrite&quot;:&quot;printed-summer-dress&quot;,&quot;name&quot;:&quot;et quasi architecto&quot;,&quot;description&quot;:&quot;<p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman's wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!<\/p>&quot;,&quot;description_short&quot;:&quot;<p>Long printed dress with thin adjustable straps. V-neckline and wiring under the bust with ruffles at the bottom of the dress.<\/p>&quot;,&quot;available_now&quot;:&quot;In stock&quot;,&quot;available_later&quot;:&quot;&quot;,&quot;id&quot;:10,&quot;id_product&quot;:10,&quot;out_of_stock&quot;:2,&quot;new&quot;:1,&quot;id_product_attribute&quot;:58,&quot;quantity_wanted&quot;:1,&quot;extraContent&quot;:[],&quot;allow_oosp&quot;:0,&quot;category&quot;:&quot;table-floor-lamps&quot;,&quot;category_name&quot;:&quot;table &amp; floor lamps&quot;,&quot;link&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?id_product=10&amp;id_product_attribute=0&amp;rewrite=printed-summer-dress&amp;controller=product&amp;id_lang=1&quot;,&quot;attribute_price&quot;:0,&quot;price_tax_exc&quot;:28.98,&quot;price_without_reduction&quot;:30.5,&quot;reduction&quot;:1.525,&quot;specific_prices&quot;:{&quot;id_specific_price&quot;:&quot;4&quot;,&quot;id_specific_price_rule&quot;:&quot;0&quot;,&quot;id_cart&quot;:&quot;0&quot;,&quot;id_product&quot;:&quot;10&quot;,&quot;id_shop&quot;:&quot;0&quot;,&quot;id_shop_group&quot;:&quot;0&quot;,&quot;id_currency&quot;:&quot;0&quot;,&quot;id_country&quot;:&quot;0&quot;,&quot;id_group&quot;:&quot;0&quot;,&quot;id_customer&quot;:&quot;0&quot;,&quot;id_product_attribute&quot;:&quot;0&quot;,&quot;price&quot;:&quot;-1.000000&quot;,&quot;from_quantity&quot;:&quot;1&quot;,&quot;reduction&quot;:&quot;0.050000&quot;,&quot;reduction_tax&quot;:&quot;1&quot;,&quot;reduction_type&quot;:&quot;percentage&quot;,&quot;from&quot;:&quot;0000-00-00 00:00:00&quot;,&quot;to&quot;:&quot;0000-00-00 00:00:00&quot;,&quot;score&quot;:&quot;0&quot;},&quot;quantity&quot;:97,&quot;quantity_all_versions&quot;:496,&quot;id_image&quot;:&quot;en-default&quot;,&quot;features&quot;:[{&quot;name&quot;:&quot;Compositions&quot;,&quot;value&quot;:&quot;Viscose&quot;,&quot;id_feature&quot;:&quot;5&quot;},{&quot;name&quot;:&quot;Styles&quot;,&quot;value&quot;:&quot;Casual&quot;,&quot;id_feature&quot;:&quot;6&quot;},{&quot;name&quot;:&quot;Properties&quot;,&quot;value&quot;:&quot;Maxi Dress&quot;,&quot;id_feature&quot;:&quot;7&quot;}],&quot;attachments&quot;:[],&quot;virtual&quot;:0,&quot;pack&quot;:0,&quot;packItems&quot;:[],&quot;nopackprice&quot;:0,&quot;customization_required&quot;:false,&quot;attributes&quot;:{&quot;1&quot;:{&quot;id_attribute&quot;:&quot;1&quot;,&quot;id_attribute_group&quot;:&quot;1&quot;,&quot;name&quot;:&quot;S&quot;,&quot;group&quot;:&quot;Size&quot;,&quot;reference&quot;:&quot;&quot;,&quot;ean13&quot;:&quot;&quot;,&quot;isbn&quot;:&quot;&quot;,&quot;upc&quot;:&quot;&quot;},&quot;3&quot;:{&quot;id_attribute&quot;:&quot;16&quot;,&quot;id_attribute_group&quot;:&quot;3&quot;,&quot;name&quot;:&quot;Yellow&quot;,&quot;group&quot;:&quot;Color&quot;,&quot;reference&quot;:&quot;&quot;,&quot;ean13&quot;:&quot;&quot;,&quot;isbn&quot;:&quot;&quot;,&quot;upc&quot;:&quot;&quot;}},&quot;rate&quot;:0,&quot;tax_name&quot;:&quot;&quot;,&quot;ecotax_rate&quot;:0,&quot;unit_price&quot;:&quot;&quot;,&quot;customizations&quot;:{&quot;fields&quot;:[]},&quot;id_customization&quot;:0,&quot;is_customizable&quot;:false,&quot;show_quantities&quot;:true,&quot;quantity_label&quot;:&quot;Items&quot;,&quot;quantity_discounts&quot;:[],&quot;customer_group_discount&quot;:0,&quot;weight_unit&quot;:&quot;kg&quot;,&quot;images&quot;:[{&quot;bySize&quot;:{&quot;cart_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-cart_default.jpg&quot;,&quot;width&quot;:80,&quot;height&quot;:85},&quot;small_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-small_default.jpg&quot;,&quot;width&quot;:98,&quot;height&quot;:104},&quot;home_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-home_default.jpg&quot;,&quot;width&quot;:281,&quot;height&quot;:299},&quot;medium_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-medium_default.jpg&quot;,&quot;width&quot;:452,&quot;height&quot;:481},&quot;large_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-large_default.jpg&quot;,&quot;width&quot;:600,&quot;height&quot;:638}},&quot;small&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-cart_default.jpg&quot;,&quot;width&quot;:80,&quot;height&quot;:85},&quot;medium&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-home_default.jpg&quot;,&quot;width&quot;:281,&quot;height&quot;:299},&quot;large&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-large_default.jpg&quot;,&quot;width&quot;:600,&quot;height&quot;:638},&quot;legend&quot;:&quot;&quot;,&quot;cover&quot;:&quot;1&quot;,&quot;id_image&quot;:&quot;111&quot;,&quot;position&quot;:&quot;1&quot;,&quot;associatedVariants&quot;:[]},{&quot;bySize&quot;:{&quot;cart_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/2\/112-cart_default.jpg&quot;,&quot;width&quot;:80,&quot;height&quot;:85},&quot;small_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/2\/112-small_default.jpg&quot;,&quot;width&quot;:98,&quot;height&quot;:104},&quot;home_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/2\/112-home_default.jpg&quot;,&quot;width&quot;:281,&quot;height&quot;:299},&quot;medium_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/2\/112-medium_default.jpg&quot;,&quot;width&quot;:452,&quot;height&quot;:481},&quot;large_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/2\/112-large_default.jpg&quot;,&quot;width&quot;:600,&quot;height&quot;:638}},&quot;small&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/2\/112-cart_default.jpg&quot;,&quot;width&quot;:80,&quot;height&quot;:85},&quot;medium&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/2\/112-home_default.jpg&quot;,&quot;width&quot;:281,&quot;height&quot;:299},&quot;large&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/2\/112-large_default.jpg&quot;,&quot;width&quot;:600,&quot;height&quot;:638},&quot;legend&quot;:&quot;&quot;,&quot;cover&quot;:null,&quot;id_image&quot;:&quot;112&quot;,&quot;position&quot;:&quot;2&quot;,&quot;associatedVariants&quot;:[]},{&quot;bySize&quot;:{&quot;cart_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/3\/113-cart_default.jpg&quot;,&quot;width&quot;
                                                :80,&quot;height&quot;:85},&quot;small_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/3\/113-small_default.jpg&quot;,&quot;width&quot;:98,&quot;height&quot;:104},&quot;home_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/3\/113-home_default.jpg&quot;,&quot;width&quot;:281,&quot;height&quot;:299},&quot;medium_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/3\/113-medium_default.jpg&quot;,&quot;width&quot;:452,&quot;height&quot;:481},&quot;large_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/3\/113-large_default.jpg&quot;,&quot;width&quot;:600,&quot;height&quot;:638}},&quot;small&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/3\/113-cart_default.jpg&quot;,&quot;width&quot;:80,&quot;height&quot;:85},&quot;medium&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/3\/113-home_default.jpg&quot;,&quot;width&quot;:281,&quot;height&quot;:299},&quot;large&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/3\/113-large_default.jpg&quot;,&quot;width&quot;:600,&quot;height&quot;:638},&quot;legend&quot;:&quot;&quot;,&quot;cover&quot;:null,&quot;id_image&quot;:&quot;113&quot;,&quot;position&quot;:&quot;3&quot;,&quot;associatedVariants&quot;:[]},{&quot;bySize&quot;:{&quot;cart_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/4\/114-cart_default.jpg&quot;,&quot;width&quot;:80,&quot;height&quot;:85},&quot;small_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/4\/114-small_default.jpg&quot;,&quot;width&quot;:98,&quot;height&quot;:104},&quot;home_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/4\/114-home_default.jpg&quot;,&quot;width&quot;:281,&quot;height&quot;:299},&quot;medium_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/4\/114-medium_default.jpg&quot;,&quot;width&quot;:452,&quot;height&quot;:481},&quot;large_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/4\/114-large_default.jpg&quot;,&quot;width&quot;:600,&quot;height&quot;:638}},&quot;small&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/4\/114-cart_default.jpg&quot;,&quot;width&quot;:80,&quot;height&quot;:85},&quot;medium&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/4\/114-home_default.jpg&quot;,&quot;width&quot;:281,&quot;height&quot;:299},&quot;large&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/4\/114-large_default.jpg&quot;,&quot;width&quot;:600,&quot;height&quot;:638},&quot;legend&quot;:&quot;&quot;,&quot;cover&quot;:null,&quot;id_image&quot;:&quot;114&quot;,&quot;position&quot;:&quot;4&quot;,&quot;associatedVariants&quot;:[]},{&quot;bySize&quot;:{&quot;cart_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/5\/115-cart_default.jpg&quot;,&quot;width&quot;:80,&quot;height&quot;:85},&quot;small_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/5\/115-small_default.jpg&quot;,&quot;width&quot;:98,&quot;height&quot;:104},&quot;home_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/5\/115-home_default.jpg&quot;,&quot;width&quot;:281,&quot;height&quot;:299},&quot;medium_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/5\/115-medium_default.jpg&quot;,&quot;width&quot;:452,&quot;height&quot;:481},&quot;large_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/5\/115-large_default.jpg&quot;,&quot;width&quot;:600,&quot;height&quot;:638}},&quot;small&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/5\/115-cart_default.jpg&quot;,&quot;width&quot;:80,&quot;height&quot;:85},&quot;medium&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/5\/115-home_default.jpg&quot;,&quot;width&quot;:281,&quot;height&quot;:299},&quot;large&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/5\/115-large_default.jpg&quot;,&quot;width&quot;:600,&quot;height&quot;:638},&quot;legend&quot;:&quot;&quot;,&quot;cover&quot;:null,&quot;id_image&quot;:&quot;115&quot;,&quot;position&quot;:&quot;5&quot;,&quot;associatedVariants&quot;:[]}],&quot;cover&quot;:{&quot;bySize&quot;:{&quot;cart_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-cart_default.jpg&quot;,&quot;width&quot;:80,&quot;height&quot;:85},&quot;small_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-small_default.jpg&quot;,&quot;width&quot;:98,&quot;height&quot;:104},&quot;home_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-home_default.jpg&quot;,&quot;width&quot;:281,&quot;height&quot;:299},&quot;medium_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-medium_default.jpg&quot;,&quot;width&quot;:452,&quot;height&quot;:481},&quot;large_default&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-large_default.jpg&quot;,&quot;width&quot;:600,&quot;height&quot;:638}},&quot;small&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-cart_default.jpg&quot;,&quot;width&quot;:80,&quot;height&quot;:85},&quot;medium&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-home_default.jpg&quot;,&quot;width&quot;:281,&quot;height&quot;:299},&quot;large&quot;:{&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/img\/p\/1\/1\/1\/111-large_default.jpg&quot;,&quot;width&quot;:600,&quot;height&quot;:638},&quot;legend&quot;:&quot;&quot;,&quot;cover&quot;:&quot;1&quot;,&quot;id_image&quot;:&quot;111&quot;,&quot;position&quot;:&quot;1&quot;,&quot;associatedVariants&quot;:[]},&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?id_product=10&amp;id_product_attribute=58&amp;rewrite=printed-summer-dress&amp;controller=product&amp;id_lang=1#\/1-size-s\/16-color-yellow&quot;,&quot;canonical_url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?id_product=10&amp;rewrite=printed-summer-dress&amp;controller=product&amp;id_lang=1&quot;,&quot;has_discount&quot;:true,&quot;discount_type&quot;:&quot;percentage&quot;,&quot;discount_percentage&quot;:&quot;-5%&quot;,&quot;discount_percentage_absolute&quot;:&quot;5%&quot;,&quot;discount_amount&quot;:&quot;$1.53&quot;,&quot;price_amount&quot;:28.98,&quot;regular_price_amount&quot;:30.5,&quot;regular_price&quot;:&quot;$30.50&quot;,&quot;discount_to_display&quot;:&quot;$1.53&quot;,&quot;unit_price_full&quot;:&quot;&quot;,&quot;add_to_cart_url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=cart&amp;add=1&amp;id_product=10&amp;id_product_attribute=58&amp;token=37a35808bd3e31a6c88db859b1dc2e5a&quot;,&quot;main_variants&quot;:[{&quot;id_product_attribute&quot;:61,&quot;texture&quot;:&quot;&quot;,&quot;id_product&quot;:&quot;10&quot;,&quot;name&quot;:&quot;Black&quot;,&quot;add_to_cart_url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=cart&amp;add=1&amp;id_product=10&amp;id_product_attribute=61&amp;token=37a35808bd3e31a6c88db859b1dc2e5a&quot;,&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?id_product=10&amp;id_product_attribute=61&amp;rewrite=printed-summer-dress&amp;controller=product&amp;id_lang=1#\/1-size-s\/11-color-black&quot;,&quot;type&quot;:&quot;color&quot;,&quot;html_color_code&quot;:&quot;#434A54&quot;},{&quot;id_product_attribute&quot;:60,&quot;texture&quot;:&quot;&quot;,&quot;id_product&quot;:&quot;10&quot;,&quot;name&quot;:&quot;Orange&quot;,&quot;add_to_cart_url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=cart&amp;add=1&amp;id_product=10&amp;id_product_attribute=60&amp;token=37a35808bd3e31a6c88db859b1dc2e5a&quot;,&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?id_product=10&amp;id_product_attribute=60&amp;rewrite=printed-summer-dress&amp;controller=product&amp;id_lang=1#\/1-size-s\/13-color-orange&quot;,&quot;type&quot;:&quot;color&quot;,&quot;html_color_code&quot;:&quot;#F39C11&quot;},{&quot;id_product_attribute&quot;:59,&quot;texture&quot;:&quot;&quot;,&quot;id_product&quot;:&quot;10&quot;,&quot;name&quot;:&quot;Blue&quot;,&quot;add_to_cart_url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=cart&amp;add=1&amp;id_product=10&amp;id_product_attribute=59&amp;token=37a35808bd3e31a6c88db859b1dc2e5a&quot;,&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?id_product=10&amp;id_product_attribute=59&amp;rewrite=printed-summer-dress&amp;controller=product&amp;id_lang=1#\/1-size-s\/14-color-blue&quot;,&quot;type&quot;:&quot;color&quot;,&quot;html_color_code&quot;:&quot;#5D9CEC&quot;},{&quot;id_product_attribute&quot;:62,&quot;texture&quot;:&quot;&quot;,&quot;id_product&quot;:&quot;10&quot;,&quot;name&quot;:&quot;Yellow&quot;,&quot;add_to_cart_url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?controller=cart&amp;add=1&amp;id_product=10&amp;id_product_attribute=62&amp;token=37a35808bd3e31a6c88db859b1dc2e5a&quot;,&quot;url&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?id_product=10&amp;id_product_attribute=62&amp;rewrite=printed-summer-dress&amp;controller=product&amp;id_lang=1#\/2-size-m\/16-color-yellow&quot;,&quot;type&quot;:&quot;color&quot;,&quot;html_color_code&quot;:&quot;#F1C40F&quot;}],&quot;flags&quot;:{&quot;discount&quot;:{&quot;type&quot;:&quot;discount&quot;,&quot;label&quot;:&quot;Reduced price&quot;},&quot;new&quot;:{&quot;type&quot;:&quot;new&quot;,&quot;label&quot;:&quot;New&quot;}},&quot;labels&quot;:{&quot;tax_short&quot;:&quot;(tax excl.)&quot;,&quot;tax_long&quot;:&quot;Tax excluded&quot;},&quot;show_availability&quot;:true,&quot;availability_date&quot;:null,&quot;availability_message&quot;:&quot;In stock&quot;,&quot;availability&quot;:&quot;available&quot;,&quot;reference_to_display&quot;:&quot;demo_5&quot;,&quot;embedded_attributes&quot;:{&quot;id_shop_default&quot;:&quot;1&quot;,&quot;id_manufacturer&quot;:&quot;1&quot;,&quot;id_supplier&quot;:&quot;1&quot;,&quot;reference&quot;:&quot;demo_5&quot;,&quot;is_virtual&quot;:&quot;0&quot;,&quot;id_category_default&quot;:&quot;11&quot;,&quot;on_sale&quot;:&quot;0&quot;,&quot;online_only&quot;:&quot;0&quot;,&quot;ecotax&quot;:0,&quot;minimal_quantity&quot;:&quot;1&quot;,&quot;price&quot;:28.975,&quot;unity&quot;:&quot;&quot;,&quot;unit_price_ratio&quot;:&quot;0.000000&quot;,&quot;additional_shipping_cost&quot;:&quot;0.00&quot;,&quot;customizable&quot;:&quot;0&quot;,&quot;text_fields&quot;:&quot;0&quot;,&quot;uploadable_files&quot;:&quot;0&quot;,&quot;redirect_type&quot;:&quot;404&quot;,&quot;id_type_redirected&quot;:&quot;0&quot;,&quot;available_for_order&quot;:&quot;1&quot;,&quot;available_date&quot;:null,&quot;show_condition&quot;:&quot;0&quot;,&quot;condition&quot;:&quot;new&quot;,&quot;show_price&quot;:&quot;1&quot;,&quot;indexed&quot;:&quot;1&quot;,&quot;visibility&quot;:&quot;both&quot;,&quot;cache_default_attribute&quot;:&quot;58&quot;,&quot;advanced_stock_management&quot;:&quot;0&quot;,&quot;date_add&quot;:&quot;2018-02-17 00:31:01&quot;,&quot;date_upd&quot;:&quot;2018-02-17 02:06:25&quot;,&quot;pack_stock_type&quot;:&quot;3&quot;,&quot;meta_description&quot;:&quot;&quot;,&quot;meta_keywords&quot;:&quot;&quot;,&quot;meta_title&quot;:&quot;&quot;,&quot;link_rewrite&quot;:&quot;printed-summer-dress&quot;,&quot;name&quot;:&quot;et quasi architecto&quot;,&quot;description&quot;:&quot;<p>Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman's wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!<\/p>&quot;,&quot;description_short&quot;:&quot;<p>Long printed dress with thin adjustable straps. V-neckline and wiring under the bust with ruffles at the bottom of the dress.<\/p>&quot;,&quot;available_now&quot;:&quot;In stock&quot;,&quot;available_later&quot;:&quot;&quot;,&quot;id&quot;:10,&quot;id_product&quot;:10,&quot;out_of_stock&quot;:2,&quot;new&quot;:1,&quot;id_product_attribute&quot;:58,&quot;quantity_wanted&quot;:1,&quot;extraContent&quot;:[],&quot;allow_oosp&quot;:0,&quot;category&quot;:&quot;table-floor-lamps&quot;,&quot;category_name&quot;:&quot;table &amp; floor lamps&quot;,&quot;link&quot;:&quot;https:\/\/capricathemes.com\/prestashop\/PRS05\/PRS050093\/index.php?id_product=10&amp;id_product_attribute=0&amp;rewrite=printed-summer-dress&amp;controller=product&amp;id_lang=1&quot;,&quot;attribute_price&quot;:0,&quot;price_tax_exc&quot;:28.98,&quot;price_without_reduction&quot;:30.5,&quot;reduction&quot;:1.525,&quot;specific_prices&quot;:{&quot;id_specific_price&quot;:&quot;4&quot;,&quot;id_specific_price_rule&quot;:&quot;0&quot;,&quot;id_cart&quot;:&quot;0&quot;,&quot;id_product&quot;:&quot;10&quot;,&quot;id_shop&quot;:&quot;0&quot;,&quot;id_shop_group&quot;:&quot;0&quot;,&quot;id_currency&quot;:&quot;0&quot;,&quot;id_country&quot;:&quot;0&quot;,&quot;id_group&quot;:&quot;0&quot;,&quot;id_customer&quot;:&quot;0&quot;,&quot;id_product_attribute&quot;:&quot;0&quot;,&quot;price&quot;:&quot;-1.000000&quot;,&quot;from_quantity&quot;:&quot;1&quot;,&quot;reduction&quot;:&quot;0.050000&quot;,&quot;reduction_tax&quot;:&quot;1&quot;,&quot;reduction_type&quot;:&quot;percentage&quot;,&quot;from&quot;:&quot;0000-00-00 00:00:00&quot;,&quot;to&quot;:&quot;0000-00-00 00:00:00&quot;,&quot;score&quot;:&quot;0&quot;},&quot;quantity&quot;:97,&quot;quantity_all_versions&quot;:496,&quot;id_image&quot;:&quot;en-default&quot;,&quot;features&quot;:[{&quot;name&quot;:&quot;Compositions&quot;,&quot;value&quot;:&quot;Viscose&quot;,&quot;id_feature&quot;:&quot;5&quot;},{&quot;name&quot;:&quot;Styles&quot;,&quot;value&quot;:&quot;Casual&quot;,&quot;id_feature&quot;:&quot;6&quot;},{&quot;name&quot;:&quot;Properties&quot;,&quot;value&quot;:&quot;Maxi Dress&quot;,&quot;id_feature&quot;:&quot;7&quot;}],&quot;attachments&quot;:[],&quot;virtual&quot;:0,&quot;pack&quot;:0,&quot;packItems&quot;:[],&quot;nopackprice&quot;:0,&quot;customization_required&quot;:false,&quot;attributes&quot;:{&quot;1&quot;:{&quot;id_attribute&quot;:&quot;1&quot;,&quot;id_attribute_group&quot;:&quot;1&quot;,&quot;name&quot;:&quot;S&quot;,&quot;group&quot;:&quot;Size&quot;,&quot;reference&quot;:&quot;&quot;,&quot;ean13&quot;:&quot;&quot;,&quot;isbn&quot;:&quot;&quot;,&quot;upc&quot;:&quot;&quot;},&quot;3&quot;:{&quot;id_attribute&quot;:&quot;16&quot;,&quot;id_attribute_group&quot;:&quot;3&quot;,&quot;name&quot;:&quot;Yellow&quot;,&quot;group&quot;:&quot;Color&quot;,&quot;reference&quot;:&quot;&quot;,&quot;ean13&quot;:&quot;&quot;,&quot;isbn&quot;:&quot;&quot;,&quot;upc&quot;:&quot;&quot;}},&quot;rate&quot;:0,&quot;tax_name&quot;:&quot;&quot;,&quot;ecotax_rate&quot;:0,&quot;unit_price&quot;:0,&quot;customizations&quot;:{&quot;fields&quot;:[]},&quot;id_customization&quot;:0,&quot;is_customizable&quot;:false,&quot;show_quantities&quot;:true,&quot;quantity_label&quot;:&quot;Items&quot;,&quot;quantity_discounts&quot;:[],&quot;customer_group_discount&quot;:0}}">
                                            <div class="product-out-of-stock">
                                            </div>
                                            <section class="product-features">
                                                {!!$value->product_content !!}
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!-- Define Number of product for SLIDER -->




                    </section>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="row">
        <section class="ct-hometabcontent">


            <h1 class="h1 products-section-title"><span>You Might Also Like</span></h1>


            <div class="tabs">


                <div class="tab-content hb-animate-element bottom-to-top hb-in-viewport">

                    <div id="newProduct" class="ct_productinner tab-pane active" aria-expanded="true">
                        <section class="newproducts clearfix">
                            <h2 class="h1 products-section-title text-uppercase">
                                Sale products
                            </h2>

                            <div class="products">
                                <div class="product_new">
                                    <!-- Define Number of product for SLIDER -->
                                    <ul id="newproduct-grid-{!!$category_id!!}"
                                        class="bestseller_grid product_list grid row gridcount">
                                        @csrf
                                    </ul>


                                    <div class="modal fade" id="myModal" role="dialog">
                                        <div class="modal-dialog modal-lg">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close"
                                                        data-dismiss="modal">&times;</button>
                                                    <h1 id="name_modal" class="modal-title">Modal Header</h1>
                                                </div>

                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 hidden-xs-down" id="gallery_home">
                                                            <div class="images-container">
                                                                <div class="product-cover">
                                                                    <img id="img_modal" class="js-qv-product-cover"
                                                                        src="" alt="" title="" style="width:100%;"
                                                                        itemprop="image">

                                                                    <div class="layer hidden-sm-down"
                                                                        data-toggle="modal"
                                                                        data-target="#product-modal1">
                                                                        <i class="material-icons zoom-in"></i>
                                                                    </div>
                                                                </div>
                                                                <!-- Define Number of product for SLIDER -->

                                                            </div>
                                                            <div class="arrows js-arrows" style="display: none;">
                                                                <i class="material-icons arrow-up js-arrow-up"></i>
                                                                <i class="material-icons arrow-down js-arrow-down"></i>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">

                                                            <div class="product-prices" style="text-align: left">
                                                                <div class="product-price h5 " itemprop="offers" itemscope="" itemtype="">
                                                                    <link itemprop="availability">

                                                                    <meta itemprop="priceCurrency" content="USD">
                                                                    <div class="current-price" style="font-size: 18px">
                                                                        <div id="price"></div>
                                                                    </div>
                                                                    <div id="product_quickview_desc_1"></div>
                                                                </div>
                                                                <div class="tax-shipping-delivery-label">
                                                                </div>
                                                            </div>
                                                            <div class="product-actions">
                                                                <form>
                                                                    @csrf
                                                                    <div id="product_quickview_value"></div>
                                                                    <input type="hidden" name="token"
                                                                        value="37a35808bd3e31a6c88db859b1dc2e5a">
                                                                    <input type="hidden" name="id_product" value="1"
                                                                        id="product_page_product_id">
                                                                    <input type="hidden" name="id_customization"
                                                                        value="0" id="product_customization_id">

                                                                    <div class="product-add-to-cart">
                                                                        <span class="control-label">Quantity</span>
                                                                        <div class="product-quantity">
                                                                            <div class="qty">
                                                                                <div
                                                                                    class="input-group bootstrap-touchspin">
                                                                                    <span
                                                                                        class="input-group-addon bootstrap-touchspin-prefix"
                                                                                        style="display: none;"></span><input
                                                                                        type="text" name="qty"
                                                                                        id="quantity_wanted" value="1"
                                                                                        class="input-group form-control"
                                                                                        min="1"
                                                                                        style="display: block;"><span
                                                                                        class="input-group-addon bootstrap-touchspin-postfix"
                                                                                        style="display: none;"></span><span
                                                                                        class="input-group-btn-vertical"><button
                                                                                            class="btn btn-touchspin js-touchspin bootstrap-touchspin-up"
                                                                                            type="button"><i
                                                                                                class="material-icons touchspin-up"></i></button><button
                                                                                            class="btn btn-touchspin js-touchspin bootstrap-touchspin-down"
                                                                                            type="button"><i
                                                                                                class="material-icons touchspin-down"></i></button></span>
                                                                                </div>
                                                                            </div>

                                                                            <div class="add">

                                                                                <input id="data-id" type="button"
                                                                                    value="Buy now"
                                                                                    class="btn btn-primary add-to-cart-quickview"
                                                                                    data-id_product
                                                                                    name="add-to-cart-quickview">

                                                                                <span id="product-availability">
                                                                                    <i
                                                                                        class="material-icons product-available"></i>
                                                                                    In stock
                                                                                </span>
                                                                            </div>

                                                                            <div id="beforesend_quickview_1"></div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <p class="product-minimal-quantity">
                                                                        </p>
                                                                    </div>
                                                                    <input class="product-refresh"
                                                                        data-url-update="false" name="refresh"
                                                                        type="submit" value="Refresh" hidden="">
                                                                </form>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary redirect-cart"
                                                        data-dismiss="modal">Go to
                                                        cart</button>
                                                    <button type="button" class="btn btn-default"
                                                        data-dismiss="modal">Close</button>
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                    <input type="hidden" id="numpage-{!!$category_id!!}"
                                        name="numpage-{!!$category_id!!}" value="0">
                                    <button id="load_more_button-{!!$category_id!!}" type="button"
                                        class="all-product-link pull-xs-left pull-md-right h4">Load
                                        more</button>
                                </div>
                            </div>
                        </section>
                    </div>

                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function(){


                        var _token = $('input[name="_token"]').val();
                        var product_slug = '{!!$product_slug!!}';
                        load_data(_token,'','','{!!$category_id!!}');

                        function load_data(_token,branch,category,id){
                        var _num = parseInt($('input[id="numpage-'+id+'"]').val()) + 4;

                        $.ajax({
                            url : '{{url('/loadmore')}}',
                            method: 'POST',
                            data:{ _token:_token,"numpage":_num,"category": category,"branch": branch,"id":id,"product_slug":product_slug,"field_sort" : 'none', "type_sort": 'none'},

                            success:function(data){
                                if(category != ''){
                                    $('#newproduct-grid-'+category).html(data.out);
                                    $('#numpage-'+category).val(data.num);
                                }else if(branch != ''){
                                    $('#newproduct-grid-'+branch).html(data.out);
                                    $('#numpage-'+branch).val(data.num);
                                }else{
                                    $('#newproduct-grid-'+id).html(data.out);
                                    $('#numpage-'+id).val(data.num);
                                }
                                // alert(id);
                                if(data.num >= data.count){
                                    document.getElementById("load_more_button-"+id).style.display = "none";
                                }

                                $('.quick-view').click(function(){
                                    var id = $(this).data('id_product');
                                    var cart_product_id = $('.cart_product_id_' + id).val();
                                    var cart_product_name = $('.cart_product_name_' + id).val();
                                    var cart_product_image = $('.cart_product_image_' + id).val();
                                    var cart_product_quantity = $('.cart_product_quantity_' + id).val();
                                    var cart_product_price = $('.cart_product_price_' + id).val();
                                    var cart_product_sale_price = $('.cart_product_sale_price_' + id).val();
                                    var cart_product_qty = $('.cart_product_qty_' + id).val();
                                    var _token = $('input[name="_token"]').val();
                                    if(cart_product_price!=(cart_product_price - cart_product_sale_price)){
                                        $('#price').html(
                                            '<span style="color:#777">'+ 'Our price: $'+'<span style="text-decoration: line-through">'+new Intl.NumberFormat().format(cart_product_price)+'</span>'+'</span>'+'<br><br>' + 'Savings: -$'+cart_product_sale_price
                                            +'<br><br>'+'Sale Price: $'+new Intl.NumberFormat().format((cart_product_price - cart_product_sale_price))
                                        );
                                    }else{
                                            $('#price').html('Our price: $'+ new Intl.NumberFormat().format(cart_product_price));
                                    }
                                    $('#price_modal').html(cart_product_price);
                                    $('#discount_modal').html(cart_product_sale_price);
                                    $('#regular_modal').html(cart_product_price - cart_product_sale_price);
                                    $('#name_modal').html(cart_product_name);
                                    $('#img_modal').attr("src", '{{URL::to('/public/uploads/product/')}}'+'/'+cart_product_image);
                                    $('#data-id').attr("data-id_product",id) ;
                                    $('#a-id').attr("href",'{{URL::to('/cart')}}') ;



                                    $('#imageGallery1').lightSlider({
                                        gallery:true,
                                        item:1,
                                        loop:true,
                                        thumbItem:3,
                                        slideMargin:0,
                                        enableDrag: false,
                                        currentPagerPosition:'left',
                                        onSliderLoad: function(el) {
                                            el.lightGallery({
                                                selector: '#imageGallery1 .lslide'
                                            });
                                        }
                                    });

                                    $("#myModal").modal();

                                });


                                $('.add-to-cart').click(function(){
                                    var id = $(this).data('id_product');
                                    var cart_product_id = $('.cart_product_id_' + id).val();
                                    var cart_product_name = $('.cart_product_name_' + id).val();
                                    var cart_product_image = $('.cart_product_image_' + id).val();
                                    var cart_product_quantity = $('.cart_product_quantity_' + id).val();
                                    var cart_product_price = $('.cart_product_price_' + id).val();
                                    var cart_product_sale_price = $('.cart_product_sale_price_' + id).val();
                                    var cart_product_qty = $('.cart_product_qty_' + id).val();
                                    var _token = $('input[name="_token"]').val();
                                    if(parseInt(cart_product_qty)>parseInt(cart_product_quantity)){
                                    alert('Smaller please ' + cart_product_quantity);
                                    }else{
                                    $.ajax({
                                        url: '{{url('/add-cart-ajax')}}',
                                        method: 'POST',
                                        data:{cart_product_id:cart_product_id,cart_product_name:cart_product_name,cart_product_image:cart_product_image,cart_product_price:cart_product_price,cart_product_sale_price:cart_product_sale_price,cart_product_qty:cart_product_qty,_token:_token,cart_product_quantity:cart_product_quantity},
                                        success:function(data){
                                            $('.cart-products-count').html(data);
                                            swal({
                                                    title: "Product added to cart",
                                                    text: "",
                                                    showCancelButton: true,
                                                    cancelButtonText: "See more",
                                                    confirmButtonClass: "btn-success",
                                                    confirmButtonText: "Go to cart",
                                                    closeOnConfirm: false

                                                },
                                                function() {
                                                    window.location.href = "{{url('/cart')}}";
                                                });

                                    }
                                });
                          }
                      });
                            }
                        });
                    }
                    $(document).on('click','#load_more_button-{!!$category_id!!}',function(){
                        load_data(_token,'','','{!!$category_id!!}');
                    });
                    });
    </script>

</div>
@endsection
