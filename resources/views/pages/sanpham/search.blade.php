@extends('layout')
@section('content')
<section class="special-products">
    <div class="container">
        <div class="row">
            <h2 class="h1 products-section-title">

                <span>Search Results</span>
            </h2>
            <div class="products">
                <div class="product_bestseller">
                    <!-- Define Number of product for SLIDER -->
                    <ul id="newproduct-grid-special1" class="bestseller_grid product_list grid row gridcount">
                        @foreach($search_product as $key => $product)
                        <li class="product_item col-xs-12 col-sm-6 col-md-4 col-lg-2">
                            <div class="product-miniature js-product-miniature">
                                <div class="thumbnail-container">

                                    <a href="{{URL::to('/product/'.$product->product_slug)}}">
                                        <img src="{{URL::to('public/uploads/product/'.$product->product_image)}}"
                                            alt="" />
                                    </a>
                                    <ul class="product-flags">
                                        <li class="new">New</li>
                                    </ul>
                                    <div class="product-hover">
                                        <a href="#" class=" exampleModal"
                                            data-id_product="{{$product->product_id}}" data-toggle="modal"
                                            data-target="#exampleModal">

                                            {{-- <i class="material-icons search"></i> Quick view --}}
                                        </a>
                                    </div>
                                </div>
                                <div class="product-description">
                                    <h1 class="h3 product-title" itemprop="name">
                                        <a
                                            href="{{URL::to('/product/'.$product->product_slug)}}">{{$product->product_name}}</a>
                                    </h1>
                                    <div class="product-price-and-shipping">
                                        {{-- <span itemprop="price" class="price">{{'$'.(number_format($product->product_price))}}</span>
                                        --}}

                                        @if($product->product_sale_price !=0)

                                        <div class="product-price-and-shipping">
                                            <span itemprop="price"
                                                class="price">{{'$'.(number_format($product->product_price))}}</span>
                                            <span
                                                class="discount-percentage">{{'-$'.(number_format($product->product_sale_price))}}</span>
                                            <span
                                                class="regular-price">{{'$'.(number_format($product->product_price - $product->product_sale_price))}}</span>
                                        </div>

                                        @else

                                        <div class="product-price-and-shipping">
                                            <span itemprop="price"
                                                class="price">{{'$'.(number_format($product->product_price))}}</span>
                                        </div>
                                        @endif
                                    </div>

                                    <div class="product-actions">

                                        <form>
                                            @csrf
                                            <input type="hidden" value="{{$product->product_id}}"
                                                class="cart_product_id_{{$product->product_id}}">
                                            <input type="hidden" value="{{$product->product_name}}"
                                                class="cart_product_name_{{$product->product_id}}">
                                            <input type="hidden" value="{{$product->product_quantity}}"
                                                class="cart_product_quantity_{{$product->product_id}}">
                                            <input type="hidden" value="{{$product->product_image}}"
                                                class="cart_product_image_{{$product->product_id}}">
                                            <input type="hidden" value="{{$product->product_price}}"
                                                class="cart_product_price_{{$product->product_id}}">
                                            <input type="hidden" value="{{$product->product_sale_price}}"
                                                class="cart_product_sale_price_{{$product->product_id}}">
                                            <input type="hidden" value="1"
                                                class="cart_product_qty_{{$product->product_id}}">


                                            <input type="button" value="&nbsp;+ Add to cart"
                                                class="btn btn-primary add-to-cart"
                                                data-id_product="{{$product->product_id}}" name="add-to-cart">
                                        </form>
                                    </div>
                                </div>


                            </div>
                        </li>
                        @endforeach
                        <!-- Button trigger modal -->
                    </ul>




                </div>



                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title product_quickview_title" id="exampleModalLabel">
                                    <span id="product_quickview_title"></span>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </h5>

                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">

                                        <span id="product_quickview_image"></span>

                                    </div>
                                    <form>
                                        @csrf
                                        <div id="product_quickview_value"></div>
                                        <div class="col-md-6" style="text-align: left">
                                            <h2><span id="product_quickview_title"></span></h2>


                                            <div class="product-price h5 has-discount" itemprop="offers"
                                                itemscope="">
                                                <link itemprop="availability">

                                                <div class="product-prices" style="text-align: left">
                                                    <div class="product-price h5 " itemprop="offers"
                                                        itemscope="" itemtype="">
                                                        <link itemprop="availability">
                                                        <meta itemprop="priceCurrency" content="USD">

                                                        <div id="price_quickview"></div>
                                                        {{-- <div id="product_quickview_desc"></div> --}}

                                                    </div>
                                                    <div class="tax-shipping-delivery-label">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="product-add-to-cart">
                                                <span class="control-label">Quantity</span>
                                                <div class="product-quantity">
                                                    <div class="qty">
                                                        <div class="input-group bootstrap-touchspin">
                                                            <span
                                                                class="input-group-addon bootstrap-touchspin-prefix"
                                                                style="display: none;"></span>
                                                            <input style="width: 60px" type="number" name="qty"
                                                                min="1" class="cart_product_qty_" value="1">
                                                            <span
                                                                class="input-group-addon bootstrap-touchspin-postfix"
                                                                style="display: none;"></span><span
                                                                class="input-group-btn-vertical"></span>
                                                        </div>
                                                    </div>
                                                    <div class="add">




                                                        <span id="product-availability">
                                                            <i class="material-icons product-available"></i>
                                                            In stock
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <p class="product-minimal-quantity">
                                                </p>
                                            </div>

                                            <div id="product_quickview_button"></div>
                                            <div id="beforesend_quickview"></div>
                                            <style>
                                                h5.modal-title.product_quickview_title {
                                                    text-align: center;
                                                    font-size: 25px;

                                                }

                                                p.quickview {
                                                    font-size: 14px;
                                                }

                                                span#product_quickview_content img {
                                                    width: 100%;
                                                }

                                                @media screen and (min-width: 768px) {
                                                    .modal-dialog {
                                                        width: 700px;
                                                    }

                                                    .modal-sm {
                                                        width: 350px;
                                                    }
                                                }

                                                @media screen and (min-width: 768px) {
                                                    .modal-lg {
                                                        width: 1200px;
                                                    }

                                                }
                                            </style>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary redirect-cart"
                                    data-dismiss="modal">Go to
                                    cart</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                </button>

                            </div>
                        </div>
                    </div>
                </div>

            @endsection
