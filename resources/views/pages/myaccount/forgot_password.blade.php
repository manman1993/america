

@extends('layout')
@section('content')

<section class="special-products">
    <div class="container">
        <section id="wrapper">
            <div class="container">
                <div class="row">
                    <div id="columns_inner">
                        <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                            <div class="block-categories block">
                                <h4 class="block_title hidden-md-down">
                                    <a href="{{'/'}}">Home</a>
                                </h4>
                                <h4 class="block_title hidden-lg-up" data-target="#block_categories_toggle"
                                    data-toggle="collapse">
                                    <a href="{{'/'}}">Home</a>
                                    <span class="pull-xs-right">
                                        <span class="navbar-toggler collapse-icons">
                                            <i class="material-icons add"></i>
                                            <i class="material-icons remove"></i>
                                        </span>
                                    </span>
                                </h4>
                                <div id="block_categories_toggle" class="block_content collapse">
                                    <ul class="category-top-menu">
                                        @foreach($category as $key => $cate)
                                        <li class="category" id="category-3">
                                            <a class="dropdown-item"
                                                href="{{URL::to('/category/'.$cate->slug_category_product)}}"
                                                data-depth="0">
                                                {{$cate->category_name}}
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                            <div class="sidebar-specials block">
                                <h4 class="block_title hidden-md-down">Sale Products</h4>
                                <h4 class="block_title hidden-lg-up" data-target="#block_specials_toggle"
                                    data-toggle="collapse">Special Products
                                    <span class="pull-xs-right">
                                        <span class="navbar-toggler collapse-icons">
                                            <i class="material-icons add"></i>
                                            <i class="material-icons remove"></i>
                                        </span>
                                    </span>
                                </h4>

                                <div class="block_content collapse" id="block_specials_toggle">
                                    <div class="products clearfix">
                                        @foreach($all_sale as $key => $cart)
                                        <div class="product-item">
                                            <div class="left-part">

                                                <a href="{{URL::to('/product/'.$cart->product_slug)}}">
                                                    <span class="product-image media-middle">
                                                        <img src="{{URL::to('public/uploads/product/'.$cart->product_image)}}"
                                                            width="90" alt="" />
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="right-part">
                                                <div class="product-description">

                                                    <h1 class="h3 product-title" itemprop="name"><a href=""></a></h1>
                                                    <div class="product-price-and-shipping">
                                                        <p>{{$cart->product_name}}</p>
                                                        <span style="text-decoration-line: line-through"
                                                            itemprop="price "
                                                            class="regular-price">{{'$'.(number_format($cart->product_price))}}</span>
                                                        <span
                                                            class="discount-percentage">{{'-$'.(number_format($cart->product_sale_price))}}</span>
                                                        <span
                                                            class="price">{{'$'.(number_format($cart->product_price - $cart->product_sale_price))}}</span>
                                                    </div>
                                                </div>
                                                <div class="product-actions">
                                                    <form>
                                                        @csrf
                                                        <input type="hidden" value="{{$cart->product_id}}"
                                                            class="cart_product_id_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_name}}"
                                                            class="cart_product_name_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_quantity}}"
                                                            class="cart_product_quantity_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_image}}"
                                                            class="cart_product_image_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_price}}"
                                                            class="cart_product_price_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_sale_price}}"
                                                            class="cart_product_sale_price_{{$cart->product_id}}">
                                                        <input type="hidden" value="1"
                                                            class="cart_product_qty_{{$cart->product_id}}">

                                                        <input type="button" value="Add to cart"
                                                            class="btn btn-default add-to-cart"
                                                            data-id_product="{{$cart->product_id}}" name="add-to-cart">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="clearfix">

                                        <a href="{{URL::to('/product-sale/sale')}}" class="allproducts">All sale
                                            products</a>


                                    </div>
                                </div>
                            </div>

                        </div>

                        <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">

                            <header class="page-header" style="border-bottom:none">
                                <h1>
                                    FORGOT YOUR PASSWORD?
                                </h1>
                            </header>
                            <section id="content" class="page-content card card-block">
                                <section class="login-form">


                                    <form method="post" action="{{url('/forgot_password')}}">

                                        @csrf
                                        @if(session('error'))
                                        <div style="color: red">{{session('error')}}</div>
                                        @endif

                                        @if(!session('step'))
                                            Please enter the e-mail you use to sign in to your account, then click Continue.<br>
                                                We'll email you a link to a page where you can easily create a new password.<br><br>
                                        @elseif(session('step')==2)

                                        @endif
                                        <input class="form-control" type="hidden" name="step" id="step"
                                            @if(session('step'))
                                                value="{{session('step')}}"
                                            @else value="1"
                                            @endif
                                        >
                                        <div class="form-group row ">
                                            <label class="col-md-4 form-control-label required">
                                                @if(!session('step'))

                                                Email:

                                                @elseif(session('step')==2)
                                                Code:
                                                @endif

                                            </label>
                                            <div class="col-md-4">
                                                <input  class="form-control" type="hidden" name="step" id="step"
                                                @if(session('step'))
                                                    value="{{session('step')}}"
                                                @else value="1"
                                                @endif
                                            >
                                            <input class="form-control"
                                            @if(!session('step') || session('step')==1) type="email"
                                            @else
                                                type="hidden"
                                            @endif name="email" id="email"
                                            @if(session('email'))
                                                value="{{session('email')}}"
                                            @endif
                                        >

                                        <input required class="form-control"
                                            @if(session('step')==2) type="text"
                                            @else
                                                type="hidden"
                                            @endif
                                            name="code" id="code"
                                            @if(session('code')) value="{{session('code')}}"
                                            @endif
                                        >
                                        @if(session('step')==3)

                                            New Password
                                            <input data-validation="length" data-validation-length="min6" data-validation-error-msg="Password has at least 6 characters" class="form-control"
                                                @if(session('step')==3) type="password"
                                                @else type="hidden"
                                                @endif
                                                name="pass" id="pass"><br>

                                            Confirm New Password
                                            <input data-validation="length" data-validation-length="min6" data-validation-error-msg="Password has at least 6 characters" class="form-control"
                                                @if(session('step')==3) type="password"
                                                @else type="hidden"
                                                @endif
                                                name="repass" id="repass">

                                        @endif
                                        <br>
                                        @if(session('success'))
                                        <div class="text-success">{{session('success')}}</div>
                                        @endif
                                            </div>
                                            <div class="col-md-3 form-control-comment">
                                            </div>
                                        </div>








                                        <button style="background: #004167;padding:10px;color:white" type="submit">Reset Password</button>
                                    </form>
                                </section>
                                <hr>
                                <div class="no-account">

                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>


@endsection
