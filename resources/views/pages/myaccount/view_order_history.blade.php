@extends('layout')
@section('content')
<section class="special-products">
    <div class="container">
        <section id="wrapper">
            <div class="container">
                <div class="row">
                    <div id="columns_inner">
                        <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                            <div class="block-categories block">
                                <h4 class="block_title hidden-md-down">
                                    <a href="{{'/'}}">Home</a>
                                </h4>
                                <h4 class="block_title hidden-lg-up" data-target="#block_categories_toggle"
                                    data-toggle="collapse">
                                    <a href="{{'/'}}">Home</a>
                                    <span class="pull-xs-right">
                                        <span class="navbar-toggler collapse-icons">
                                            <i class="material-icons add"></i>
                                            <i class="material-icons remove"></i>
                                        </span>
                                    </span>
                                </h4>
                                <div id="block_categories_toggle" class="block_content collapse">
                                    <ul class="category-top-menu">
                                        @foreach($category as $key => $cate)
                                        <li class="category" id="category-3">
                                            <a class="dropdown-item"
                                                href="{{URL::to('/category/'.$cate->slug_category_product)}}"
                                                data-depth="0">
                                                {{$cate->category_name}}
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="sidebar-specials block">
                                <h4 class="block_title hidden-md-down">Sale Products</h4>
                                <h4 class="block_title hidden-lg-up" data-target="#block_specials_toggle"
                                    data-toggle="collapse">Special Products
                                    <span class="pull-xs-right">
                                        <span class="navbar-toggler collapse-icons">
                                            <i class="material-icons add"></i>
                                            <i class="material-icons remove"></i>
                                        </span>
                                    </span>
                                </h4>
                                <div class="block_content collapse" id="block_specials_toggle">
                                    <div class="products clearfix">
                                        @foreach($all_sale as $key => $cart)
                                        <div class="product-item">
                                            <div class="left-part">
                                                <a href="{{URL::to('/product/'.$cart->product_slug)}}">
                                                    <span class="product-image media-middle">
                                                        <img src="{{URL::to('public/uploads/product/'.$cart->product_image)}}"
                                                            width="90" alt="" />
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="right-part">
                                                <div class="product-description">
                                                    <h1 class="h3 product-title" itemprop="name"><a href=""></a></h1>
                                                    <div class="product-price-and-shipping">
                                                        <p>{{$cart->product_name}}</p>
                                                        <span style="text-decoration-line: line-through"
                                                            itemprop="price "
                                                            class="regular-price">{{'$'.(number_format($cart->product_price))}}</span>
                                                        <span
                                                            class="discount-percentage">{{'-$'.(number_format($cart->product_sale_price))}}</span>
                                                        <span
                                                            class="price">{{'$'.(number_format($cart->product_price - $cart->product_sale_price))}}</span>
                                                    </div>
                                                </div>
                                                <div class="product-actions">
                                                    <form>
                                                        @csrf
                                                        <input type="hidden" value="{{$cart->product_id}}"
                                                            class="cart_product_id_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_name}}"
                                                            class="cart_product_name_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_quantity}}"
                                                            class="cart_product_quantity_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_image}}"
                                                            class="cart_product_image_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_price}}"
                                                            class="cart_product_price_{{$cart->product_id}}">
                                                        <input type="hidden" value="{{$cart->product_sale_price}}"
                                                            class="cart_product_sale_price_{{$cart->product_id}}">
                                                        <input type="hidden" value="1"
                                                            class="cart_product_qty_{{$cart->product_id}}">
                                                        <input type="button" value="Add to cart"
                                                            class="btn btn-default add-to-cart"
                                                            data-id_product="{{$cart->product_id}}" name="add-to-cart">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="clearfix">
                                        <a href="{{URL::to('/product-sale/sale')}}" class="allproducts">All sale
                                            products</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="content-wrapper" class="left-column col-xs-12 col-sm-8 col-md-9">
                            <header class="page-header" style="border-bottom:none">
                                <h1>
                                    YOUR ORDER HISTORY
                                </h1>
                            </header>
                            <section id="content" class="page-content" style="padding: 30px;
								border: 1px solid #ececec;">
                                <aside id="notifications">
                                    <div class="container">
                                    </div>
                                </aside>

                                <div class="table-responsive">
                                        <?php
										$message = Session::get('message');
										if($message){
										    echo '<h3 class="text-alert text-success ">'.$message.'</h3>';
										    Session::put('message',null);
										}
                                        ?>

                                    <div class="table-agile-info">

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                ORDER DETAILS
                                            </div>

                                            <div class="table-responsive">
                                                <?php
                                            $message = Session::get('message');
                                            if($message){
                                                echo '<span class="text-alert">'.$message.'</span>';
                                                Session::put('message',null);
                                            }
                                            ?>

                                                <table class="table table-striped table-bordered table-labeled hidden-sm-down">
                                                    <thead class="thead-default">
                                                        <tr>
                                                            <th style="width:20px;">
                                                                No
                                                            </th>
                                                            <th style="text-align: center">Product's name</th>
                                                            <th style="text-align: center">Product price ($)</th>
                                                            <th style="text-align: center">Amount</th>
                                                            <th style="text-align: center">Total Product Price ($)</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php

                                                        $i = 0;
                                                        $total = 0;
                                                        @endphp
                                                        @foreach($order_details as $key => $details)
                                                        <?php $total_after_fee = $details->product_price - $details->product_sale_price ?>
                                                        @php
                                                        $i++;
                                                        $subtotal = $total_after_fee*$details->product_sales_quantity;
                                                        $total+=$subtotal;

                                                        $total_price= $total_after_fee*$details->product_sales_quantity;
                                                        $vat = $total_price*0.0825;
                                                        $total_coupon = $total + $total*0.0825 + $details->product_feeship ;
                                                        @endphp
                                                        <tr class="color_qty_{{$details->product_id}}">

                                                            <td><i>{{$i}}</i></td>
                                                            <td>{{$details->product_name}}</td>
                                                            <td>{{number_format( $total_after_fee)}}</td>
                                                            <td>
                                                                {{$details->product_sales_quantity}}
                                                            </td>
                                                            <td>{{number_format( $total_price)}}
                                                            </td>






                                                        </tr>

                                                        @endforeach


                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>
                                        <p>shipping : {{'$'.number_format($details->product_feeship )}}</p>
                                    <p>Sales Tax : {{'$'.number_format($total*0.0825)}}</p>
                                        <p>Total Payment :{{'$'.number_format($total + $total*0.0825 + $details->product_feeship)}}</p>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
@endsection
