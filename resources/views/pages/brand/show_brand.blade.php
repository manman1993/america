@extends('layout')
@section('content')
<nav data-depth="2" class="breadcrumb hidden-sm-down">
    <div class="container">
   <ol itemscope="">

       <li itemprop="itemListElement" itemscope="" itemtype="">
       <a itemprop="item" href="{{'/'}}">
           <span itemprop="name">Home</span>
         </a>
         <meta itemprop="position" content="1">
       </li>

       @foreach($brand_name as $key => $name)
       <li itemprop="itemListElement" itemscope="" itemtype="">
         <a itemprop="item" href="{{URL::to('/product-sale/'.$name->brand_slug)}}">

           <span itemprop="name">{{$name->brand_name}}</span>

         </a>
         <meta itemprop="position" content="2">
       </li>
       @endforeach
       </ol>
   </div>
 </nav>

 <div class="container">
    <div id="">

        <div id="js-product-list-top" class="row products-selection">
            <div class="col-sm-12 hidden-lg-up filter-button">
                <button id="search_filter_toggler" class="btn btn-secondary">
                    Filter
                </button>
            </div>
            <div class="col-md-6 hidden-md-down total-products">
                <ul class="display hidden-xs grid_list">
                    {{-- <li id="grid"><a href="#" title="Grid">Grid</a></li>
                    <li id="list" class="selected"><a href="#" title="List">List</a></li> --}}
                </ul>
                {{-- <p>There are <span>20</span> products.</p> --}}
            </div>
            <div class="col-md-6">
                <div class="row sort-by-row">


                    <span style="font-size: 20px;color:black" class="col-sm-3 col-md-3 hidden-sm-down sort-by">Sort by:</span>
                    <div class="col-sm-9 col-xs-8 col-md-9 products-sort-order dropdown">

                        <select name="sort" id="sort" class="form-control select-title">
                            <option value="none-none">Featured</option>
                            <option value="product_price-asc">Price - Low to High</option>
                            <option value="product_price-desc">Price - High to Low</option>
                            <option value="product_sale_price-asc"> Discount - Low to High</option>
                            <option value="product_sale_price-desc">Discount - High to Low</option>
                        </select>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <section class="ct-hometabcontent">
            {{-- @foreach($brand as $key => $name)

            <h1 class="h1 products-section-title"><span>{{$name->brand}}</span></h1>
            @endforeach --}}

            <div class="tabs">


                <div class="tab-content hb-animate-element bottom-to-top hb-in-viewport">

                    <div id="newProduct" class="ct_productinner tab-pane active" aria-expanded="true">
                        <section class="newproducts clearfix">
                            <h2 class="h1 products-section-title text-uppercase">
                                Sale products
                            </h2>

                            <div class="products">
                                <div class="product_new">
                                    <!-- Define Number of product for SLIDER -->
                                    <ul id="newproduct-grid-{!!$brand_slug!!}"
                                    class="bestseller_grid product_list grid row gridcount">
                                    @csrf
                                </ul>


                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog modal-lg">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close"
                                                    data-dismiss="modal">&times;</button>
                                                <h1 id="name_modal" class="modal-title">Modal Header</h1>
                                            </div>

                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 hidden-xs-down" id="gallery_home">
                                                        <div class="images-container">
                                                            <div class="product-cover">
                                                                <img id="img_modal" class="js-qv-product-cover"
                                                                    src="" alt="" title="" style="width:100%;"
                                                                    itemprop="image">

                                                                <div class="layer hidden-sm-down"
                                                                    data-toggle="modal"
                                                                    data-target="#product-modal1">
                                                                    <i class="material-icons zoom-in"></i>
                                                                </div>
                                                            </div>
                                                            <!-- Define Number of product for SLIDER -->

                                                        </div>
                                                        <div class="arrows js-arrows" style="display: none;">
                                                            <i class="material-icons arrow-up js-arrow-up"></i>
                                                            <i class="material-icons arrow-down js-arrow-down"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">

                                                        <div class="product-prices" style="text-align: left">
                                                            <div class="product-price h5 " itemprop="offers"
                                                                itemscope="" itemtype="">
                                                                <link itemprop="availability">
                                                                <meta itemprop="priceCurrency" content="USD">
                                                                <div class="current-price" style="font-size: 18px">
                                                                   <div id="price"></div>
                                                                </div>

                                                            </div>

                                                            <div class="tax-shipping-delivery-label">
                                                            </div>
                                                        </div>

                                                        <div class="product-actions">
                                                            <form>
                                                                <input type="hidden" name="token"
                                                                    value="37a35808bd3e31a6c88db859b1dc2e5a">
                                                                <input type="hidden" name="id_product" value="1"
                                                                    id="product_page_product_id">
                                                                <input type="hidden" name="id_customization"
                                                                    value="0" id="product_customization_id">

                                                                <div class="product-add-to-cart">
                                                                    <span class="control-label">Quantity</span>
                                                                    <div class="product-quantity">
                                                                        <div class="qty">
                                                                            <div
                                                                                class="input-group bootstrap-touchspin">
                                                                                <span
                                                                                    class="input-group-addon bootstrap-touchspin-prefix"
                                                                                    style="display: none;"></span><input
                                                                                    type="text" name="qty"
                                                                                    id="quantity_wanted" value="1"
                                                                                    class="input-group form-control"
                                                                                    min="1"
                                                                                    style="display: block;"><span
                                                                                    class="input-group-addon bootstrap-touchspin-postfix"
                                                                                    style="display: none;"></span><span
                                                                                    class="input-group-btn-vertical"><button
                                                                                        class="btn btn-touchspin js-touchspin bootstrap-touchspin-up"
                                                                                        type="button"><i
                                                                                            class="material-icons touchspin-up"></i></button><button
                                                                                        class="btn btn-touchspin js-touchspin bootstrap-touchspin-down"
                                                                                        type="button"><i
                                                                                            class="material-icons touchspin-down"></i></button></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="add">

                                                                            {{-- <a id="a-id" href=""> --}}
                                                                                <input id="data-id" type="button"
                                                                                value="Buy now"
                                                                                class="btn btn-primary add-to-cart-quickview"
                                                                                data-id_product
                                                                                name="add-to-cart-quickview">
                                                                            {{-- </a> --}}
                                                                            <span id="product-availability">
                                                                                <i
                                                                                    class="material-icons product-available"></i>
                                                                                In stock
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <p class="product-minimal-quantity">
                                                                    </p>
                                                                </div>
                                                                <input class="product-refresh"
                                                                    data-url-update="false" name="refresh"
                                                                    type="submit" value="Refresh" hidden="">
                                                            </form>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary redirect-cart" data-dismiss="modal">Go to
                                                    cart</button>
                                                <button type="button" class="btn btn-default"
                                                    data-dismiss="modal">Close</button>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <input type="hidden" id="numpage-{!!$brand_slug!!}"
                                    name="numpage-{!!$brand_slug!!}" value="0">
                                <button id="load_more_button-{!!$brand_slug!!}" type="button"
                                    class="all-product-link pull-xs-left pull-md-right h4">Load more</button>
                                </div>
                            </div>
                        </section>
                    </div>

                </div>
            </div>

        </section>
    </div>
    {{-- <script>
        $(document).ready(function(){
            var _token = $('input[name="_token"]').val();
            load_data(_token,'{!!$brand_slug!!}','',0);
            function load_data(_token,branch,category,notloadmore){
            // var _num = parseInt($('input[id="numpage-'+branch+'"]').val()) + 4;
            if(notloadmore > 0){
                var _num = parseInt($('input[id="numpage-'+branch+'"]').val());
            }else{
                var _num = parseInt($('input[id="numpage-'+branch+'"]').val()) + 4;
            }
            var url = $('#sort').val();
            var temp = url.split('-');
            // var _token = $('input[name="_token"]').val();
            // load_data(_token,'','{!!$brand_slug!!}',0);
            // function load_data(_token,branch,category,notloadmore){
            // if(notloadmore > 0){
            //     var _num = parseInt($('input[id="numpage-'+category+'"]').val());
            // }else{
            //     var _num = parseInt($('input[id="numpage-'+category+'"]').val()) + 4;
            // }
            // var url = $('#sort').val();
            // var temp = url.split('-');
            $.ajax({
                url : '{{url('/loadmore')}}',
                method: 'POST',
                data:{ _token:_token,"numpage":_num,"category": category,"branch": branch,"field_sort" : temp[0], "type_sort": temp[1]},
                success:function(data){
                    if(category != ''){
                        $('#newproduct-grid-'+category).html(data.out);
                        $('#numpage-'+category).val(data.num);
                    }else{
                        $('#newproduct-grid-'+branch).html(data.out);
                        $('#numpage-'+branch).val(data.num);
                    }
                    // alert(data.count);
                    if(data.num >= data.count){
                        document.getElementById("load_more_button-"+branch).style.display = "none";
                    }

                    $('.quick-view').click(function(){
                        var id = $(this).data('id_product');
                        var cart_product_id = $('.cart_product_id_' + id).val();
                        var cart_product_name = $('.cart_product_name_' + id).val();
                        var cart_product_image = $('.cart_product_image_' + id).val();
                        var cart_product_quantity = $('.cart_product_quantity_' + id).val();
                        var cart_product_price = $('.cart_product_price_' + id).val();
                        var cart_product_sale_price = $('.cart_product_sale_price_' + id).val();
                        var cart_product_qty = $('.cart_product_qty_' + id).val();
                        var _token = $('input[name="_token"]').val();
                        if(cart_product_price!=(cart_product_price - cart_product_sale_price)){
                            $('#price').html(
                                '<span style="color:#777">'+ 'Our price: $'+'<span style="text-decoration: line-through">'+new Intl.NumberFormat().format(cart_product_price)+'</span>'+'</span>'+'<br><br>' + 'Savings: -$'+cart_product_sale_price
                                +'<br><br>'+'Sale Price: $'+new Intl.NumberFormat().format((cart_product_price - cart_product_sale_price))
                            );
                        }else{
                                $('#price').html('Our price: $'+ new Intl.NumberFormat().format(cart_product_price));
                        }
                        $('#price_modal').html(cart_product_price);
                        $('#discount_modal').html(cart_product_sale_price);
                        $('#regular_modal').html(cart_product_price - cart_product_sale_price);
                        $('#name_modal').html(cart_product_name);
                        $('#img_modal').attr("src", '{{URL::to('/public/uploads/product/')}}'+'/'+cart_product_image);
                        $('#data-id').attr("data-id_product",id) ;
                        $('#a-id').attr("href",'{{URL::to('/cart')}}') ;



                        $('#imageGallery1').lightSlider({
                            gallery:true,
                            item:1,
                            loop:true,
                            thumbItem:3,
                            slideMargin:0,
                            enableDrag: false,
                            currentPagerPosition:'left',
                            onSliderLoad: function(el) {
                                el.lightGallery({
                                    selector: '#imageGallery1 .lslide'
                                });
                            }
                        });

                        $("#myModal").modal();

                    });


                    $('.add-to-cart').click(function(){
                        var id = $(this).data('id_product');
                        var cart_product_id = $('.cart_product_id_' + id).val();
                        var cart_product_name = $('.cart_product_name_' + id).val();
                        var cart_product_image = $('.cart_product_image_' + id).val();
                        var cart_product_quantity = $('.cart_product_quantity_' + id).val();
                        var cart_product_price = $('.cart_product_price_' + id).val();
                        var cart_product_sale_price = $('.cart_product_sale_price_' + id).val();
                        var cart_product_qty = $('.cart_product_qty_' + id).val();
                        var _token = $('input[name="_token"]').val();
                        if(parseInt(cart_product_qty)>parseInt(cart_product_quantity)){
                        alert('Smaller please ' + cart_product_quantity);
                        }else{
                        $.ajax({
                            url: '{{url('/add-cart-ajax')}}',
                            method: 'POST',
                            data:{cart_product_id:cart_product_id,cart_product_name:cart_product_name,cart_product_image:cart_product_image,cart_product_price:cart_product_price,cart_product_sale_price:cart_product_sale_price,cart_product_qty:cart_product_qty,_token:_token,cart_product_quantity:cart_product_quantity},
                            success:function(data){
                                $('.cart-products-count').html(data);
                                swal({
                                        title: "Product added to cart",
                                        text: "",
                                        showCancelButton: true,
                                        cancelButtonText: "See more",
                                        confirmButtonClass: "btn-success",
                                        confirmButtonText: "Go to cart",
                                        closeOnConfirm: false

                                    },
                                    function() {
                                        window.location.href = "{{url('/cart')}}";
                                    });

                        }
                    });
              }
          });
                }
            });
        }
        $(document).on('click','#load_more_button-{!!$brand_slug!!}',function(){
            load_data(_token,'{!!$brand_slug!!}','',0);
        });
        $(document).on('change','#sort',function(){
            load_data(_token,'{!!$brand_slug!!}','',1);
        });
        });
    </script> --}}
    <script>
        $(document).ready(function(){

            var _token = $('input[name="_token"]').val();
            load_data(_token,'{!!$brand_slug!!}','',0);
            function load_data(_token,branch,category,notloadmore){
            if(notloadmore > 0){
                var _num = parseInt($('input[id="numpage-'+branch+'"]').val());
            }else{
                var _num = parseInt($('input[id="numpage-'+branch+'"]').val()) + 4;
            }
            var url = $('#sort').val();
            var temp = url.split('-');
            $.ajax({
                url : '{{url('/loadmore')}}',
                method: 'POST',
                data:{ _token:_token,"numpage":_num,"category": category,"branch": branch,"field_sort" : temp[0], "type_sort": temp[1]},
                success:function(data){
                    if(category != ''){
                        $('#newproduct-grid-'+category).html(data.out);
                        $('#numpage-'+category).val(data.num);
                    }else{
                        $('#newproduct-grid-'+branch).html(data.out);
                        $('#numpage-'+branch).val(data.num);
                    }
                    // alert(data.count);
                    if(data.num >= data.count){
                        document.getElementById("load_more_button-"+branch).style.display = "none";
                    }

                    $('.quick-view').click(function(){
                        var id = $(this).data('id_product');
                        var cart_product_id = $('.cart_product_id_' + id).val();
                        var cart_product_name = $('.cart_product_name_' + id).val();
                        var cart_product_image = $('.cart_product_image_' + id).val();
                        var cart_product_quantity = $('.cart_product_quantity_' + id).val();
                        var cart_product_price = $('.cart_product_price_' + id).val();
                        var cart_product_sale_price = $('.cart_product_sale_price_' + id).val();
                        var cart_product_qty = $('.cart_product_qty_' + id).val();
                        var _token = $('input[name="_token"]').val();
                        if(cart_product_price!=(cart_product_price - cart_product_sale_price)){
                            $('#price').html(
                                '<span style="color:#777">'+ 'Our price: $'+'<span style="text-decoration: line-through">'+new Intl.NumberFormat().format(cart_product_price)+'</span>'+'</span>'+'<br><br>' + 'Savings: -$'+cart_product_sale_price
                                +'<br><br>'+'Sale Price: $'+new Intl.NumberFormat().format((cart_product_price - cart_product_sale_price))
                            );
                        }else{
                                $('#price').html('Our price: $'+ new Intl.NumberFormat().format(cart_product_price));
                        }


                        $('#price_modal').html(cart_product_price);
                        $('#discount_modal').html(cart_product_sale_price);
                        $('#regular_modal').html(cart_product_price - cart_product_sale_price);
                        $('#name_modal').html(cart_product_name);
                        $('#img_modal').attr("src", '{{URL::to('/public/uploads/product/')}}'+'/'+cart_product_image);
                        $('#data-id').attr("data-id_product",id) ;
                        $('#a-id').attr("href",'{{URL::to('/cart')}}') ;

                        $('#imageGallery1').lightSlider({
                            gallery:true,
                            item:1,
                            loop:true,
                            thumbItem:3,
                            slideMargin:0,
                            enableDrag: false,
                            currentPagerPosition:'left',
                            onSliderLoad: function(el) {
                                el.lightGallery({
                                    selector: '#imageGallery1 .lslide'
                                });
                            }
                        });

                        $("#myModal").modal();

                    });


                    $('.add-to-cart').click(function(){
                        var id = $(this).data('id_product');
                        var cart_product_id = $('.cart_product_id_' + id).val();
                        var cart_product_name = $('.cart_product_name_' + id).val();
                        var cart_product_image = $('.cart_product_image_' + id).val();
                        var cart_product_quantity = $('.cart_product_quantity_' + id).val();
                        var cart_product_price = $('.cart_product_price_' + id).val();
                        var cart_product_sale_price = $('.cart_product_sale_price_' + id).val();
                        var cart_product_qty = $('.cart_product_qty_' + id).val();
                        var _token = $('input[name="_token"]').val();
                        if(parseInt(cart_product_qty)>parseInt(cart_product_quantity)){
                        alert('Smaller please ' + cart_product_quantity);
                        }else{
                        $.ajax({
                            url: '{{url('/add-cart-ajax')}}',
                            method: 'POST',
                            data:{cart_product_id:cart_product_id,cart_product_name:cart_product_name,cart_product_image:cart_product_image,cart_product_price:cart_product_price,cart_product_sale_price:cart_product_sale_price,cart_product_qty:cart_product_qty,_token:_token,cart_product_quantity:cart_product_quantity},
                            success:function(data){
                                $('.cart-products-count').html(data);
                                swal({
                                        title: "Product added to cart",
                                        text: "",
                                        showCancelButton: true,
                                        cancelButtonText: "See more",
                                        confirmButtonClass: "btn-success",
                                        confirmButtonText: "Go to cart",
                                        closeOnConfirm: false

                                    },
                                    function() {
                                        window.location.href = "{{url('/cart')}}";
                                    });

                        }
                    });
              }
          });
                }
            });
        }
        $(document).on('click','#load_more_button-{!!$brand_slug!!}',function(){
            load_data(_token,'{!!$brand_slug!!}','',0);
        });
        $(document).on('change','#sort',function(){
            load_data(_token,'{!!$brand_slug!!}','',1);
        });
        });
    </script>
</div>
@endsection
