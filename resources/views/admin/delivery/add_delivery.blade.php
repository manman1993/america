@extends('admin_layout')
@section('admin_content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Add shipping fee
            </header>
            <?php
                            $message = Session::get('message');
                            if($message){
                                echo '<span class="text-alert">'.$message.'</span>';
                                Session::put('message',null);
                            }
                            ?>
            <div class="panel-body">

                <div class="position-center">
                    <form>
                        @csrf

                        <div class="form-group">
                            <label style="font-size: 20px" for="exampleInputPassword1">Select state</label>
                            <select name="city" id="city" class="form-control input-sm m-bot15 choose city">

                                <option value="">-- Choose --</option>
                                @foreach($city as $key => $ci)
                                <option value="{{$ci->matp}}">{{$ci->name_city}}</option>

                                @endforeach

                            </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Shipping fee</label>
                            <input type="text" name="fee_ship" class="form-control fee_ship" id="exampleInputEmail1"
                                placeholder="Name list">
                        </div>

                        <button style="margin-bottom:20px" type="button" name="add_delivery"
                            class="btn btn-info add_delivery"> Add shipping fee</button>
                    </form>
                </div>

                <div id="load_delivery">
                </div>


            </div>
        </section>

    </div>
    @endsection
