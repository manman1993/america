@extends('admin_layout')
@section('admin_content')
<div class="row">
            <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                           Add evens
                        </header>
                         <?php
                            $message = Session::get('message');
                            if($message){
                                echo '<h3 class="text-alert text-center text-success">'.$message.'</h3>';
                                Session::put('message',null);
                            }
                            ?>
                        <div class="panel-body">

                            <div class="position-center">
                                <form role="form" action="{{URL::to('/save-brand-product')}}" method="post">
                                    {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Name</label>
                                    <input type="text" data-validation="length" data-validation-length="min3" data-validation-error-msg="Please enter at least 3 characters" name="brand_product_name" class="form-control" onkeyup="ChangeToSlug();" id="slug" placeholder="Name list">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Slug</label>
                                    <input type="text" name="brand_slug" class="form-control" id="convert_slug" placeholder="Slug">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Description</label>
                                    <textarea style="resize: none" rows="8" class="form-control" name="brand_product_desc" id="exampleInputPassword1" placeholder="Mô tả danh mục"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status</label>
                                      <select name="brand_product_status" class="form-control input-sm m-bot15">
                                            <option value="0">Show</option>
                                            <option value="1">Hidden</option>

                                    </select>
                                </div>

                                <button type="submit" name="add_category_product" class="btn btn-info">Add</button>
                                </form>
                            </div>

                        </div>
                    </section>

            </div>
@endsection
