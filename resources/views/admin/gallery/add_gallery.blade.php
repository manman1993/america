@extends('admin_layout')
@section('admin_content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Add photo gallery
            </header>
            <?php
                            $message = Session::get('message');
                            if($message){

                                echo '<h3  style="margin:10px;text-align:center;color:#5cb85c" class="text-alert">'.$message.'</h3>';
                                Session::put('message',null);
                            }
                            ?>
            <form action="{{url('/insert-gallery/'.$pro_id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-3" align="right">

                    </div>
                    <div class="col-md-6">
                        <input id="file" type="file" class="form-control" name="file[]" accept="image/*" multiple>
                        <h3 style="text-align: center;margin:10px;color:#5cb85c;" id="error_gallery"></h3>
                    </div>
                    <div class="col-md-3">
                        <input type="submit" name="upload" value="Upload" class="btn btn-success btn-xs">
                    </div>
                </div>
            </form>

            <div class="panel-body">
                <input type="hidden" value="{{$pro_id}}" class="pro_id">
                <form>
                    @csrf
                    <div id="gallery_load">

                    </div>
                </form>
            </div>
        </section>

    </div>
    @endsection
