@extends('admin_layout')
@section('admin_content')
<div class="row">
            <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                           Add product
                        </header>
                         <?php
                            $message = Session::get('message');
                            if($message){
                                echo '<h3 class="text-alert text-center text-success">'.$message.'</h3>';
                                Session::put('message',null);
                            }
                            ?>
                        <div class="panel-body">

                            <div class="position-center">
                                <form role="form" action="{{URL::to('/save-product')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Product's name</label>
                                    <input type="text" data-validation="length" data-validation-length="min5" data-validation-error-msg="Please enter at least 5 characters" name="product_name" class="form-control " id="slug" placeholder="Name list" onkeyup="ChangeToSlug();">
                                </div>
                                 <div class="form-group">
                                    <label for="exampleInputEmail1">Amount</label>
                                    <input type="text" data-validation="number" data-validation-error-msg="Please enter the amount" name="product_quantity" class="form-control" id="exampleInputEmail1" placeholder="Điền số lượng">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Slug</label>
                                    <input type="text" name="product_slug" class="form-control " id="convert_slug" placeholder="Name list">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Product price</label>
                                    <input type="text" data-validation="number" data-validation-error-msg="Please enter the amount" name="product_price" class="form-control" id="" placeholder="Name list">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">The product price wants to reduce</label>
                                    <input type="text" data-validation="number" data-validation-error-msg="Please enter the amount" name="product_sale_price" class="form-control" id="" placeholder="Name list">
                                </div>
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Product photos</label>
                                    <input type="file" name="product_image" class="form-control" id="exampleInputEmail1">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Description</label>
                                    <textarea style="resize: none"  rows="8" class="form-control" name="product_desc" id="ckeditor1" placeholder="Description"></textarea>
                                </div>
                                 <div class="form-group">
                                    <label for="exampleInputPassword1">Product Details</label>
                                    <textarea style="resize: none" rows="8" class="form-control" name="product_content"  id="id4" placeholder="Product Details"></textarea>
                                </div>
                                 <div class="form-group">
                                    <label for="exampleInputPassword1">Product category</label>
                                      <select name="product_cate" class="form-control input-sm m-bot15">
                                        @foreach($cate_product as $key => $cate)
                                            <option value="{{$cate->category_id}}">{{$cate->category_name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                 <div class="form-group">
                                    <label for="exampleInputPassword1">Evens</label>
                                      <select name="product_brand" class="form-control input-sm m-bot15">
                                        @foreach($brand_product as $key => $brand)
                                            <option value="{{$brand->brand_id}}">{{$brand->brand_name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status</label>
                                      <select name="product_status" class="form-control input-sm m-bot15">
                                         <option value="0">Shows</option>
                                            <option value="1">Hidden</option>

                                    </select>
                                </div>

                                <button type="submit" name="add_product" class="btn btn-info">Add product</button>
                                </form>
                            </div>

                        </div>
                    </section>

            </div>
@endsection
