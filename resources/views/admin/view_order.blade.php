@extends('admin_layout')
@section('admin_content')
<div class="table-agile-info">

  <div class="panel panel-default">
    <div class="panel-heading">
        Login information
    </div>

    <div class="table-responsive">
                      <?php
                            $message = Session::get('message');
                            if($message){
                                echo '<h3 class="text-alert text-center text-success">'.$message.'</h3>';
                                Session::put('message',null);
                            }
                            ?>
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>

            <th>First name</th>
            <th>Last name</th>
            <th>Phone</th>
            <th>Email</th>

            <th style="width:30px;"></th>
          </tr>
        </thead>
        <tbody>

          <tr>
            <td>{{$customer->customer_first_name}}</td>
            <td>{{$customer->customer_last_name}}</td>
            <td>{{$customer->customer_phone}}</td>
            <td>{{$customer->customer_email}}</td>
          </tr>

        </tbody>
      </table>

    </div>

  </div>
</div>
<br>
<div class="table-agile-info">
     <div class="panel panel-default">
    <div class="panel-heading">
        Paypal INFORMATION
    </div>


    <div class="table-responsive">
                      <?php
                            $message = Session::get('message');
                            if($message){
                                echo '<span class="text-alert">'.$message.'</span>';
                                Session::put('message',null);
                            }
                        ?>
    @if($order_by_id==true)
        <table class="table table-striped b-t b-light">
            <thead>
            <tr>
                <th>Name</th>
                <th>Transaction ID: </th>
                <th>Payment Total Amount:</th>
                <th>Payment Status:</th>
                <th style="width:30px;"></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{$order_by_id->full_name}}</td>
                <td>{{$order_by_id->id_paypal}}</td>
                <td>{{'$'.number_format($order_by_id->price_paypal)}}</td>
                <td>
                    {{$order_by_id->status }}
                </td>
            </tr>
            </tbody>
        </table>
     @else

     @endif
    </div>

  </div>
</div>
<br>



  <div class="panel panel-default">
    <div class="panel-heading">
        SHIPPING INFORMATION
    </div>


    <div class="table-responsive">
                      <?php
                            $message = Session::get('message');
                            if($message){
                                echo '<span class="text-alert">'.$message.'</span>';
                                Session::put('message',null);
                            }
                            ?>
      <table class="table table-striped b-t b-light">
        <thead>
          <tr>

            <th>First name</th>
            <th>Last name</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Apt, suite, etc</th>
            <th>City</th>
            <th>Created at</th>



            <th style="width:30px;"></th>
          </tr>
        </thead>
        <tbody>

          <tr>

            <td>{{$shipping->first_name}}</td>
            <td>{{$shipping->last_name}}</td>
            <td>{{$shipping->shipping_address}}</td>
             <td>{{$shipping->shipping_phone}}</td>
             <td>{{$shipping->shipping_notes}}</td>
             <td>{{$shipping->shipping_city}}</td>
             <td>{{$shipping->created_at}}</td>

          </tr>

        </tbody>
      </table>

    </div>

  </div>
</div>
<br>

<div class="table-agile-info">

  <div class="panel panel-default">
    <div class="panel-heading">
        ORDER DETAILS
    </div>

    <div class="table-responsive">
                      <?php
                            $message = Session::get('message');
                            if($message){
                                echo '<span class="text-alert">'.$message.'</span>';
                                Session::put('message',null);
                            }
                            ?>

      <table class="table table-striped b-t b-light">
        <thead>
          <tr>
            <th style="width:20px;">
              No
            </th>
            <th>Product's name</th>
            <th>Inventory number</th>
            <th>shipping</th>
            <th>Sales Tax</th>
            <th>Amount</th>
            <th>Product price ($)</th>


            <th style="width:30px;"></th>
          </tr>
        </thead>
        <tbody>
          @php

          $i = 0;
          $total = 0;
          @endphp
        @foreach($order_details as $key => $details)
        <?php $total_after_fee = $details->product_price - $details->product_sale_price ?>
          @php
          $i++;
          $subtotal = $total_after_fee*$details->product_sales_quantity;
          $total+=$subtotal;
          $vat = ($details->product_price-$details->product_sale_price)*0.0825;
          @endphp
          <tr class="color_qty_{{$details->product_id}}">

            <td><i>{{$i}}</i></td>
            <td>{{$details->product_name}}</td>
            <td>{{$details->product->product_quantity}}</td>
            <td>{{number_format($details->product_feeship )}}</td>
            <td>{{number_format($vat)}}</td>
            <td>
                {{$details->product_sales_quantity}}

            </td>

            <td>{{number_format( $total_after_fee)}}</td>

          </tr>
        @endforeach
          <tr>
            <td colspan="2">
            @php
                $total_coupon = 0;
              @endphp

                  @php

                  $total_coupon = $total + $total*0.0825 + $details->product_feeship ;

                  @endphp


           </br>

           Shipping: ${{number_format($details->product_feeship )}}<br>
           Total Sales Tax: ${{number_format($total*0.0825)}}<br>
           Total product price: ${{number_format($total)}}<br>
             <span style="font-weight: bold;font-size:16px">Payment: ${{number_format($total_coupon)}}</span>
            </td>
          </tr>
          <tr>
            <td colspan="6">
              @foreach($order as $key => $or)
                @if($or->order_status==1)
                <form>
                   @csrf
                  <select class="form-control order_details">
                    <option value="">--Choose your order form--</option>
                    <option id="{{$or->order_id}}" selected value="1">No process</option>
                    <option id="{{$or->order_id}}" value="2">Processed - Delivered</option>
                    <option id="{{$or->order_id}}" value="3">Cancellation - Hold</option>
                  </select>
                </form>
                @elseif($or->order_status==2)
                <form>
                  @csrf
                  <select class="form-control order_details">
                    <option value="">--Choose your order form--</option>
                    <option id="{{$or->order_id}}" value="1">No process</option>
                    <option id="{{$or->order_id}}" selected value="2">Processed - Delivered</option>
                    <option id="{{$or->order_id}}" value="3">Cancellation - Hold</option>
                  </select>
                </form>

                @else
                <form>
                   @csrf
                  <select class="form-control order_details">
                    <option value="">--Choose your order form---</option>
                    <option id="{{$or->order_id}}" value="1">No process</option>
                    <option id="{{$or->order_id}}"  value="2">Processed - Delivered</option>
                    <option id="{{$or->order_id}}" selected value="3">Cancellation - Hold</option>
                  </select>
                </form>

                @endif
                @endforeach


            </td>
          </tr>
        </tbody>
      </table>
      <a target="_blank" href="{{url('/print-order/'.$details->order_code)}}">Print order</a>
    </div>

  </div>
</div>
@endsection
