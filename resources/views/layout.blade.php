<!doctype html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset="UTF-8">
    <title>Handmadeofamerica.com</title>
    <meta property="og:title" content="Handmade launched because of you - our Customers" />
    <meta property="og:locale" content="en" />
    <meta property="og:image" content="https://handmadeofamerica.com/public/frontend/images/web.png" />
    <meta name="description"
        content="Handmade launched because of you - our Customers. Your desire to support local Artisans and Makers around the world inspired us to launch this category." />
    <meta property="og:description"
        content="Handmade launched because of you - our Customers. Your desire to support local Artisans and Makers around the world inspired us to launch this category." />
    <meta property="og:type" content="website" />
    <link rel="canonical" href="https://handmadeofamerica.com" />
    <meta property="og:url" content="https://handmadeofamerica.com" />
    <meta property="og:site_name" content="Handmade" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <base href="{{('/public/frontend/')}}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/sweetalert.css" rel="stylesheet">
    <link href="css/lightslider.css" rel="stylesheet">
    <link href="css/prettify.css" rel="stylesheet">
    <link href="css/lightgallery.min.css" rel="stylesheet">

    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
    <!-- Templatemela added -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="themes/PRS050093/assets/css/theme.css" type="text/css" media="all">
    <link rel="stylesheet" href="modules/psblog/views/css/psblog.css" type="text/css" media="all">
    <link rel="stylesheet" href="js/jquery/ui/themes/base/minified/jquery-ui.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="js/jquery/ui/themes/base/minified/jquery.ui.theme.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="modules/ct_imageslider/views/css/flexslider.css" type="text/css" media="all">
    <link rel="stylesheet" href="themes/PRS050093/assets/css/custom.css" type="text/css" media="all">
    <script type="text/javascript" src="themes/home.js"></script>


</head>

<body id="index" class="lang-en country-us currency-usd layout-full-width page-index tax-display-disabled">

    <main>
        <header id="header">
            <nav class="header-nav">
                <div class="container">
                    <div class="row">
                        <div class="hidden-lg-up text-xs-left mobile">
                            <div class="pull-xs-left hidden-lg-up" id="menu-icon">
                                <i class="material-icons menu-open">&#xE5D2;</i>
                                <i class="material-icons menu-close">&#xE5CD;</i>
                            </div>
                            <div id="mobile_top_menu_wrapper" class="row hidden-lg-up" style="display:none;">
                                <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
                                <div class="js-top-menu-bottom">
                                    <div id="_mobile_currency_selector"></div>
                                    <div id="_mobile_language_selector"></div>
                                    <div id="_mobile_contact_link"></div>
                                    <div id="_mobile_user_info"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="left-nav" id="ctcmsblock">
                            <div class=" pull-right">

                                <ul class="nav navbar-nav">

                                    <li>
                                        <a href="{{'/'}}">
                                            <span class="material-icons">
                                                home
                                            </span>
                                            home
                                        </a>
                                    </li>
                                    <?php
                                       $customer_id = Session::get('customer_id');
                                       $shipping_id = Session::get('shipping_id');
                                       if($customer_id!=NULL && $shipping_id==NULL){
                                     ?>
                                    <li>
                                        <a href="{{URL::to('/checkout')}}">
                                            <span class="material-icons">
                                                payment
                                            </span>
                                            Payment
                                        </a>
                                    </li>
                                    <?php
                                     }elseif($customer_id!=NULL && $shipping_id!=NULL){
                                     ?>
                                    <li><a href="{{URL::to('/payment')}}"><span class="material-icons">
                                                payment
                                            </span>Payment</a></li>
                                    <?php
                                    }else{
                                    ?>
                                    <li><a href="{{URL::to('/sign-in')}}"><span class="material-icons">
                                                payment
                                            </span> Payment</a></li>
                                    <?php
                                     }
                                    ?>
                                    <li>
                                        <a href="{{URL::to('/cart')}}"><span class="material-icons">
                                                add_shopping_cart
                                            </span> Cart
                                        </a>
                                    </li>

                                    <?php
                                            $customer_id = Session::get('customer');
                                            if($customer_id!=NULL){
                                        ?>
                                    <li>
                                        @if($customer_id==true)
                                        <a href="{{('/my-account')}}">
                                            <span class="material-icons">
                                                person
                                            </span>{{ $customer_id->customer_first_name }}<span>&nbsp;{{ $customer_id->customer_last_name }}</span></a>
                                        @else
                                        @endif
                                    </li>
                                    <li>
                                        <a href="{{URL::to('/logout-checkout')}}"><span class="material-icons">
                                                exit_to_app
                                            </span>
                                            Sign out </a>

                                    </li>

                                    <?php
                                }else{
                                     ?>
                                    <li><a href="{{URL::to('/sign-in')}}"><span class="material-icons">
                                                exit_to_app
                                            </span> Sign in</a></li>
                                    <?php
                                 }
                                     ?>


                                </ul>
                            </div>
                        </div>
                        <div class="right-nav">
                            <!-- Block search module TOP -->
                            <div class="col-lg-4 col-md-5 col-sm-12 search-widget"
                                data-search-controller-url="indexbd1a.html?controller=search">
                                <span class="search_button"><i class="material-icons search">&#xE8B6;</i><i
                                        class="material-icons cross">&#xE14C;</i><span>Search</span></span>
                                <div>
                                    <form action="{{URL::to('/Search')}}" method="POST">
                                        {{csrf_field()}}
                                        <input style="margin-top: 4px" type="text" name="keywords_submit" autocomplete="off" id="keywords"
                                            placeholder="Search..." />
                                        <div id="search_ajax"></div>
                                        <button type="submit">
                                            <i class="material-icons search">&#xE8B6;</i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                            <!-- /Block search module TOP -->
                            <div id="desktop_cart">
                                <div class="blockcart cart-preview inactive"
                                    data-refresh-url="index22d1.json?fc=module&amp;module=ps_shoppingcart&amp;controller=ajax&amp;id_lang=1">
                                    <div class="header blockcart-header dropdown js-dropdown">
                                        <a rel="nofollow" href="{{url('/cart')}}">
                                            <i class="material-icons shopping-cart"></i>
                                            <span class="hidden-md-down title">Cart</span>

                                            @php
                                            $total_qty = 0;
                                            @endphp
                                            @if(Session::get('cart')==true)
                                            @foreach(Session::get('cart') as $key => $cart)
                                            @php
                                            $subtotal = $cart['product_qty'];
                                            $total_qty+=$subtotal;
                                            @endphp
                                            @endforeach
                                            <span class="cart-products-count"> {{$total_qty}}</span>
                                            @else

                                            <span class="cart-products-count"> 0</span>
                                            @endif

                                        </a>
                                    </div>
                                </div>
                            </div>
                            <i class="material-icons more-icon">&#xE5D4;</i>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="header-top hidden-md-down">
                <div class="container">
                    <div class="row">
                        <div class="top-logo" id="_mobile_logo"></div>
                        <div class="main-logo">
                            <div class="header_logo" id="_desktop_logo">
                                <a href="{{'/'}}">
                                    <img class="logo " src="images/logo.png" width="500" height="auto" alt="">
                                </a>
                            </div>
                        </div>
                        <div id="ctcmsblock">
                            <div class="cmsblock cmsblock slide">
                                <ul>
                                    <li>
                                        <a href="{{'/'}}">
                                            <span class="material-icons">
                                                home
                                            </span>
                                            home
                                        </a>
                                    </li>
                                    <?php
                                   $customer_id = Session::get('customer_id');
                                   $shipping_id = Session::get('shipping_id');
                                   if($customer_id!=NULL && $shipping_id==NULL){
                                 ?>
                                    <li>
                                        <a href="{{URL::to('/checkout')}}">
                                            <span class="material-icons">
                                                payment
                                            </span>
                                            Payment
                                        </a>
                                    </li>
                                    <?php
                                 }elseif($customer_id!=NULL && $shipping_id!=NULL){
                                 ?>
                                    <li><a href="{{URL::to('/payment')}}"><span class="material-icons">
                                                payment
                                            </span>Payment</a></li>
                                    <?php
                                }else{
                                ?>
                                    <li><a href="{{URL::to('/sign-in')}}"><span class="material-icons">
                                                payment
                                            </span> Payment</a></li>
                                    <?php
                                 }
                                ?>
                                    <li><a href="{{URL::to('/cart')}}"><span class="material-icons">
                                                add_shopping_cart
                                            </span> Cart</a></li>
                                    <?php
                                        $customer_id = Session::get('customer_id');
                                        if($customer_id!=NULL){
                                    ?>
                                    <li><a href="{{URL::to('/logout-checkout')}}"><span class="material-icons">
                                                exit_to_app
                                            </span>
                                            Sign out</a></li>
                                    <?php
                            }else{
                                 ?>
                                    <li><a href="{{URL::to('/sign-in')}}"><span class="material-icons">
                                                exit_to_app
                                            </span> Sign in</a></li>
                                    <?php
                             }
                                 ?>

                                </ul>

                            </div>
                        </div>
                        <div class="pull-xs-right" id="_mobile_cart"></div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="menu col-lg-8 col-md-7 js-top-menu position-static hidden-sm-down"
                        id="_desktop_top_menu">
                        <ul class="top-menu" id="top-menu" data-depth="0">
                            <li class="cms-page" id="cms-page-4">
                                <a class="dropdown-item" href="{{'/'}}" data-depth="0">
                                    Home
                                </a>
                            </li>
                            @foreach($category as $key => $cate)
                            <li class="category" id="category-3">
                                <a class="dropdown-item" href="{{URL::to('/category/'.$cate->slug_category_product)}}"
                                    data-depth="0">
                                    {{$cate->category_name}}
                                </a>
                            </li>
                            @endforeach
                            @foreach($brand as $key => $brand)
                            <li class="category" id="category-3">
                                <a class="dropdown-item" href="{{URL::to('/product-sale/'.$brand->brand_slug)}}"
                                    data-depth="0">
                                    {{$brand->brand_name}}
                                </a>
                            </li>
                            @endforeach
                            <li class="cms-page" id="cms-page-4">
                                <a class="dropdown-item" href="indexac29.html?id_cms=4&amp;controller=cms&amp;id_lang=1"
                                    data-depth="0">
                                    About us
                                </a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <nav data-depth="1" class="breadcrumb hidden-sm-down">
                <div class="container">
                    <ol itemscope itemtype="">
                        <li itemprop="itemListElement" itemscope itemtype="">
                            <a itemprop="item" href="index.html">
                                <span itemprop="name">Home</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                    </ol>
                </div>
            </nav>
        </header>
        <aside id="notifications">
            <div class="container">
            </div>
        </aside>
        <section id="wrapper">
            <div class="home_container">
                <div id="columns_inner">
                    <div id="content-wrapper">
                        <section id="main">
                            <section id="content" class="page-home">
                                @yield('content')


                                <footer id="footer">

                                    <div class="footer-container hb-animate-element right-to-left">
                                        <div class="container">
                                            <div class="row">
                                                <div class="footer">
                                                    <div id="ctfootercmsblock" class="block">
                                                        <div class="block_about">
                                                            <div class="toggle-footer">
                                                                <div class="footer_logo"><a href="{{'/'}}"><img
                                                                            src="images/logo.png"
                                                                            alt="footer logo" /></a></div>
                                                                <div class="footer-about-desc">
                                                                    <p style="text-align: justify">Handmade launched
                                                                        because of you - our Customers. Your desire to
                                                                        support local Artisans and Makers around the
                                                                        world inspired us to launch this category.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="block-contact footer-block col-xs-12 col-sm-4 links wrapper">
                                                        <h3 class="text-uppercase block-contact-title hidden-md-down">
                                                            Contact Us
                                                        </h3>
                                                        <div class="title clearfix hidden-lg-up"
                                                            data-target="#block-contact_list" data-toggle="collapse">
                                                            <span class="h3"> Contact Us</span>
                                                            <span class="pull-xs-right">
                                                                <span class="navbar-toggler collapse-icons">
                                                                    <i class="material-icons add">&#xE313;</i>
                                                                    <i class="material-icons remove">&#xE316;</i>
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <ul id="block-contact_list" class="collapse">

                                                            <li>

                                                                <i class="material-icons "></i>
                                                                <span>4311 tristan Ridge ln
                                                                    Katy ,tx 77449
                                                                </span>

                                                            </li>
                                                            {{-- <li>
                                                                <i class="material-icons phone">&#xE324;</i>
                                                                <span>8323100713</span>
                                                            </li> --}}

                                                            <li>
                                                                <i class="material-icons mail">&#xE554;</i>
                                                                <span>handmadeofamerica@gmail.com</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-4 links block links">
                                                        <h3 class="h3 hidden-md-down"><i
                                                                class="material-icons">&#xE3C7;</i>Products
                                                        </h3>
                                                        <div class="title h3 block_title hidden-lg-up"
                                                            data-target="#footer_sub_menu_58064" data-toggle="collapse">
                                                            <span class="">Products</span>
                                                            <span class="pull-xs-right">
                                                                <span class="navbar-toggler collapse-icons">
                                                                    <i class="material-icons add">&#xE313;</i>
                                                                    <i class="material-icons remove">&#xE316;</i>
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <ul id="footer_sub_menu_58064" class="collapse block_content">
                                                            @foreach($category as $key => $cate)
                                                            <li>
                                                                <a href="{{URL::to('/category/'.$cate->slug_category_product)}}"
                                                                    data-depth="0">
                                                                    {{$cate->category_name}}
                                                                </a>
                                                            </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-4 links block links">
                                                        <h3 class="h3 hidden-md-down"><i
                                                                class="material-icons">&#xE3C7;</i>Our
                                                            company</h3>
                                                        <div class="title h3 block_title hidden-lg-up"
                                                            data-target="#footer_sub_menu_4617" data-toggle="collapse">
                                                            <span class="">Our company</span>
                                                            <span class="pull-xs-right">
                                                                <span class="navbar-toggler collapse-icons">
                                                                    <i class="material-icons add">&#xE313;</i>
                                                                    <i class="material-icons remove">&#xE316;</i>
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <ul id="footer_sub_menu_4617" class="collapse block_content">
                                                            <a target="_blank" href="{{'/about-us'}}">
                                                                <li>About Us</li>
                                                            </a>

                                                            <a target="_blank" href="{{'/terms-privacy'}}">
                                                                <li>Privacy Policy</li>
                                                            </a>
                                                            <a target="_blank" href="{{'/terms-conditions'}}">
                                                                <li>Terms & Conditions</li>
                                                            </a>
                                                            <a target="_blank" href="{{'/contact-us'}}">
                                                                <li>Contact us</li>
                                                            </a>
                                                            <a href="{{'/my-account'}}">
                                                                <li>My Account</li>
                                                            </a>



                                                        </ul>
                                                    </div>

                                                    <div class="block-social">
                                                        <ul>
                                                                  <li class="facebook"><a href="https://www.facebook.com/Handmadeofamerica-102382851720896" target="_blank">Facebook</a></li>

                                                                  <li class="instagram"><a href="https://www.instagram.com/handmadeofamerica/" target="_blank">Instagram</a></li>
                                                              </ul>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer-bottom">
                                        <div class="container">
                                            <div class="row">
                                                <div class="footer-after col-md-12">
                                                    <p class="copyright hb-animate-element bottom-to-top">
                                                        <a class="_blank" href="{{'/'}}" target="_blank">
                                                            © 2020 - Ecommerce software by Handmade Of America
                                                        </a>
                                                    </p>
                                                    <div id="ctpaymentcmsblock">
                                                        <ul class="payment-cms hb-animate-element bottom-to-top">
                                                            {{-- <li class="visa"><a href="#"></a></li> --}}
                                                            <li class="paypal"><a href="javascript:"></a></li>
                                                            {{-- <li class="mastercard"><a href="#"></a></li>
                                                            <li class="discover"><a href="#"></a></li> --}}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a class="top_button" href="#" style=""><span><i
                                                class="material-icons">&#xE5C7;</i></span></a>
                                </footer>
    </main>
    <script type="text/javascript" src="themes/core.js"></script>
    <script type="text/javascript" src="themes/PRS050093/assets/js/theme.js"></script>
    <script type="text/javascript" src="js/jquery/ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="modules/ps_searchbar/ps_searchbar.js"></script>
    <script type="text/javascript" src="modules/ps_shoppingcart/ps_shoppingcart.js"></script>
    <script type="text/javascript" src="modules/ct_imageslider/views/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="themes/PRS050093/assets/js/owl.carousel.js"></script>
    <script type="text/javascript" src="themes/PRS050093/assets/js/totalstorage.js"></script>
    <script type="text/javascript" src="themes/PRS050093/assets/js/waypoints.min.js"></script>
    <script type="text/javascript" src="themes/PRS050093/assets/js/inview.js"></script>
    <script type="text/javascript" src="themes/PRS050093/assets/js/lightbox-2.6.min.js"></script>
    <script type="text/javascript" src="themes/PRS050093/assets/js/custom.js"></script>


    <script src="js/notify.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/sweetalert.min.js"></script>
    <script src="js/lightslider.js"></script>
    <script src="js/prettify.js"></script>
    <script src="js/lightgallery-all.min.js"></script>

    <script src="js/jquery.form-validator.min.js"></script>
    <script type="text/javascript">
        $.validate({
    });
    </script>

    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v9.0'
        });
      };

      (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Your Chat Plugin code -->
    <div class="fb-customerchat"
      attribution=setup_tool
      page_id="102382851720896">
    </div>

    <script type="text/javascript">
        $('#keywords').keyup(function(){
            var query = $(this).val();
            if(query != ''){
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url : '{{url('/autocomple-ajax')}}',
                    method: 'POST',
                    data:{query:query,_token:_token},
                    success:function(data){
                        $('#search_ajax').fadeIn();
                        $('#search_ajax').html(data);
                    }
                });

            }else{
                $('#search_ajax').fadeOut();
            }
        $(document).on('click','.li_search_ajax',function(){
            $('#keywords').val($(this).text());
            $('#search_ajax').fadeOut();
        });
    });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
              $('#imageGallery').lightSlider({
                  gallery:true,
                  item:1,
                  loop:true,
                  thumbItem:3,
                  slideMargin:0,
                  enableDrag: false,
                  currentPagerPosition:'left',
                  onSliderLoad: function(el) {
                      el.lightGallery({
                          selector: '#imageGallery .lslide'
                      });
                  }
              });
          });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#idform").submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var url = '{{url('/confirm-order')}}';
                        $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(), // serializes the form's elements.
                        beforeSend: function() {
                            $(':input[type="submit"]').prop('disabled', true);
                        },
                        success: function (data) {

                        if($.isEmptyObject(data.error)){
                                document.location = data;
                            }else{
                                alert('Please select a city state');
                                $(':input[type="submit"]').prop('disabled', false);

                            }
                        }
                    });
            });
        });


    </script>

    <script type="text/javascript">
        $(document).ready(function(){

                $('.city').change(function(){
                    var matp = $('.city').val();
                    var _token = $('input[name="_token"]').val();
                    if(matp == ''){

                    }else{
                        $.ajax({
                        url : '{{url('/calculate-fee')}}',
                        method: 'POST',
                        data:{matp:matp,_token:_token},
                        success:function(data){
                            $("#fee_ajax").html(data);
                            var total = parseFloat($('#total_price').val()) + parseFloat(data);

                            $("#total_112").html(new Intl.NumberFormat().format(Math.round(total) ));

                        }
                        });
                    }
            });

      });
    </script>
    <script type="text/javascript">
        // add to cart quick view

          $(document).on('click','.add-to-cart-quickview',function(){
              var id = $(this).data('id_product');
              var cart_product_id = $('.cart_product_id_' + id).val();
              var cart_product_name = $('.cart_product_name_' + id).val();
              var cart_product_image = $('.cart_product_image_' + id).val();
              var cart_product_quantity = $('.cart_product_quantity_' + id).val();
              var cart_product_price = $('.cart_product_price_' + id).val();
              var cart_product_sale_price = $('.cart_product_sale_price_' + id).val();
              var cart_product_qty = $('.cart_product_qty_' + id).val();
              var _token = $('input[name="_token"]').val();
              if(parseInt(cart_product_qty)>parseInt(cart_product_quantity)){
                  alert('Smaller please ' + cart_product_quantity);
              }else{
                  $.ajax({
                      url: '{{url('/add-cart-ajax')}}',

                      method: 'POST',
                      data:{cart_product_id:cart_product_id,cart_product_name:cart_product_name,cart_product_image:cart_product_image,cart_product_price:cart_product_price,cart_product_sale_price:cart_product_sale_price,cart_product_qty:cart_product_qty,_token:_token,cart_product_quantity:cart_product_quantity},

                      success:function(data){
                        $.notify(  'Product added to cart', "success" );
                        $('.cart-products-count').html(data);

                      }
                  });
              }
          });

          $(document).on('click','.redirect-cart',function(){
              window.location.href = "{{url('/cart')}}";
          });
    </script>

    <script type="text/javascript">
        // sweetalert
          $(document).ready(function(){
              $('.add-to-cart').click(function(){
                  var id = $(this).data('id_product');
                  var cart_product_id = $('.cart_product_id_' + id).val();
                  var cart_product_name = $('.cart_product_name_' + id).val();
                  var cart_product_image = $('.cart_product_image_' + id).val();
                  var cart_product_quantity = $('.cart_product_quantity_' + id).val();
                  var cart_product_price = $('.cart_product_price_' + id).val();
                  var cart_product_sale_price = $('.cart_product_sale_price_' + id).val();
                  var cart_product_qty = $('.cart_product_qty_' + id).val();
                  var _token = $('input[name="_token"]').val();
                  if(parseInt(cart_product_qty)>parseInt(cart_product_quantity)){
                      alert('Smaller please ' + cart_product_quantity);
                  }else{
                      $.ajax({
                          url: '{{url('/add-cart-ajax')}}',

                          method: 'POST',
                          data:{cart_product_id:cart_product_id,cart_product_name:cart_product_name,cart_product_image:cart_product_image,cart_product_price:cart_product_price,cart_product_sale_price:cart_product_sale_price,cart_product_qty:cart_product_qty,_token:_token,cart_product_quantity:cart_product_quantity},
                          success:function(data){
                            $('.cart-products-count').html(data);
                              swal({
                                      title: "Product added to cart",
                                      text: "",
                                      showCancelButton: true,
                                      cancelButtonText: "See more",
                                      confirmButtonClass: "btn-success",
                                      confirmButtonText: "Go to cart",
                                      closeOnConfirm: false
                                },
                                  function() {
                                      window.location.href = "{{url('/cart')}}";
                                  });

                          }
                      });
                  }
              });
          });

    </script>
    <script type="text/javascript">
        $('.exampleModal').click(function(){


            var product_id = $(this).data('id_product');
            var cart_product_id = $('.cart_product_id_' + product_id).val();
            var _token = $('input[name="_token"]').val();
            $.ajax({
                url: '{{url('/quickview')}}',
                        method: 'POST',
                        dataType: "JSON",
                        data:{product_id:product_id,_token:_token},
                        success:function(data){
                            $('#product_quickview_title').html(data.product_name);
                            $('#product_quickview_id').html(data.product_id);
                            $('#product_quickview_price').html(data.product_price);
                            $('#product_quickview_sale_price').html(data.product_sale_price);
                            $('#product_quickview_image').html(data.product_image)
                            $('#price_quickview').html(data.price_quickview);
                            $('#product_quickview_gallery').html(data.product_gallery);
                            $('#product_quickview_desc').html(data.product_desc);
                            $('#product_quickview_desc_1').html(data.product_desc);
                            $('#product_quickview_content').html(data.product_content);
                            $('#product_quickview_value').html(data.product_quickview_value);
                            $('#product_quickview_button').html(data.product_button);
                            $('#product_quickview_button_1').html(data.product_button_1);

                        }
            });
        });
    </script>
    <style>
        .bootstrap-touchspin-up,
        .bootstrap-touchspin-down {
            display: none;
        }

        .nav>li>a:hover,
        .nav>li>a:focus {
            text-decoration: none;
            background-color: #000;
        }
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus {
    color:black;
    cursor: default;
    background-color: none;
    border:none;
    border-bottom-color: none;
    }


    </style>
</body>

</html>
