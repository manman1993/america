<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    public $timestamps = false; //set time to false
    protected $fillable = [
        'customer_id','first_name','last_name', 'shipping_address', 'shipping_phone',
        'shipping_notes','order_code','zip_code','shipping_city',
    ];
    protected $primaryKey = 'shipping_id';
 	protected $table = 'tbl_shipping';
}
