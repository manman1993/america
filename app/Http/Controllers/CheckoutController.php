<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Cart;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

session_start();
require __DIR__ . "/vendor/autoload.php";

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use App\City;
use App\Province;
use App\Wards;
use App\Paypal;
use App\Feeship;
use App\Slider;
use App\Shipping;
use App\Order;
use App\Customer;
use App\OrderDetails;
use Mail;
use Validator;

class CheckoutController extends Controller
{

    public function error()
    {
        return view('pages.checkout.error');
    }
    public function confirm_order(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'city' => 'required',

        ]);

        if (!$validator->passes()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }
        $data = $request->all();
        $checkout_code = substr(md5(microtime()), rand(0, 26), 5);

        $shipping = new Shipping();
        $shipping->first_name = $data['first_name'];
        $shipping->last_name = $data['last_name'];
        $shipping->shipping_phone = $data['shipping_phone'];
        $shipping->zip_code = $data['zip_code'];
        $shipping->shipping_address = $data['shipping_address'];
        $shipping->shipping_notes = $data['shipping_notes'];
        $shipping->shipping_city = $data['shipping_city'];
        $shipping->order_code = $checkout_code;
        // $shipping->shipping_method = $data['shipping_method'];
        $shipping->customer_id = Session::get('customer_id');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $shipping->created_at = now();
        $shipping->save();
        $shipping_id = $shipping->shipping_id;
        $checkout_code = substr(md5(microtime()), rand(0, 26), 5);



        $order = new Order;
        $order->customer_id = Session::get('customer_id');
        $order->shipping_id = $shipping_id;
        $order->order_status = 1;
        $order->order_code = $checkout_code;


        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $order->created_at = now();
        $order->save();

        if (Session::get('cart') == true) {
            foreach (Session::get('cart') as $key => $cart) {
                $order_details = new OrderDetails;
                $order_details->order_code = $checkout_code;
                $order_details->product_id = $cart['product_id'];
                $order_details->product_name = $cart['product_name'];
                $order_details->product_price = $cart['product_price'];
                $order_details->product_sale_price = $cart['product_sale_price'];
                $order_details->product_sales_quantity = $cart['product_qty'];
                // $order_details->product_coupon =  $data['order_coupon'];
                $order_details->product_feeship = Session::get('fee');
                $order_details->save();
            }
        }

        $cart = Session::get('cart');
        $total = 0;
        foreach (Session::get('cart') as $key => $cartpay) {

            $subtotal = ($cartpay['product_price'] - $cartpay['product_sale_price']) * $cartpay['product_qty'];
            $total += $subtotal;
        }
        $total_after_fee =round($total + Session::get('fee') + ($total*0.0825),2);



        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $order->shipping_fee = Session::get('fee');
        $order->total = $total_after_fee;
        $order->created_at = now();
        $order->save();
        // Creating an environment
        $clientId = "ASHXLg6XEKwoyU1o8FtkMyKsMwIMcmq7kldWC6u2Z6jaEoaKsUUy7aZoM5Q3n0MSDjVGqnHohJYUjZto";
        $clientSecret = "EOtS87TEyzBclYeztS7FNOCRbehmXZxwQW5WCQ42ab2yXQ8JC4kCIjPlrWCeCdKsGsd9Agd08q_wMU7C";

        $environment = new SandboxEnvironment($clientId, $clientSecret);
        $client = new PayPalHttpClient($environment);
        //var_dump($client);
        $request = new OrdersCreateRequest();
        $request->prefer('return=representation');
        $request->body = [
            "intent" => "CAPTURE",
            "purchase_units" => [[
                "reference_id" => $shipping_id,
                "amount" => [
                    "value" => $total_after_fee,
                    "currency_code" => "USD"
                ]
            ]],
            "application_context" => [
                "cancel_url" => config('app.url')."/error",

                "return_url" => config('app.url')."/checkout-payment"
            ]
        ];

        try {
            // Call API with your client and get a response for your call
            $response = $client->execute($request);
            $response = json_decode(json_encode($response), 1);

            return $response['result']['links'][1]['href'];
        } catch (HttpException $ex) {
            // echo $ex->statusCode;
            // print_r($ex->getMessage());
        }
    }


    public function del_fee()
    {
        Session::forget('fee');
        return redirect()->back();
    }

    public function AuthLogin()
    {
        $admin_id = Session::get('admin_id');
        if ($admin_id) {
            return Redirect::to('dashboard');
        } else {
            return Redirect::to('admin')->send();
        }
    }
    public function calculate_fee(Request $request)
    {
        $data = $request->all();
        $count_feeship = 0;
        if ($data['matp']) {
            $feeship = Feeship::where('fee_matp', $data['matp'])->first();
            if ($feeship) {
                $count_feeship = $feeship->fee_feeship;
            }
            Session::put('fee', $count_feeship);
            Session::save();
        }
        return  $count_feeship;
    }
    public function select_delivery_home(Request $request)
    {
        $data = $request->all();
        if ($data['action']) {
            $output = '';
            if ($data['action'] == "city") {
                $select_province = Province::where('matp', $data['ma_id'])->orderby('maqh', 'ASC')->get();
                $output .= '<option>---Chọn quận huyện---</option>';
                foreach ($select_province as $key => $province) {
                    $output .= '<option value="' . $province->maqh . '">' . $province->name_quanhuyen . '</option>';
                }
            } else {

                $select_wards = Wards::where('maqh', $data['ma_id'])->orderby('xaid', 'ASC')->get();
                $output .= '<option>---Chọn xã phường---</option>';
                foreach ($select_wards as $key => $ward) {
                    $output .= '<option value="' . $ward->xaid . '">' . $ward->name_xaphuong . '</option>';
                }
            }
            echo $output;
        }
    }

    public function login_checkout(Request $request)
    {
        //slide
        $slider = Slider::orderBy('slider_id', 'DESC')->where('slider_status', '1')->take(4)->get();

        //seo
        $meta_desc = "Sign in to pay";
        $meta_keywords = "Sign in to pay";
        $meta_title = "Sign in to pay";
        $url_canonical = $request->url();
        //--seo

        $cate_product = DB::table('tbl_category_product')->where('category_status', '0')->orderby('category_id', 'asc')->get();
        $brand_product = DB::table('tbl_brand')->where('brand_status', '0')->orderby('brand_id', 'asc')->get();
        $all_special = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'special')->get();
        $all_sale = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'sale')->limit(4)->get();
        return view('pages.checkout.login_checkout')->with('all_sale', $all_sale)->with('all_special', $all_special)->with('category', $cate_product)
        ->with('brand', $brand_product)->with('meta_desc', $meta_desc)->with('meta_keywords', $meta_keywords)
        ->with('meta_title', $meta_title)->with('url_canonical', $url_canonical)->with('slider', $slider);
    }

    public function add_customer(Request $request)
    {

        $data = array();
        $data['customer_first_name'] = $request->customer_first_name;
        $data['customer_last_name'] = $request->customer_last_name;
        $data['customer_phone'] = $request->customer_phone;
        $data['customer_email'] = $request->customer_email;
        $data['customer_password'] = md5($request->customer_password);
        $customer_id = DB::table('tbl_customers')->insertGetId($data);
        Session::put('customer_id', $customer_id);
        Session::put('customer_name', $request->customer_first_name.$request->customer_last_name);
        return Redirect::to('/sign-in');
    }

    public function checkout(Request $request)
    {
        //seo
        //slide
        $slider = Slider::orderBy('slider_id', 'DESC')->where('slider_status', '1')->take(4)->get();

        $meta_desc = "Sign in to pay";
        $meta_keywords = "Sign in to pay";
        $meta_title = "Sign in to pay";
        $url_canonical = $request->url();
        //--seo

        $cate_product = DB::table('tbl_category_product')->where('category_status', '0')->orderby('category_id', 'asc')->get();
        $brand_product = DB::table('tbl_brand')->where('brand_status', '0')->orderby('brand_id', 'asc')->get();
        $city = City::orderby('matp', 'ASC')->get();

        return view('pages.checkout.show_checkout')->with('category', $cate_product)->with('brand', $brand_product)->with('meta_desc', $meta_desc)->with('meta_keywords', $meta_keywords)->with('meta_title', $meta_title)->with('url_canonical', $url_canonical)->with('city', $city)->with('slider', $slider);
    }
    public function checkout_payment(Request $request)
    {
        $data = $request->all();
        $clientId = "ASHXLg6XEKwoyU1o8FtkMyKsMwIMcmq7kldWC6u2Z6jaEoaKsUUy7aZoM5Q3n0MSDjVGqnHohJYUjZto";
        $clientSecret = "EOtS87TEyzBclYeztS7FNOCRbehmXZxwQW5WCQ42ab2yXQ8JC4kCIjPlrWCeCdKsGsd9Agd08q_wMU7C";
        $environment = new SandboxEnvironment($clientId, $clientSecret);
        // try{
            $client = new PayPalHttpClient($environment);
        // }catch(\HttpException $e){

        // }

        $token = $_REQUEST['token'];
        $request = new OrdersCaptureRequest($token);
        $request->prefer('return=representation');
        // try {
            // Call API with your client and get a response for your call
            $response = $client->execute($request);
            //   echo "<pre>";
            // print_r(json_encode($response));

            $response = json_decode(json_encode($response), 1);

            // If call returns body in response, you can get the deserialized version from the result attribute of the response

            $paypal = new Paypal;
            $id_paypal_new = $response['result']['id'];
            $id_shipping_new = $response['result']['purchase_units'][0]['reference_id'];
            $price_paypal_new = $response['result']['purchase_units'][0]['amount']['value'];
            $status_paypal_new = $response['result']['status'];
            $time_paypal_new = $response['result']['create_time'];
            $full_name = $response['result']['purchase_units'][0]['shipping']['name']['full_name'];

            $paypal->id_paypal = $id_paypal_new;
            $paypal->shipping_id = $id_shipping_new;
            $paypal->price_paypal = $price_paypal_new;
            $paypal->full_name = $full_name;
            $paypal->status = $status_paypal_new;
            $paypal->time_paypal = $time_paypal_new;
            $paypal->save();
            // Order-status-payment
            // DB::table('tbl_order')
            //     ->where('order_code', $user->id)
            //     ->update(['status' => 1]);
            // Email
            // $data = array();

            $order_user = DB::table('tbl_order')
                ->join('tbl_customers', 'tbl_order.customer_id', '=', 'tbl_customers.customer_id')
                ->join('tbl_shipping', 'tbl_order.shipping_id', '=', 'tbl_shipping.shipping_id')
                ->join('tbl_paypal', 'tbl_order.shipping_id', '=', 'tbl_paypal.shipping_id')
                ->select('tbl_order.*', 'tbl_customers.*', 'tbl_shipping.*', 'tbl_paypal.*')->where('tbl_order.shipping_id', $id_shipping_new)->first();

            $order_by_id = DB::table('tbl_order')
                ->join('tbl_order_details', 'tbl_order_details.order_code', '=', 'tbl_order.order_code')

                ->select('tbl_order.*', 'tbl_order_details.*')->where('tbl_order.shipping_id', $id_shipping_new)->get();

            DB::table('tbl_order')
                ->where('order_code', $order_by_id[0]->order_code)
                ->update(['status' => 1]);
            $output = '';
            $total_sum=0;
            foreach ($order_by_id as $pro) {

                $output .= '
                    <tr style="color:#000000;font-family:Helvetica,sans-serif;">
                    <td align="left" width="15"></td>
                    <td align="left" style="font-weight:normal">
                    <span style="font-size:11px;line-height:18px">
                    ';
                $output .= $pro->product_sales_quantity;
                $output .= '
                   x &nbsp;</span>
                    <span style="font-size:11px;font-weight:bold">
                    ';
                $output .= $pro->product_name;
                $output .= '
                    </span>
                    </td>
                    <td align="left" style="font-weight:normal">
                        <span style="font-size:11px;line-height:18px">&nbsp;&nbsp;
                    ';
                $output .= '$'.number_format(($pro->product_price - $pro->product_sale_price)*$pro->product_sales_quantity);
                $output .= '
                    </span>
                    </td>
                    <td align="right" width="15"></td>
                    </tr>
                    ';


                    $sum=($pro->product_price - $pro->product_sale_price)*($pro->product_sales_quantity);
                    $total_sum += $sum;

            }


            $to_name = 'HandMadeOfAmerica';
            $to_email = $order_user->customer_email; //send to this email
            $data = array(

                "customer_name" => $order_user->customer_first_name. $order_user->customer_last_name,
                "price" => $price_paypal_new,
                "time" =>   $time_paypal_new,
                "vat" =>  $total_sum,
                "output" =>  $output,
                "fee" => $order_user->shipping_fee,
            );
            //body of mail.blade.php
            try {
                Mail::send('pages.checkout.success', $data, function ($message) use ($to_name, $to_email) {
                    $message->to($to_email)->subject('Payment'); //send this mail with subject
                    $message->from("info@handmadeofamerica.com", $to_name); //send from this mail
                });
             }catch (\Exception $ex) {
                return redirect('/');
             }

                Session::forget('coupon');
                Session::forget('fee');
                Session::forget('cart');

        // }catch (\Exception $ex) {
        //         // return redirect('/');
        //     }

            return view('pages.checkout.show_checkout_payment')->with('paypal', $paypal);
    }

    public function save_checkout_customer(Request $request)
    {
        $data = array();
        $data['shipping_name'] = $request->shipping_name;
        $data['shipping_phone'] = $request->shipping_phone;
        // $data['shipping_email'] = $request->shipping_email;
        $data['shipping_notes'] = $request->shipping_notes;
        $data['shipping_address'] = $request->shipping_address;

        $shipping_id = DB::table('tbl_shipping')->insertGetId($data);

        Session::put('shipping_id', $shipping_id);

        return Redirect::to('/payment');
    }
    public function payment(Request $request)
    {
        //seo
        $meta_desc = "Sign in to pay";
        $meta_keywords = "Sign in to pay";
        $meta_title = "Sign in to pay";
        $url_canonical = $request->url();
        //--seo
        $cate_product = DB::table('tbl_category_product')->where('category_status', '0')->orderby('category_id', 'desc')->get();
        $brand_product = DB::table('tbl_brand')->where('brand_status', '0')->orderby('brand_id', 'desc')->get();
        return view('pages.checkout.payment')->with('category', $cate_product)->with('brand', $brand_product)->with('meta_desc', $meta_desc)->with('meta_keywords', $meta_keywords)->with('meta_title', $meta_title)->with('url_canonical', $url_canonical);
    }

    public function logout_checkout()
    {
        Session::forget('customer_id');
        Session::forget('customer');
        return Redirect::to('/sign-in');
    }
    public function login_customer(Request $request)
    {
        $email = $request->email_account;
        $password = md5($request->password_account);
        $result = DB::table('tbl_customers')->where('customer_email', $email)->where('customer_password', $password)->first();

        // if ($result) {
        //     Session::put('result', $result);
        //     return Redirect::to('/checkout');
        // } else {
        //     return Redirect::to('/sign-in');
        // }
        if ($result) {
            Session::put('customer_id', $result->customer_id);
            Session::put('customer', $result);

            return Redirect::to('/checkout');
        } else {
            return Redirect::to('/sign-in')->with(['error' => 'Invalid username or password']);
        }
        Session::save();

    }

    public function manage_order()
    {

        $this->AuthLogin();
        $all_order = DB::table('tbl_order')
            ->join('tbl_customers', 'tbl_order.customer_id', '=', 'tbl_customers.customer_id')
            ->select('tbl_order.*', 'tbl_customers.customer_name')
            ->orderby('tbl_order.order_id', 'desc')->get();
        $manager_order  = view('admin.manage_order')->with('all_order', $all_order);
        return view('admin_layout')->with('admin.manage_order', $manager_order);
    }
}
