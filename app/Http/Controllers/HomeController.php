<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Shipping;
use App\Gallery;
use App\Product;
use App\Http\Requests;
use Mail;
use App\Slider;
use App\OrderDetails;
use App\Order;
use App\Customer;
use App\Forgotpass;
use Illuminate\Support\Facades\Redirect;
session_start();

class HomeController extends Controller
{
    public function error_page(){
        return view('errors.404');
    }
    public function autocomple_ajax(Request $request){
        $data = $request->all();
        if($data['query']){
            $product = Product::where('product_status',0)->where('product_name','like','%'.$data['query'].'%')->get();
            $output = '
                <ul class="dropdown-menu" style="display:block; position:absolute;width: 100%;border-radius: 5px;"> ';
                foreach($product as $key => $val){
                    $output .='
                        <li class="li_search_ajax"><a style="color:black" href="javacript:">'.$val->product_name.'</a></li>
                    ';
                }
                $output .=' </ul>';

            echo $output;
        }
    }
    public function send_mail(){

                $to_name = "";
                $to_email = "";//send to this email


                $data = array("name"=>"","body"=>''); //body of mail.blade.php
                Mail::send('pages.send_mail',$data,function($message) use ($to_name,$to_email){
                    $message->to($to_email)->subject('');//send this mail with subject
                    $message->from($to_email,$to_name);//send from this mail
                });
                return redirect('/')->with('message','');
                //--send mail
    }


    public function index(Request $request){
        $product_id = $request->product_id;
        //slide
        $slider = Slider::orderBy('slider_id','DESC')->where('slider_status','1')->take(4)->get();
        //seo
        $meta_desc = "";
        $meta_keywords = "";
        $meta_title = "";
        $url_canonical = $request->url();
        $num  = $request->numpage;
        //--seo
    	$cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','asc')->get();
        $brand_product = DB::table('tbl_brand')->where('brand_status','0')->orderby('brand_id','desc')->get();
        $all_product = DB::table('tbl_product')->where('product_status','0')->orderby(DB::raw('RAND()'))->paginate(4);
        $all_special = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'special')->get();
        $all_sale = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'sale')->get();
        $result= Session::get('customer');

        $gallery = Gallery::where('product_id', $product_id)->get();
        return view('pages.home')->with('gallery',$gallery)->with('gallery',$gallery)->with('result',$result)->with('all_special',$all_special)->with('category',$cate_product)->with('brand',$brand_product)->with('all_product',$all_product)->with('meta_desc',$meta_desc)->with('meta_keywords',$meta_keywords)->with('meta_title',$meta_title)->with('url_canonical',$url_canonical)->with('slider',$slider); //1

    }

    public function load_data(Request $request){
        if($request->ajax())
        {
            $product_slug =$request->product_slug;
            $num  = $request->numpage;
            $branch = $request->branch;
            $category = $request->category;
            $id = $request->id;
            $keywords = $request->keywords_submit;
            $field_sort = $request->field_sort;
            $type_sort = $request->type_sort;
            if($field_sort == "none" || $type_sort == "none"){
                $field_sort = "product_price";
                $type_sort = "DESC";
            }
            if($branch != null){
                $data = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', $branch)->orderBy( $field_sort,$type_sort)->paginate($num);
                $count = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', $branch)->count();

            }else if($category != null){
                $data = DB::table('tbl_product')->join('tbl_category_product', 'tbl_product.category_id', '=', 'tbl_category_product.category_id')->where('tbl_category_product.slug_category_product', $category)->orderBy($field_sort,$type_sort)->paginate($num);
                $count = DB::table('tbl_product')->join('tbl_category_product', 'tbl_product.category_id', '=', 'tbl_category_product.category_id')->where('tbl_category_product.slug_category_product', $category)->count();
            }else if($id != null){
                $data = DB::table('tbl_product')->join('tbl_category_product', 'tbl_product.category_id', '=', 'tbl_category_product.category_id')->where('tbl_product.category_id', $id)->whereNotIn('tbl_product.product_slug', [$product_slug])->orderBy( $field_sort,$type_sort)->paginate($num);
                $count = DB::table('tbl_product')->join('tbl_category_product', 'tbl_product.category_id', '=', 'tbl_category_product.category_id')->where('tbl_product.category_id', $id)->whereNotIn('tbl_product.product_slug', [$product_slug])->count();
            }else{
                $data = DB::table('tbl_product')->where('product_name','like','%'.$keywords.'%')->orderBy( $field_sort,$type_sort)->paginate($num);
                $count = DB::table('tbl_product')->where('product_name','like','%'.$keywords.'%')->count();
            }

            $output='';
            if(!$data->isEmpty()){
                foreach($data as $product){

                $output.='
                <li class="product_item col-xs-12 col-sm-6 col-md-4 col-lg-2">
                    <div class="product-miniature js-product-miniature">
                        <div class="thumbnail-container">

                            <a href="'.url('/product/'.$product->product_slug).'">
                                <img src="'.url('public/uploads/product/'.$product->product_image).'"
                                    alt="" />
                            </a>

                            <ul class="product-flags">
                                <li class="new">New</li>
                            </ul>

                            <div class="product-hover">
                                <a href="#"  class="quick-view" data-id_product="'.$product->product_id.'"     data-link-action="quickview">
                                    <i class="material-icons search"></i> Quick view
                                </a>


                            </div>

                        </div>
                        <div class="product-description">
                            <h1 class="h3 product-title" itemprop="name">
                                <a
                                    href="'.url('/product/'.$product->product_slug).'">'.$product->product_name.'</a>
                            </h1>
                            ';
                            if($product->product_sale_price !=0){
                                $output.='
                                <div class="product-price-and-shipping">
                                    <span itemprop="price"
                                        class="price">'.'$'.(number_format($product->product_price)).'</span>
                                    <span
                                        class="discount-percentage">'.'-$'.(number_format($product->product_sale_price)).'</span>
                                    <span
                                        class="regular-price">'.'$'.(number_format($product->product_price - $product->product_sale_price)).'</span>
                                </div>
                                ';
                            }else{
                            $output.='
                            <div class="product-price-and-shipping">
                            <span itemprop="price" class="price">'.'$'.(number_format($product->product_price)).'</span>
                                </div>
                            ';
                            }
                            $output.='
                            <div class="product-actions">

                                <form>
                                '.csrf_field().'
                                    <input type="hidden" value="'.$product->product_id.'"
                                        class="cart_product_id_'.$product->product_id.'">
                                    <input type="hidden" value="'.$product->product_name.'"
                                        class="cart_product_name_'.$product->product_id.'">
                                    <input type="hidden" value="'.$product->product_quantity.'"
                                        class="cart_product_quantity_'.$product->product_id.'">
                                    <input type="hidden" value="'.$product->product_image.'"
                                        class="cart_product_image_'.$product->product_id.'">
                                    <input type="hidden" value="'.$product->product_price.'"
                                        class="cart_product_price_'.$product->product_id.'">
                                    <input type="hidden" value="'.$product->product_sale_price.'"
                                        class="cart_product_sale_price_'.$product->product_id.'">
                                    <input type="hidden" value="1"
                                        class="cart_product_qty_'.$product->product_id.'">


                                        <input style="margin:0 0 15px 0" type="button" value="&nbsp;+ Add to cart"
                                        class="btn btn-primary add-to-cart"
                                        data-id_product="'.$product->product_id.'" name="add-to-cart">

                                </form>
                            </div>
                        </div>





                    </div>
                </li>
                    ';
                }
            }else{

            }
            $resulf = array('num'=>$num,"out" => $output,'count'=> $count,'product_slug'=>$product_slug);
            return $resulf;
        }
    }


    public function search(Request $request){
         //slide
        $slider = Slider::orderBy('slider_id','DESC')->where('slider_status','1')->take(4)->get();


         $keywords = $request->keywords_submit;

        $cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','asc')->get();
        $brand_product = DB::table('tbl_brand')->where('brand_status','0')->orderby('brand_id','desc')->get();

        $search_product = DB::table('tbl_product')->where('product_name','like','%'.$keywords.'%')->get();


        return view('pages.sanpham.search')->with('category',$cate_product)
        ->with('brand',$brand_product)->with('search_product',$search_product)
        // ->with('keywords')
        ->with('slider',$slider);

    }

    public function about_us(Request $request){
          //slide
          $slider = Slider::orderBy('slider_id','DESC')->where('slider_status','1')->take(4)->get();
          //seo
          $meta_desc = "";
          $meta_keywords = "";
          $meta_title = "";
          $url_canonical = $request->url();
          //--seo
          $cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','asc')->get();
          $brand_product = DB::table('tbl_brand')->where('brand_status','0')->orderby('brand_id','desc')->get();
          $all_product = DB::table('tbl_product')->where('product_status','0')->orderby(DB::raw('RAND()'))->paginate(8);
          $all_special = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'special')->get();
          $all_sale = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'sale')->get();
          return view('pages.ourcompany.about_us')->with('all_special',$all_special)->with('category',$cate_product)->with('brand',$brand_product)->with('all_product',$all_product)->with('meta_desc',$meta_desc)->with('meta_keywords',$meta_keywords)->with('meta_title',$meta_title)->with('url_canonical',$url_canonical)->with('slider',$slider); //1

    }
    public function terms_privacy(Request $request){
        //slide
        $slider = Slider::orderBy('slider_id','DESC')->where('slider_status','1')->take(4)->get();
        //seo
        $meta_desc = "";
        $meta_keywords = "";
        $meta_title = "";
        $url_canonical = $request->url();
        //--seo
        $cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','asc')->get();
        $brand_product = DB::table('tbl_brand')->where('brand_status','0')->orderby('brand_id','desc')->get();
        $all_product = DB::table('tbl_product')->where('product_status','0')->orderby(DB::raw('RAND()'))->paginate(8);
        $all_special = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'special')->get();
        $all_sale = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'sale')->get();
        return view('pages.ourcompany.terms_privacy')->with('all_special',$all_special)->with('category',$cate_product)->with('brand',$brand_product)->with('all_product',$all_product)->with('meta_desc',$meta_desc)->with('meta_keywords',$meta_keywords)->with('meta_title',$meta_title)->with('url_canonical',$url_canonical)->with('slider',$slider); //1

  }
  public function terms_conditions(Request $request){
    //slide
    $slider = Slider::orderBy('slider_id','DESC')->where('slider_status','1')->take(4)->get();
    //seo
    $meta_desc = "";
    $meta_keywords = "";
    $meta_title = "";
    $url_canonical = $request->url();
    $cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','asc')->get();
    $brand_product = DB::table('tbl_brand')->where('brand_status','0')->orderby('brand_id','desc')->get();
    $all_product = DB::table('tbl_product')->where('product_status','0')->orderby(DB::raw('RAND()'))->paginate(8);
    $all_special = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'special')->get();
    $all_sale = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'sale')->get();
    return view('pages.ourcompany.terms_conditions')->with('all_special',$all_special)->with('category',$cate_product)->with('brand',$brand_product)->with('all_product',$all_product)->with('meta_desc',$meta_desc)->with('meta_keywords',$meta_keywords)->with('meta_title',$meta_title)->with('url_canonical',$url_canonical)->with('slider',$slider);

}
    public function order_history(Request $request){
         //slide
        $slider = Slider::orderBy('slider_id','DESC')->where('slider_status','1')->take(4)->get();
        $result= Session::get('customer');
        $customer_id = $result->customer_id;

         //seo
         $meta_desc = "";
         $meta_keywords = "";
         $meta_title = "";
         $url_canonical = $request->url();

         $order = Order::where('customer_id',$customer_id)->orderby('created_at','DESC')->get();

        $order_by_id = DB::table('tbl_order')
        ->join('tbl_customers', 'tbl_order.customer_id', '=', 'tbl_customers.customer_id')
        ->join('tbl_shipping', 'tbl_order.shipping_id', '=', 'tbl_shipping.shipping_id')
        ->join('tbl_order_details', 'tbl_order.order_id', '=', 'tbl_order_details.order_details_id')
        ->join('tbl_paypal', 'tbl_order.shipping_id', '=', 'tbl_paypal.shipping_id')
        ->select('tbl_order.*', 'tbl_customers.*', 'tbl_shipping.*', 'tbl_order_details.*','tbl_paypal.*')->where('tbl_shipping.customer_id',$customer_id)->get();

         //--seo
         $cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','asc')->get();
         $brand_product = DB::table('tbl_brand')->where('brand_status','0')->orderby('brand_id','desc')->get();
         $all_product = DB::table('tbl_product')->where('product_status','0')->orderby(DB::raw('RAND()'))->paginate(8);
         $all_special = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'special')->get();
         $all_sale = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'sale')->limit(4)->get();

           $result= Session::get('customer');


           return view('pages.myaccount.order_history')->with('order',$order)->with('order_by_id',$order_by_id)->with('result',$result)->with('all_sale',$all_sale)->with('all_special',$all_special)->with('category',$cate_product)->with('brand',$brand_product)->with('all_product',$all_product)->with('meta_desc',$meta_desc)->with('meta_keywords',$meta_keywords)->with('meta_title',$meta_title)->with('url_canonical',$url_canonical)->with('slider',$slider);
    }
    public function view_order_history($order_code){

		$order_details = OrderDetails::with('product')->where('order_code',$order_code)->get();
		$order = Order::where('order_code',$order_code)->get();
		foreach($order as $key => $ord){
			$customer_id = $ord->customer_id;
			$shipping_id = $ord->shipping_id;
			$order_status = $ord->order_status;
        }
        $category = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','asc')->get();
        $brand = DB::table('tbl_brand')->where('brand_status','0')->orderby('brand_id','desc')->get();
        $all_product = DB::table('tbl_product')->where('product_status','0')->orderby(DB::raw('RAND()'))->paginate(8);
        $all_special = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'special')->get();
        $all_sale = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'sale')->limit(4)->get();

		$customer = Customer::where('customer_id',$customer_id)->first();
		$shipping = Shipping::where('shipping_id',$shipping_id)->first();
		$order_details_product = OrderDetails::with('product')->where('order_code', $order_code)->get();
        $order_by_id = DB::table('tbl_order')
        ->join('tbl_customers', 'tbl_order.customer_id', '=', 'tbl_customers.customer_id')
        ->join('tbl_shipping', 'tbl_order.shipping_id', '=', 'tbl_shipping.shipping_id')
        ->join('tbl_order_details', 'tbl_order.order_id', '=', 'tbl_order_details.order_details_id')
        ->join('tbl_paypal', 'tbl_order.shipping_id', '=', 'tbl_paypal.shipping_id')
        ->select('tbl_order.*', 'tbl_customers.*', 'tbl_shipping.*', 'tbl_order_details.*','tbl_paypal.*')->where('tbl_paypal.shipping_id',$shipping_id)->first();
		return view('pages.myaccount.view_order_history')->with(compact('category','brand','all_sale','order_details','customer','shipping','order_details','order','order_status','order_by_id'));

    }


    public function contact_us(Request $request){
        //slide
        $slider = Slider::orderBy('slider_id','DESC')->where('slider_status','1')->take(4)->get();
        //seo
        $meta_desc = "";
        $meta_keywords = "";
        $meta_title = "";
        $url_canonical = $request->url();
        //--seo
        $cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','asc')->get();
        $brand_product = DB::table('tbl_brand')->where('brand_status','0')->orderby('brand_id','desc')->get();
        $all_product = DB::table('tbl_product')->where('product_status','0')->orderby(DB::raw('RAND()'))->paginate(8);
        $all_special = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'special')->get();
        $all_sale = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'sale')->get();
        return view('pages.ourcompany.contact_us')->with('all_special',$all_special)->with('category',$cate_product)->with('brand',$brand_product)->with('all_product',$all_product)->with('meta_desc',$meta_desc)->with('meta_keywords',$meta_keywords)->with('meta_title',$meta_title)->with('url_canonical',$url_canonical)->with('slider',$slider); //1

    }

    public function my_account(Request $request){
        //slide
        if(Session::get('customer_id')==false){
            return Redirect::to('sign-in');
        }
        $slider = Slider::orderBy('slider_id','DESC')->where('slider_status','1')->take(4)->get();
        //seo
        $meta_desc = "";
        $meta_keywords = "";
        $meta_title = "";
        $url_canonical = $request->url();
        //--seo
        $cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','asc')->get();
        $brand_product = DB::table('tbl_brand')->where('brand_status','0')->orderby('brand_id','desc')->get();
        $all_product = DB::table('tbl_product')->where('product_status','0')->orderby(DB::raw('RAND()'))->paginate(8);
        $all_special = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'special')->get();
        $all_sale = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'sale')->get();
        return view('pages.ourcompany.my_account')->with('all_sale',$all_sale)->with('all_special',$all_special)->with('category',$cate_product)->with('brand',$brand_product)->with('all_product',$all_product)->with('meta_desc',$meta_desc)->with('meta_keywords',$meta_keywords)->with('meta_title',$meta_title)->with('url_canonical',$url_canonical)->with('slider',$slider);

    }
    public function  information(Request $request){
          //slide
          $slider = Slider::orderBy('slider_id','DESC')->where('slider_status','1')->take(4)->get();
          $code = DB::table('tbl_forgot_password')->where('email',$request->email)->first();
          //seo
          $meta_desc = "";
          $meta_keywords = "";
          $meta_title = "";
          $url_canonical = $request->url();
          //--seo
          $cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','asc')->get();
          $brand_product = DB::table('tbl_brand')->where('brand_status','0')->orderby('brand_id','desc')->get();
          $all_product = DB::table('tbl_product')->where('product_status','0')->orderby(DB::raw('RAND()'))->paginate(8);
          $all_special = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'special')->get();
          $all_sale = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'sale')->limit(4)->get();

            $result= Session::get('customer');

            $pass_md5 = md5($request->password);
            DB::table('tbl_customers')
            ->where('customer_email', $request->email)
            ->update(['customer_password' => $pass_md5]);
            return view('pages.myaccount.information')->with('result',$result)->with('all_sale',$all_sale)->with('all_special',$all_special)->with('category',$cate_product)->with('brand',$brand_product)->with('all_product',$all_product)->with('meta_desc',$meta_desc)->with('meta_keywords',$meta_keywords)->with('meta_title',$meta_title)->with('url_canonical',$url_canonical)->with('slider',$slider);

    }
    public function  update_information(Request $request){
        //slide
        $slider = Slider::orderBy('slider_id','DESC')->where('slider_status','1')->take(4)->get();
        $code = DB::table('tbl_forgot_password')->where('email',$request->email)->first();
        //seo
        $meta_desc = "";
        $meta_keywords = "";
        $meta_title = "";
        $url_canonical = $request->url();
        //--seo
        $cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','asc')->get();
        $brand_product = DB::table('tbl_brand')->where('brand_status','0')->orderby('brand_id','desc')->get();
        $all_product = DB::table('tbl_product')->where('product_status','0')->orderby(DB::raw('RAND()'))->paginate(8);
        $all_special = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'special')->get();
        $all_sale = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'sale')->limit(4)->get();

          $result= Session::get('customer');

          $pass_md5 = md5($request->password);
          DB::table('tbl_customers')
          ->where('customer_email', $request->email)
          ->update(['customer_password' => $pass_md5]);
          return view('pages.myaccount.update_password')->with('result',$result)->with('all_sale',$all_sale)->with('all_special',$all_special)->with('category',$cate_product)->with('brand',$brand_product)->with('all_product',$all_product)->with('meta_desc',$meta_desc)->with('meta_keywords',$meta_keywords)->with('meta_title',$meta_title)->with('url_canonical',$url_canonical)->with('slider',$slider); //1

  }
  public function  update_password(Request $request){
    $result= Session::get('customer');

    $customer_id = $result->customer_id;
    $password = DB::table('tbl_customers')->where('customer_id',$customer_id)->first();
    $your_pass =md5($request->your_password);
    if( $your_pass !=  $password->customer_password){
        return redirect()->back()->with(['error' => 'Wrong password']);
    }else{
          if($request->pass == $request->repass){
            $pass_md5 = md5($request->pass);
            DB::table('tbl_customers')
            ->where('customer_id', $customer_id)
            ->update(
                ['customer_password' => $pass_md5]

            );
            return back()->with(['success' => 'Password changed successfully']);
          }else{
            return back()->with(['error' => '2 Passwords must be matched']);
          }
      }

}
    public function create_account(Request $request){
        //slide
        $slider = Slider::orderBy('slider_id','DESC')->where('slider_status','1')->take(4)->get();
        //seo
        $meta_desc = "";
        $meta_keywords = "";
        $meta_title = "";
        $url_canonical = $request->url();
        //--seo
        $cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','asc')->get();
        $brand_product = DB::table('tbl_brand')->where('brand_status','0')->orderby('brand_id','desc')->get();
        $all_product = DB::table('tbl_product')->where('product_status','0')->orderby(DB::raw('RAND()'))->paginate(8);
        $all_special = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'special')->get();
        $all_sale = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'sale')->limit(4)->get();

          $result= Session::get('customer');
          return view('pages.myaccount.create_account')->with('result',$result)->with('all_sale',$all_sale)->with('all_special',$all_special)->with('category',$cate_product)->with('brand',$brand_product)->with('all_product',$all_product)->with('meta_desc',$meta_desc)->with('meta_keywords',$meta_keywords)->with('meta_title',$meta_title)->with('url_canonical',$url_canonical)->with('slider',$slider); //1

  }

  public function forgot(){
    $cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','asc')->get();
    $brand_product = DB::table('tbl_brand')->where('brand_status','0')->orderby('brand_id','desc')->get();
    $all_product = DB::table('tbl_product')->where('product_status','0')->orderby(DB::raw('RAND()'))->paginate(8);
    $all_special = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'special')->get();
    $all_sale = DB::table('tbl_product')->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.brand_id')->where('tbl_brand.brand_slug', 'sale')->limit(4)->get();
    $result= Session::get('customer');
    return view('pages.myaccount.forgot_password')->with(['step' => '1'])->with('result',$result)->with('all_sale',$all_sale)->with('all_special',$all_special)->with('category',$cate_product)->with('brand',$brand_product)->with('all_product',$all_product);
  }
  public function password(Request $request){

      $user = DB::table('tbl_customers')->where('customer_email',$request->email)->first();

      if($user == null){
           return redirect()->back()->with(['error' => 'Email not exists']);

      }
      if($request->step==1){
        $six_digit_random_number = mt_rand(100000, 999999);
        $to_name = "HandMadeOfAmerica";
        $to_email = $request->email;
        $data = array(
          "code" => $six_digit_random_number,
          "name" => $user->customer_first_name.' '.$user->customer_last_name,
          );
        //   echo "<pre>";
        //   print_R(config('mail'));
        $mail=Mail::send('pages.myaccount.forgot',$data,function($message) use ($to_name,$to_email){
            $message->to($to_email)->subject('Forgot Your Password');
            $message->from('info@handmadeofamerica.com',$to_name);
        });
        // print_R($mail);exit;
        $user = DB::table('tbl_forgot_password')->where('email',$request->email)->delete();
        $forgot_pass = new Forgotpass();
        $forgot_pass->email = $to_email;
        $forgot_pass->code = $six_digit_random_number;
      //   date_default_timezone_set('Asia/Ho_Chi_Minh');
        $forgot_pass->created_at = now();
        $forgot_pass->save();
      }


        if($request->code == null){
                return redirect()->back()->with(['success' => 'The code is sent to your email.'])->with(['step' => '2'])->with(['email' => $request->email]);
        }else{
            if($request->step==2){
                $code = DB::table('tbl_forgot_password')->where('email',$request->email)->first();

            if($code->code==$request->code){
                return redirect()->back()->with(['success' => 'Please enter a new password'])->with(['step' => '3'])->with(['email' => $request->email])->with(['code' => $request->code]);
            }else{
                    return redirect()->back()->with(['error' => 'Wrong email code'])->with(['step' => '2'])->with(['email' => $request->email]);
                }
            }
        }



      if($request->pass == null || $request->repass == null){
        return redirect()->back()->with(['success' => 'Please enter a new password'])->with(['step' => '3'])->with(['email' => $request->email])->with(['code' => $request->code]);
      }else{
          if($request->pass == $request->repass){
            $code = DB::table('tbl_forgot_password')->where('email',$request->email)->first();
            $user = DB::table('tbl_forgot_password')->where('email',$request->email)->delete();
            $pass_md5 = md5($request->pass);
            DB::table('tbl_customers')
            ->where('customer_email', $code->email)
            ->update(['customer_password' => $pass_md5]);
            return redirect()->back()->with(['success' => 'Password changed successfully']);
          }else{
            return redirect()->back()->with(['error' => '2 Passwords must be matched'])->with(['step' => '3'])->with(['email' => $request->email])->with(['code' => $request->code]);
          }

      }


  }

}
