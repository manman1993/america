<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use App\Slider;
use Illuminate\Support\Facades\Redirect;
use Cart;
use App\Coupon;
session_start();
require __DIR__ . "/vendor/autoload.php";
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
class CartController extends Controller
{


    public function paypal(Request $request){

         $cart = Session::get('cart');
         $total = 0;
				foreach(Session::get('cart') as $key => $cartpay){

                    $subtotal = ($cartpay['product_price']-$cartpay['product_sale_price'])*$cartpay['product_qty'];
                    $total+=$subtotal;
                }
        // Creating an environment
        $clientId = "ASHXLg6XEKwoyU1o8FtkMyKsMwIMcmq7kldWC6u2Z6jaEoaKsUUy7aZoM5Q3n0MSDjVGqnHohJYUjZto";
        $clientSecret = "EOtS87TEyzBclYeztS7FNOCRbehmXZxwQW5WCQ42ab2yXQ8JC4kCIjPlrWCeCdKsGsd9Agd08q_wMU7C";

        $environment = new SandboxEnvironment($clientId, $clientSecret);
        $client = new PayPalHttpClient($environment);
        //var_dump($client);
        $request = new OrdersCreateRequest();
        $request->prefer('return=representation');
        $request->body = [
            "intent" => "CAPTURE",
            "purchase_units" => [[
                "reference_id" => "test_ref_id1",
                "amount" => [
                    "value" =>$total,
                    "currency_code" => "USD"
                ]
            ]],
            "application_context" => [
                "cancel_url" => "http://127.0.0.1:8002/checkout-payment",
                "return_url" => "http://127.0.0.1:8002/"
            ]
        ];

        try {
            // Call API with your client and get a response for your call
            $response = $client->execute($request);
            echo "<pre>";
            // If call returns body in response, you can get the deserialized version from the result attribute of the response
            $response = json_decode(json_encode($response), 1);
            //var_dump($response);
        } catch (HttpException $ex) {
            // echo $ex->statusCode;
            // print_r($ex->getMessage());
        }
        return Redirect::to($response['result']['links'][1]['href']);

    }
    public function check_coupon(Request $request){
        $data = $request->all();
        $coupon = Coupon::where('coupon_code',$data['coupon'])->first();
        if($coupon){
            $count_coupon = $coupon->count();
            if($count_coupon>0){
                $coupon_session = Session::get('coupon');
                if($coupon_session==true){
                    $is_avaiable = 0;
                    if($is_avaiable==0){
                        $cou[] = array(
                            'coupon_code' => $coupon->coupon_code,
                            'coupon_condition' => $coupon->coupon_condition,
                            'coupon_number' => $coupon->coupon_number,

                        );
                        Session::put('coupon',$cou);
                    }
                }else{
                    $cou[] = array(
                            'coupon_code' => $coupon->coupon_code,
                            'coupon_condition' => $coupon->coupon_condition,
                            'coupon_number' => $coupon->coupon_number,

                        );
                    Session::put('coupon',$cou);
                }
                Session::save();
                return redirect()->back()->with('message','Thêm mã giảm giá thành công');
            }

        }else{
            return redirect()->back()->with('error','Mã giảm giá không đúng');
        }
    }
    public function cart(Request $request){
         //seo
         //slide
        $slider = Slider::orderBy('slider_id','DESC')->where('slider_status','1')->take(4)->get();

        $meta_desc = "Your cart";
        $meta_keywords = "Your cart";
        $meta_title = "Your cart";
        $url_canonical = $request->url();
        $sale_product = DB::table('tbl_product')
        ->join('tbl_category_product', 'tbl_category_product.category_id', '=', 'tbl_product.category_id')
        ->join('tbl_brand', 'tbl_brand.brand_id', '=', 'tbl_product.brand_id')
        ->where('tbl_product.product_sale_price',">" ,0)
        ->orderby('tbl_product.product_id', 'desc')->paginate(3);
        //--seo
        $cate_product = DB::table('tbl_category_product')->where('category_status','0')->orderby('category_id','asc')->get();
        $brand_product = DB::table('tbl_brand')->where('brand_status','0')->orderby('brand_id','asc')->get();

        return view('pages.cart.cart_ajax')->with('sale_product',$sale_product)->with('category',$cate_product)->with('brand',$brand_product)->with('meta_desc',$meta_desc)->with('meta_keywords',$meta_keywords)->with('meta_title',$meta_title)->with('url_canonical',$url_canonical)->with('slider',$slider);
    }
    public function add_cart_ajax(Request $request){
        // Session::forget('cart');
        $data = $request->all();
        $session_id = substr(md5(microtime()),rand(0,26),5);
        $cart = Session::get('cart');
        if($cart==true){
            $is_avaiable = 0;
            foreach($cart as $key => $val){
                if($val['product_id']==$data['cart_product_id']){
                    $is_avaiable++;
                }
            }
            if($is_avaiable == 0){
                $cart[] = array(
                'session_id' => $session_id,
                'product_name' => $data['cart_product_name'],
                'product_id' => $data['cart_product_id'],
                'product_image' => $data['cart_product_image'],
                'product_quantity' => $data['cart_product_quantity'],
                'product_qty' => $data['cart_product_qty'],
                'product_price' => $data['cart_product_price'],
                'product_sale_price' => $data['cart_product_sale_price']
                );
                Session::put('cart',$cart);
            }
        }else{
            $cart[] = array(
                'session_id' => $session_id,
                'product_name' => $data['cart_product_name'],
                'product_id' => $data['cart_product_id'],
                'product_image' => $data['cart_product_image'],
                'product_quantity' => $data['cart_product_quantity'],
                'product_qty' => $data['cart_product_qty'],
                'product_price' => $data['cart_product_price'],
                'product_sale_price' => $data['cart_product_sale_price']

            );
            Session::put('cart',$cart);
        }
            Session::save();

            $qty = 0;
            if(Session::get('cart')==true){
                foreach(Session::get('cart') as $key => $cart){
                    $subtotal = $cart['product_qty'];
                    $qty += $subtotal;
                }
            }
            Session::put('qty',$qty);
            return $qty;
    }

    public function delete_product($session_id){
        $cart = Session::get('cart');
        // echo '<pre>';
        // print_r($cart);
        // echo '</pre>';
        if($cart==true){
            foreach($cart as $key => $val){
                if($val['session_id']==$session_id){
                    unset($cart[$key]);
                }
            }
            Session::put('cart',$cart);
            return redirect()->back()->with('message','Product deleted successfully');

        }else{
            return redirect()->back()->with('message','
            Product deletion failed');
        }



    }
    public function update_cart(Request $request){
        $data = $request->all();
        $cart = Session::get('cart');
        if($cart==true){
            $message ='<p style="color:#3c763d;text-align:center">
            Update successful</p>';
            foreach($data['cart_qty'] as $key => $qty){
                $i = 0;
                foreach($cart as $session => $val){
                    $i++;

                    if($val['session_id']==$key && $qty<=$cart[$session]['product_quantity']){
                        $cart[$session]['product_qty'] = $qty;
                    }elseif($val['session_id']==$key && $qty>=$cart[$session]['product_quantity']){
                        $cart[$session]['product_qty'] = $qty;
                    }

                }
            }
            Session::put('cart',$cart);
            return redirect()->back()->with('message',$message);
        }else{
            return redirect()->back()->with('message','
            Update failed');
        }
    }
    public function delete_all_product(){
        $cart = Session::get('cart');
        if($cart==true){
            // Session::destroy();
            Session::forget('cart');
            Session::forget('coupon');
            return redirect()->back()->with('message','Erased all successfully');
        }
    }


}
