<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Feeship;
use App\Shipping;
use App\Order;
use DB;
use App\OrderDetails;
use App\Customer;
use App\Coupon;
use App\Paypal;
use App\Product;
use PDF;
use Auth;
use Illuminate\Support\Facades\Redirect;

class OrderController extends Controller
{
    public function AuthLogin()
    {
        $admin_id = Auth::id();
        if ($admin_id) {
            return Redirect::to('dashboard');
        } else {
            return Redirect::to('login-auth')->send();
        }
    }
	public function order_code(Request $request ,$order_code){
		$order = Order::where('order_code',$order_code)->first();
		$order->delete();
		 Session::put('message','Order deleted successfully');
        return redirect()->back();

	}
	public function update_qty(Request $request){
		$data = $request->all();
		$order_details = OrderDetails::where('product_id',$data['order_product_id'])->where('order_code',$data['order_code'])->first();
		$order_details->product_sales_quantity = $data['order_qty'];
		$order_details->save();
	}
	public function update_order_qty(Request $request){
		//update order
		$data = $request->all();
		$order = Order::find($data['order_id']);
		$order->order_status = $data['order_status'];
		$order->save();
		if($order->order_status==2){
			foreach($data['order_product_id'] as $key => $product_id){

				$product = Product::find($product_id);
				$product_quantity = $product->product_quantity;
				$product_sold = $product->product_sold;
				foreach($data['quantity'] as $key2 => $qty){
						if($key==$key2){
								$pro_remain = $product_quantity - $qty;
								$product->product_quantity = $pro_remain;
								$product->product_sold = $product_sold + $qty;
								$product->save();
						}
				}
			}
		}elseif($order->order_status!=2 && $order->order_status!=3){
			foreach($data['order_product_id'] as $key => $product_id){

				$product = Product::find($product_id);
				$product_quantity = $product->product_quantity;
				$product_sold = $product->product_sold;
				foreach($data['quantity'] as $key2 => $qty){
						if($key==$key2){
								$pro_remain = $product_quantity + $qty;
								$product->product_quantity = $pro_remain;
								$product->product_sold = $product_sold - $qty;
								$product->save();
						}
				}
			}
		}


	}
	public function print_order($checkout_code){
		$pdf = \App::make('dompdf.wrapper');
		$pdf->loadHTML($this->print_order_convert($checkout_code));

		return $pdf->stream();
	}
	public function print_order_convert($checkout_code){
		$order_details = OrderDetails::where('order_code',$checkout_code)->get();
		$order = Order::where('order_code',$checkout_code)->get();
		foreach($order as $key => $ord){
			$customer_id = $ord->customer_id;
            $shipping_id = $ord->shipping_id;


		}
		$customer = Customer::where('customer_id',$customer_id)->first();
		$shipping = Shipping::where('shipping_id',$shipping_id)->first();

		$order_details_product = OrderDetails::with('product')->where('order_code', $checkout_code)->get();

		foreach($order_details_product as $key => $order_d){

			$product_coupon = $order_d->product_coupon;
		}
		if($product_coupon != 'no'){
			$coupon = Coupon::where('coupon_code',$product_coupon)->first();

			$coupon_condition = $coupon->coupon_condition;
			$coupon_number = $coupon->coupon_number;

			if($coupon_condition==1){
				$coupon_echo = $coupon_number.'%';
			}elseif($coupon_condition==2){
				$coupon_echo = '$'.number_format($coupon_number);
			}
		}else{
			$coupon_condition = 2;
			$coupon_number = 0;

			$coupon_echo = '0';

		}

		$output = '';

		$output.='<style>body{
			font-family: DejaVu Sans;
		}
		.table-styling{
			border:1px solid #000;
		}
		.table-styling tbody tr td{
			border:1px solid #000;
        }
        table, td, th {
            border: 1px solid black;
          }

          table {
            border-collapse: collapse;
            width: 100%;
          }

          td {
            text-align: center;
          }
		</style>

        <H2>Orderer</H2>
        <table>
            <tr>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
            </tr>

            <tbody>';

            $output.='
                        <tr>
                            <td>'.$customer->customer_name.'</td>
                            <td>'.$customer->customer_phone.'</td>
                            <td>'.$customer->customer_email.'</td>
                        </tr>';


            $output.='
                    </tbody>

        </table>



		<h2>Shipping to</h2>
			<table class="table-styling">
				<thead>
					<tr>
						<th>Receiver</th>
						<th>Address</th>
						<th>Phone</th>
						<th>Email</th>
						<th>Note</th>
					</tr>
				</thead>
				<tbody>';

		$output.='
					<tr>
						<td>'.$shipping->shipping_name.'</td>
						<td>'.$shipping->shipping_address.'</td>
						<td>'.$shipping->shipping_phone.'</td>
						<td>'.$shipping->shipping_email.'</td>
						<td>'.$shipping->shipping_notes.'</td>

					</tr>';


		$output.='
				</tbody>

		</table>

		<h2>The order</h2>
			<table class="table-styling">
				<thead>
					<tr>
						<th>Product name</th>

						<th>Shipping fee</th>
						<th>Amount</th>
						<th>Product price</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>';

				$total = 0;

				foreach($order_details_product as $key => $product){

					$subtotal = ($product->product_price - $product->product_sale_price)*$product->product_sales_quantity;
					$total+=$subtotal;

					if($product->product_coupon!='no'){
						$product_coupon = $product->product_coupon;
					}else{
						$product_coupon = 'No coupon';
					}

                    $output.='

					<tr>
						<td>'.$product->product_name.'</td>
						<td>'.'$'.number_format($product->product_feeship,0).'</td>
						<td>'.$product->product_sales_quantity.'</td>
						<td>'.'$'.number_format($product->product_price - $product->product_sale_price).'</td>
						<td>'.'$'.number_format($subtotal + $product->product_feeship ).'</td>

					</tr>';
				}

				if($coupon_condition==1){
					$total_after_coupon = ($total*$coupon_number)/100;
	                $total_coupon = $total - $total_after_coupon;
				}else{
                  	$total_coupon = $total - $coupon_number;
				}

		        $output.= '<tr>
				<td colspan="12">


					<p>Payment : '.'$'.number_format($total_coupon + $product->product_feeship)  .'</p>
				</td>
		        </tr>
				</tbody>
		    </table>
		';


		return $output;

	}
	public function view_order($order_code){
        $this->AuthLogin();
		$order_details = OrderDetails::with('product')->where('order_code',$order_code)->get();
		$order = Order::where('order_code',$order_code)->get();
		foreach($order as $key => $ord){
			$customer_id = $ord->customer_id;
			$shipping_id = $ord->shipping_id;
			$order_status = $ord->order_status;
		}
		$customer = Customer::where('customer_id',$customer_id)->first();
		$shipping = Shipping::where('shipping_id',$shipping_id)->first();
		$order_details_product = OrderDetails::with('product')->where('order_code', $order_code)->get();
        $order_by_id = DB::table('tbl_order')
        ->join('tbl_customers', 'tbl_order.customer_id', '=', 'tbl_customers.customer_id')
        ->join('tbl_shipping', 'tbl_order.shipping_id', '=', 'tbl_shipping.shipping_id')
        ->join('tbl_order_details', 'tbl_order.order_id', '=', 'tbl_order_details.order_details_id')
        ->join('tbl_paypal', 'tbl_order.shipping_id', '=', 'tbl_paypal.shipping_id')
        ->select('tbl_order.*', 'tbl_customers.*', 'tbl_shipping.*', 'tbl_order_details.*','tbl_paypal.*')->where('tbl_paypal.shipping_id',$shipping_id)->first();
		return view('admin.view_order')->with(compact('order_details','customer','shipping','order_details','order','order_status','order_by_id'));

    }


    public function manage_order(){
    	$order = Order::orderby('created_at','DESC')->paginate(5);
    	return view('admin.manage_order')->with(compact('order'));
    }
}
