<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Forgotpass extends Model
{
    public $timestamps = false;
    protected $fillable = [
          'email',  'code',  'created_at',
    ];
    protected $primaryKey = 'id';
 	protected $table = 'tbl_forgot_password';
}
