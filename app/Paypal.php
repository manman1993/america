<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paypal extends Model
{
    public $timestamps = false; //set time to false
    protected $fillable = [
    	'shipping_id', 'price_paypal', 'status','time_paypal',
    ];
    protected $primaryKey = 'id_paypal';
 	protected $table = 'tbl_paypal';
}
