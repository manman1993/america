<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Frontend
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
// load data
Route::post('/loadmore', 'HomeController@load_data');
Route::post('/loadmorebutton', 'HomeController@loadmorebutton');
Route::post('/gallery', 'HomeController@gallery');
Route::get('/404', 'HomeController@error_page');
Route::post('/Search', 'HomeController@search');
Route::post('/autocomple-ajax', 'HomeController@autocomple_ajax');
Route::post('/quickview', 'HomeController@quickview');
// our company
Route::get('/about-us', 'HomeController@about_us');
Route::get('/terms-privacy', 'HomeController@terms_privacy');
Route::get('/terms-conditions', 'HomeController@terms_conditions');
Route::get('/contact-us', 'HomeController@contact_us');
Route::get('/my-account', 'HomeController@my_account');
Route::get('/forgot_password', 'HomeController@forgot');
Route::post('/forgot_password', 'HomeController@password');
// my account
Route::get('/information', 'HomeController@information');
Route::get('/update-information', 'HomeController@update_information');
Route::post('/update-information', 'HomeController@update_password');
Route::get('/create-account', 'HomeController@create_account');
Route::get('/order-history', 'HomeController@order_history');
Route::get('/view-order-history/{order_code}', 'HomeController@view_order_history');

//Danh muc san pham trang chu
Route::get('/category/{slug_category_product}', 'CategoryProduct@show_category_home');
Route::get('/product-sale/{brand_slug}', 'BrandProduct@show_brand_home');
Route::get('/product/{product_slug}', 'ProductController@details_product');

//Backend
Route::get('/admin', 'AdminController@index');
Route::get('/dashboard', 'AdminController@show_dashboard');
Route::get('/logout', 'AdminController@logout');
Route::post('/admin-dashboard', 'AdminController@dashboard');


//Category Product
Route::get('/add-category-product', 'CategoryProduct@add_category_product')->middleware('auth.roles');
Route::get('/edit-category-product/{category_product_id}', 'CategoryProduct@edit_category_product')->middleware('auth.roles');
Route::get('/delete-category-product/{category_product_id}', 'CategoryProduct@delete_category_product')->middleware('auth.roles');
Route::get('/all-category-product', 'CategoryProduct@all_category_product')->middleware('auth.roles');

Route::post('/export-csv', 'CategoryProduct@export_csv');
Route::post('/import-csv', 'CategoryProduct@import_csv');


Route::get('/unactive-category-product/{category_product_id}', 'CategoryProduct@unactive_category_product');
Route::get('/active-category-product/{category_product_id}', 'CategoryProduct@active_category_product');

//Send Mail
Route::get('/send-mail', 'HomeController@send_mail');

//Login facebook
Route::get('/login-facebook', 'AdminController@login_facebook');
Route::get('/admin/callback', 'AdminController@callback_facebook');

//Login google
Route::get('/login-google', 'AdminController@login_google');
Route::get('/google/callback', 'AdminController@callback_google');

Route::post('/save-category-product', 'CategoryProduct@save_category_product');
Route::post('/update-category-product/{category_product_id}', 'CategoryProduct@update_category_product');

//Brand Product
Route::get('/add-brand-product', 'BrandProduct@add_brand_product')->middleware('auth.roles');
Route::get('/edit-brand-product/{brand_product_id}', 'BrandProduct@edit_brand_product')->middleware('auth.roles');
Route::get('/delete-brand-product/{brand_product_id}', 'BrandProduct@delete_brand_product')->middleware('auth.roles');
Route::get('/all-brand-product', 'BrandProduct@all_brand_product')->middleware('auth.roles');

Route::get('/unactive-brand-product/{brand_product_id}', 'BrandProduct@unactive_brand_product');
Route::get('/active-brand-product/{brand_product_id}', 'BrandProduct@active_brand_product');

Route::post('/save-brand-product', 'BrandProduct@save_brand_product');
Route::post('/update-brand-product/{brand_product_id}', 'BrandProduct@update_brand_product');


//Product
Route::group(['middleware' => 'auth.roles'], function () {
    Route::get('/add-product', 'ProductController@add_product');
    Route::get('/edit-product/{product_id}', 'ProductController@edit_product');
});
Route::get('users', 'UserController@index')->middleware('auth.roles');
Route::get('add-users', 'UserController@add_users')->middleware('auth.roles');
Route::post('store-users', 'UserController@store_users');
Route::post('assign-roles', 'UserController@assign_roles')->middleware('auth.roles');
Route::get('delete-user-roles/{admin_id}', 'UserController@delete_user_roles')->middleware('auth.roles');

Route::get('/delete-product/{product_id}', 'ProductController@delete_product')->middleware('auth.roles');
Route::get('/all-product', 'ProductController@all_product')->middleware('auth.roles');
Route::get('/unactive-product/{product_id}', 'ProductController@unactive_product');
Route::get('/active-product/{product_id}', 'ProductController@active_product');
Route::post('/save-product', 'ProductController@save_product');
Route::post('/update-product/{product_id}', 'ProductController@update_product');
Route::post('/quickview', 'ProductController@quickview');
//Coupon
Route::post('/check-coupon', 'CartController@check_coupon');

Route::get('/unset-coupon', 'CouponController@unset_coupon');
Route::get('/insert-coupon', 'CouponController@insert_coupon');
Route::get('/delete-coupon/{coupon_id}', 'CouponController@delete_coupon');
Route::get('/list-coupon', 'CouponController@list_coupon');
Route::post('/insert-coupon-code', 'CouponController@insert_coupon_code');

//Cart
Route::post('/update-cart-quantity', 'CartController@update_cart_quantity');
Route::post('/update-cart', 'CartController@update_cart');
Route::post('/save-cart', 'CartController@save_cart');
Route::post('/add-cart-ajax', 'CartController@add_cart_ajax');
Route::get('/show-cart', 'CartController@show_cart');
Route::get('/cart', 'CartController@cart');
Route::get('/delete-to-cart/{rowId}', 'CartController@delete_to_cart');
Route::get('/del-product/{session_id}', 'CartController@delete_product');
Route::get('/del-all-product', 'CartController@delete_all_product');

//Checkout success
Route::get('/success', 'CheckoutController@success');
Route::get('/error', 'CheckoutController@error');
Route::get('/sign-in', 'CheckoutController@login_checkout');
Route::get('/del-fee', 'CheckoutController@del_fee');

Route::get('/logout-checkout', 'CheckoutController@logout_checkout');
Route::post('/add-customer', 'CheckoutController@add_customer');
Route::post('/order-place', 'CheckoutController@order_place');
Route::post('/login-customer', 'CheckoutController@login_customer');
Route::get('/checkout', 'CheckoutController@checkout');
Route::get('/checkout-payment', 'CheckoutController@checkout_payment');
Route::get('/payment', 'CheckoutController@payment');
Route::get('/paypal', 'CheckoutController@paypal');
Route::get('/paypal', 'CartController@paypal');
Route::post('/save-checkout-customer', 'CheckoutController@save_checkout_customer');
Route::post('/calculate-fee', 'CheckoutController@calculate_fee');
Route::post('/select-delivery-home', 'CheckoutController@select_delivery_home');
Route::post('/confirm-order', 'CheckoutController@confirm_order');


//Order
Route::get('/delete-order/{order_code}', 'OrderController@order_code');
Route::get('/print-order/{checkout_code}', 'OrderController@print_order');
Route::get('/manage-order', 'OrderController@manage_order')->middleware('auth.roles');
Route::get('/view-order/{order_code}', 'OrderController@view_order');
Route::post('/update-order-qty', 'OrderController@update_order_qty');
Route::post('/update-qty', 'OrderController@update_qty');



//Delivery
Route::get('/delete-feeship/{product_id}', 'DeliveryController@delete_feeship')->middleware('auth.roles');//new
Route::get('/delivery', 'DeliveryController@delivery')->middleware('auth.roles');
Route::post('/select-delivery', 'DeliveryController@select_delivery')->middleware('auth.roles');
Route::post('/insert-delivery', 'DeliveryController@insert_delivery')->middleware('auth.roles');
Route::post('/select-feeship', 'DeliveryController@select_feeship')->middleware('auth.roles');
Route::post('/update-delivery', 'DeliveryController@update_delivery')->middleware('auth.roles');
Route::get('/delete-fee/{fee_id}', 'DeliveryController@delete_feeship');

//Banner
Route::get('/manage-slider', 'SliderController@manage_slider')->middleware('auth.roles');
Route::get('/add-slider', 'SliderController@add_slider')->middleware('auth.roles');
Route::get('/delete-slide/{slide_id}', 'SliderController@delete_slide');
Route::post('/insert-slider', 'SliderController@insert_slider');
Route::get('/unactive-slide/{slide_id}', 'SliderController@unactive_slide');
Route::get('/active-slide/{slide_id}', 'SliderController@active_slide');


//authentication

Route::get('/register-auth', 'AuthController@register_auth');
Route::post('/register', 'AuthController@register');
Route::get('/login-auth', 'AuthController@login_auth');
Route::post('/login', 'AuthController@login');
Route::get('/logout-auth', 'AuthController@logout_auth');

// gallery
Route::get('/add-gallery/{product_id}', 'GalleryController@add_gallery');
Route::post('/select-gallery', 'GalleryController@select_gallery');
Route::post('/insert-gallery/{pro_id}', 'GalleryController@insert_gallery');
Route::post('/update-gallery-name', 'GalleryController@update_gallery_name');
Route::post('/delete-gallery', 'GalleryController@delete_gallery');
Route::post('/update-gallery', 'GalleryController@update_gallery');
// our company
