var prestashop = {
    cart: {
        products: [],
        totals: {
            total: { type: "total", label: "Total", amount: 0, value: "$0.00" },
            total_including_tax: {
                type: "total",
                label: "Total (tax incl.)",
                amount: 0,
                value: "$0.00"
            },
            total_excluding_tax: {
                type: "total",
                label: "Total (tax excl.)",
                amount: 0,
                value: "$0.00"
            }
        },
        subtotals: {
            products: {
                type: "products",
                label: "Subtotal",
                amount: 0,
                value: "$0.00"
            },
            discounts: null,
            shipping: {
                type: "shipping",
                label: "Shipping",
                amount: 0,
                value: "Free"
            },
            tax: { type: "tax", label: "Taxes", amount: 0, value: "$0.00" }
        },
        products_count: 0,
        summary_string: "0 items",
        labels: { tax_short: "(tax excl.)", tax_long: "(tax excluded)" },
        id_address_delivery: 0,
        id_address_invoice: 0,
        is_virtual: false,
        vouchers: { allowed: 0, added: [] },
        discounts: [],
        minimalPurchase: 0,
        minimalPurchaseRequired: ""
    },
    currency: {
        name: "US Dollar",
        iso_code: "USD",
        iso_code_num: "840",
        sign: "$"
    },
    customer: {
        lastname: null,
        firstname: null,
        email: null,
        last_passwd_gen: null,
        birthday: null,
        newsletter: null,
        newsletter_date_add: null,
        ip_registration_newsletter: null,
        optin: null,
        website: null,
        company: null,
        siret: null,
        ape: null,
        outstanding_allow_amount: 0,
        max_payment_days: 0,
        note: null,
        is_guest: 0,
        id_shop: null,
        id_shop_group: null,
        id_default_group: 1,
        date_add: null,
        date_upd: null,
        reset_password_token: null,
        reset_password_validity: null,
        id: null,
        is_logged: false,
        gender: { type: null, name: null, id: null },
        risk: { name: null, color: null, percent: null, id: null },
        addresses: []
    },
    language: {
        name: "English (English)",
        iso_code: "en",
        locale: "en-US",
        language_code: "en-us",
        is_rtl: "0",
        date_format_lite: "m/d/Y",
        date_format_full: "m/d/Y H:i:s",
        id: 1
    },
    page: {
        title: "",
        canonical: null,
        meta: {
            title: "Art Demo Store",
            description: "Shop powered by PrestaShop",
            keywords: "",
            robots: "index"
        },
        page_name: "index",
        body_classes: {
            "lang-en": true,
            "lang-rtl": false,
            "country-US": true,
            "currency-USD": true,
            "layout-full-width": true,
            "page-index": true,
            "tax-display-disabled": true
        },
        admin_notifications: []
    },
    shop: {
        name: "Art Demo Store",
        email: "urcompany@gmail.com",
        registration_number: "",
        long: false,
        lat: false,
        logo:
            "/prestashop/PRS05/PRS050093/img/art-demo-store-logo-1519024403.jpg",
        stores_icon: "/prestashop/PRS05/PRS050093/img/logo_stores.png",
        favicon: "/prestashop/PRS05/PRS050093/img/favicon.ico",
        favicon_update_time: "1519024403",
        address: {
            formatted: "Art Demo Store<br>California<br>United States",
            address1: "",
            address2: "",
            postcode: "",
            city: "",
            state: "California",
            country: "United States"
        },
        phone: "0123456789",
        fax: "465463"
    },
    urls: {
        base_url: "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/",
        current_url:
            "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php",
        shop_domain_url: "https://handmadeofamerica.com",
        img_ps_url: "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/img/",
        img_cat_url:
            "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/img/c/",
        img_lang_url:
            "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/img/l/",
        img_prod_url:
            "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/img/p/",
        img_manu_url:
            "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/img/m/",
        img_sup_url:
            "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/img/su/",
        img_ship_url:
            "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/img/s/",
        img_store_url:
            "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/img/st/",
        img_col_url:
            "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/img/co/",
        img_url:
            "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/themes/PRS050093/assets/img/",
        css_url:
            "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/themes/PRS050093/assets/css/",
        js_url:
            "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/themes/PRS050093/assets/js/",
        pic_url: "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/upload/",
        pages: {
            address:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=address",
            addresses:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=addresses",
            authentication:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=authentication",
            cart:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=cart",
            category:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=category",
            cms:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=cms",
            contact:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=contact",
            discount:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=discount",
            guest_tracking:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=guest-tracking",
            history:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=history",
            identity:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=identity",
            index:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php",
            my_account:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=my-account",
            order_confirmation:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=order-confirmation",
            order_detail:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=order-detail",
            order_follow:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=order-follow",
            order:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=order",
            order_return:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=order-return",
            order_slip:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=order-slip",
            pagenotfound:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=pagenotfound",
            password:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=password",
            pdf_invoice:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=pdf-invoice",
            pdf_order_return:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=pdf-order-return",
            pdf_order_slip:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=pdf-order-slip",
            prices_drop:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=prices-drop",
            product:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=product",
            search:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=search",
            sitemap:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=sitemap",
            stores:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=stores",
            supplier:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=supplier",
            register:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=authentication&create_account=1",
            order_login:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?controller=order&login=1"
        },
        theme_assets: "/prestashop/PRS05/PRS050093/themes/PRS050093/assets/",
        actions: {
            logout:
                "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php?mylogout="
        }
    },
    configuration: {
        display_taxes_label: false,
        low_quantity_threshold: 3,
        is_b2b: false,
        is_catalog: false,
        show_prices: true,
        opt_in: { partner: true },
        quantity_discount: { type: "discount", label: "Discount" },
        voucher_enabled: 0,
        return_enabled: 0,
        number_of_days_for_return: 14
    },
    field_required: [],
    breadcrumb: {
        links: [
            {
                title: "Home",
                url:
                    "https://handmadeofamerica.com/prestashop/PRS05/PRS050093/index.php"
            }
        ],
        count: 1
    },
    link: { protocol_link: "https://", protocol_content: "https://" },
    time: 1603875622,
    static_token: "d0079bf62de7300595f3731cc8e67631",
    token: "858056daa07e99773122634f8598b2de"
};
